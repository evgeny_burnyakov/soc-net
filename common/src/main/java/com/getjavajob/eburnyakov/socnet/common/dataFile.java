package com.getjavajob.eburnyakov.socnet.common;

import javax.persistence.*;
import java.io.File;
import java.util.Arrays;

@Embeddable
public class DataFile {
    @Transient
    private File filePath;
    @Column(name = "PHOTO")
    @Access(AccessType.PROPERTY)
    private String filePathString;

    @Transient
    private byte[] data;

    public String getFilePathString() {
        return filePath != null ? filePath.getPath() : null;
    }

    private void setFilePathString(String filePathString) {
        this.filePathString = filePathString.replace('\\', '/');
        this.filePath = new File(filePathString);
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public File getFilePath() {
        return filePath;
    }

    public void setFilePath(File file) {
        this.filePath = file;
        this.filePathString = file.getPath().replace('\\', '/');
    }

    @Override
    public String toString() {
        return "DataFile{" +
                "filePath=" + filePathString +
                ", data size(bytes)=" + (data == null ? "" : data.length) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DataFile dataFile = (DataFile) o;

        if (filePath != null ? !filePath.equals(dataFile.filePath) : dataFile.filePath != null) return false;
        return Arrays.equals(data, dataFile.data);

    }

    @Override
    public int hashCode() {
        int result = filePath != null ? filePath.hashCode() : 0;
        result = 31 * result + Arrays.hashCode(data);
        return result;
    }
}
