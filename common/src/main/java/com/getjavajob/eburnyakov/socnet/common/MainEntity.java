package com.getjavajob.eburnyakov.socnet.common;

import javax.persistence.*;

@MappedSuperclass
public abstract class MainEntity {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
