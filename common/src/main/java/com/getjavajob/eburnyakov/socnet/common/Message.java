package com.getjavajob.eburnyakov.socnet.common;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "MESSAGES")
public class Message extends MainEntity {
    @ManyToOne
    @JoinColumn(name = "AUTHOR_ID")
    private Account author;
    @Column(name = "TEXT")
    private String text;
    @Column(name = "REGISTRATION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registrationDate;
    @Column(name = "TARGET_TYPE")
    @Enumerated
    private TargetType targetType;
    @Column(name = "TARGET_ID")
    private long targetId;
    @Embedded
    private DataFile photo;

    public long getTargetID() {
        return targetId;
    }

    public void setTargetID(long targetId) {
        this.targetId = targetId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public DataFile getPhoto() {
        return photo;
    }

    public void setPhoto(DataFile photo) {
        this.photo = photo;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Account getAuthor() {
        return author;
    }

    public void setAuthor(Account author) {
        this.author = author;
    }

    public TargetType getTargetType() {
        return targetType;
    }

    public void setTargetType(TargetType targetType) {
        this.targetType = targetType;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", photo='" + photo + '\'' +
                ", text='" + text + '\'' +
                ", registrationDate=" + registrationDate +
                ", author id=" + author.getId() +
                ", targetType=" + targetType +
                ", targetID=" + targetId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Message message = (Message) o;

        if (targetId != message.targetId) return false;
        if (text != null ? !text.equals(message.text) : message.text != null) return false;
        if (registrationDate != null ? !registrationDate.equals(message.registrationDate) : message.registrationDate != null)
            return false;
        return targetType == message.targetType;

    }

    @Override
    public int hashCode() {
        int result = text != null ? text.hashCode() : 0;
        result = 31 * result + (registrationDate != null ? registrationDate.hashCode() : 0);
        result = 31 * result + (targetType != null ? targetType.hashCode() : 0);
        result = 31 * result + (int) (targetId ^ (targetId >>> 32));
        return result;
    }

    public enum TargetType {
        ACCOUNT_PRIVATE(0), ACCOUNT_WALL(1), GROUP_WALL(2);
        private int id;

        TargetType(int id) {
            this.id = id;
        }

        public static TargetType getTargetTypeById(int id) {
            switch (id) {
                case 0:
                    return ACCOUNT_PRIVATE;
                case 1:
                    return ACCOUNT_WALL;
                case 2:
                    return GROUP_WALL;
                default:
                    return null;
            }
        }

        public int getId() {
            return id;
        }
    }
}
