package com.getjavajob.eburnyakov.socnet.common;

import javax.persistence.*;

@Entity
@Table(name = "PHONES")
public class Phone extends MainEntity{
    @ManyToOne
    private Account owner;
    @Column(name = "PHONE_NUMBER")
    private String phoneNumber;

    public Phone() {
    }

    public Phone(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Account getOwner() {
        return owner;
    }

    public void setOwner(Account phoneOwner) {
        this.owner = phoneOwner;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "id=" + id +
                (owner != null ? ", phoneOwner=" + owner.getId() : "") +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Phone phone = (Phone) o;

        if (owner != null ? owner.getId() != phone.owner.getId() : phone.owner != null) return false;
        return phoneNumber.equals(phone.phoneNumber);
    }

    @Override
    public int hashCode() {
        int result = owner != null ? Long.valueOf(owner.getId()).hashCode() : 0;
        result = 31 * result + phoneNumber.hashCode();
        return result;
    }
}
