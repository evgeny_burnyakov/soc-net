package com.getjavajob.eburnyakov.socnet.common;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "GROUP_MEMBERS")
public class GroupMember implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @ManyToOne
    @JoinColumn(name = "ACCOUNT_ID")
    private Account account;
    @Id
    @ManyToOne
    @JoinColumn(name = "GROUP_ID")
    private Group group;

    @Column(name = "ROLE")
    @Enumerated
    private GroupRoles role;

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public GroupRoles getRole() {
        return role;
    }

    public void setRole(GroupRoles role) {
        this.role = role;
    }

    public enum GroupRoles {
        USER(0), MODERATOR(1);
        private int id;

        GroupRoles(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GroupMember that = (GroupMember) o;

        if (!account.equals(that.account)) return false;
        return group.equals(that.group);

    }

    @Override
    public int hashCode() {
        int result = account.hashCode();
        result = 31 * result + group.hashCode();
        return result;
    }
}
