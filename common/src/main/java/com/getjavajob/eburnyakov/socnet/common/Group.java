package com.getjavajob.eburnyakov.socnet.common;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "GROUPS")
public class Group extends MainEntity {
    @Column(name = "NAME", unique = true, nullable = false)
    private String uniqueField; // group name
    @Column(name = "REGISTRATION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registrationDate;
    @Column(name = "ISOPEN")
    private boolean isOpen; // true = open group (subscription without approval)
    @Column(name = "DESCRIPTION")
    private String description;
    @ManyToOne
    private Account creator;
    @Embedded
    private DataFile photo;

    @OneToMany(mappedBy = "group", cascade = CascadeType.ALL, orphanRemoval = true)
    private final List<GroupMember> members = new ArrayList<>();

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "GROUP_REQUESTS",
            joinColumns = @JoinColumn(name = "GROUP_ID"),
            inverseJoinColumns = @JoinColumn(name = "ACCOUNT_ID")
    )
    private final List<Account> requestsToJoinGroups = new ArrayList<>();

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "GROUP_INVITES",
            joinColumns = @JoinColumn(name = "GROUP_ID"),
            inverseJoinColumns = @JoinColumn(name = "ACCOUNT_ID")
    )
    private final List<Account> invitesToJoinGroups = new ArrayList<>();

    public Group() {
    }

    public Group(String uniqueField) {
        this.uniqueField = uniqueField;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUniqueField() {
        return uniqueField;
    }

    public void setUniqueField(String name) {
        this.uniqueField = name;
    }

    public DataFile getPhoto() {
        return photo;
    }

    public void setPhoto(DataFile photo) {
        this.photo = photo;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Account getCreator() {
        return creator;
    }

    public void setCreator(Account creator) {
        this.creator = creator;
    }

    public List<GroupMember> getMembers() {
        return members;
    }

    public void setMembers(List<GroupMember> members) {
        if (members != null) {
            this.members.clear();
            this.members.addAll(members);
        }
    }

    public boolean getIsOpen() {
        return isOpen;
    }

    public void setIsOpen(boolean open) {
        isOpen = open;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public void setRequestsToJoinGroups(List<Account> requestsToJoinGroups) {
        if (requestsToJoinGroups != null) {
            this.requestsToJoinGroups.clear();
            this.requestsToJoinGroups.addAll(requestsToJoinGroups);
        }
    }

    public List<Account> getRequestsToJoinGroups() {
        return requestsToJoinGroups;
    }

    public void addRequestsToJoinGroups(Account account) {
        requestsToJoinGroups.add(account);
    }

    public void setInvitesToJoinGroups(List<Account> invitesToJoinGroups) {
        if (invitesToJoinGroups != null) {
            this.invitesToJoinGroups.clear();
            this.invitesToJoinGroups.addAll(invitesToJoinGroups);
        }
    }

    public List<Account> getInvitesToJoinGroups() {
        return invitesToJoinGroups;
    }

    public void addInvitesToJoinGroups(Account account) {
        if (account != null) {
            invitesToJoinGroups.add(account);
        }
    }

    public void addGroupMember(Account account) {
        GroupMember groupMember = new GroupMember();
        groupMember.setAccount(account);
        groupMember.setGroup(this);
        groupMember.setRole(GroupMember.GroupRoles.USER);
        members.add(groupMember);
        account.getGroupMembers().add(groupMember);
    }

    public void removeGroupMember(Account account) {
        GroupMember groupMember = new GroupMember();
        groupMember.setAccount(account);
        groupMember.setGroup(this);
        account.getGroupMembers().remove(groupMember);
        members.remove(groupMember);
        groupMember.setAccount(null);
        groupMember.setGroup(null);
    }

    public void addInviteToJoinGroup(Account account) {
        invitesToJoinGroups.add(account);
        account.getInvitesToJoinGroups().add(this);
    }

    public void removeInviteToJoinGroup(Account account) {
        account.getInvitesToJoinGroups().remove(this);
        invitesToJoinGroups.remove(account);
    }

    public void removeRequestToJoinGroup(Account account) {
        account.getRequestsToJoinGroups().remove(this);
        requestsToJoinGroups.remove(account);
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", name='" + uniqueField + '\'' +
                ", photo=" + photo +
                ", registrationDate=" + registrationDate +
                ", description='" + description + '\'' +
                (creator != null ? ", creator id=" + creator.id : "") +
                ", isOpen=" + isOpen +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Group group = (Group) o;

        return uniqueField.equals(group.uniqueField);

    }

    @Override
    public int hashCode() {
        return uniqueField.hashCode();
    }
}
