package com.getjavajob.eburnyakov.socnet.common;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "ACCOUNTS")
public class Account extends MainEntity {
    @Column(name = "EMAIL", unique = true, nullable = false)
    private String uniqueField; // account email
    @Column(name = "PASSWORD")
    private String pass;
    @Column(name = "ROLE")
    @Enumerated
    private AccountRoles role;
    @Column(name = "REGISTRATION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registrationDate;
    @Column(name = "FIRST_NAME")
    private String firstName;
    @Column(name = "SECOND_NAME")
    private String secondName;
    @Column(name = "LAST_NAME")
    private String lastName;
    @Column(name = "BIRTH_DATE")
    @Temporal(TemporalType.DATE)
    private Date birthDate;
    @Column(name = "ICQ")
    private String icq;
    @Column(name = "SKYPE")
    private String skype;
    @Embedded
    private DataFile photo;
    @Column(name = "TIME_OUT")

    @OneToMany(mappedBy = "owner", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private final List<Phone> phones = new ArrayList<>();

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name="FRIEND_REQUESTS",
            joinColumns=@JoinColumn(name="REQUEST_ACCOUNT_ID"),
            inverseJoinColumns=@JoinColumn(name="APPROVAL_ACCOUNT_ID")
    )
    private final List<Account> friendsSentRequests = new ArrayList<>(); // requests to add friend by this account
    @ManyToMany(mappedBy = "friendsSentRequests")
    private final List<Account> friendsReceivedRequests = new ArrayList<>(); // friend requests from other accounts, waiting for our approval

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name="FRIENDS",
            joinColumns=@JoinColumn(name="FRIEND1_ID"),
            inverseJoinColumns=@JoinColumn(name="FRIEND2_ID")
    )
    private final List<Account> friends = new ArrayList<>(); // my friends

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name="GROUP_REQUESTS",
            joinColumns=@JoinColumn(name="ACCOUNT_ID"),
            inverseJoinColumns=@JoinColumn(name="GROUP_ID")
    )
    private final List<Group> requestsToJoinGroups = new ArrayList<>(); // requests to subscribe to closed groups by this account

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name="GROUP_INVITES",
            joinColumns=@JoinColumn(name="ACCOUNT_ID"),
            inverseJoinColumns=@JoinColumn(name="GROUP_ID")
    )
    private final List<Group> invitesToJoinGroups = new ArrayList<>(); // invitations to join some groups

    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL, orphanRemoval = true)
    private final List<GroupMember> groupMembers = new ArrayList<>(); // approved group subscriptions (open groups are approved automatically)

    public Account() {
    }

    public Account(String uniqueField) {
        this.uniqueField = uniqueField;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<String> getPhoneNumbers() {
        if (phones == null) {
            return null;
        }
        List<String> phoneList = new ArrayList<>();
        for (Phone phone : phones) {
            phoneList.add(phone.getPhoneNumber());
        }
        return phoneList;
    }

    public DataFile getPhoto() {
        return photo;
    }

    public void setPhoto(DataFile photo) {
        this.photo = photo;
    }

    public String getUniqueField() {
        return uniqueField;
    }

    public void setUniqueField(String uniqueField) {
        this.uniqueField = uniqueField;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public AccountRoles getRole() {
        return role;
    }

    public void setRole(AccountRoles role) {
        this.role = role;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getIcq() {
        return icq;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        if (phones != null){
            this.phones.clear();
            this.phones.addAll(phones);
        }
    }

    public List<Account> getFriends() {
        return friends;
    }

    public void setFriends(List<Account> friends) {
        if (friends != null){
            this.friends.clear();
            this.friends.addAll(friends);
        }
    }

    public List<Account> getFriendsSentRequests() {
        return friendsSentRequests;
    }

    public void setFriendsSentRequests(List<Account> friendsSentRequests) {
        if (friendsSentRequests != null){
            this.friendsSentRequests.clear();
            this.friendsSentRequests.addAll(friendsSentRequests);
        }
    }

    public List<Account> getFriendsReceivedRequests() {
        return friendsReceivedRequests;
    }

    public void setFriendsReceivedRequests(List<Account> friendsReceivedRequests) {
        if (friendsReceivedRequests != null){
            this.friendsReceivedRequests.clear();
            this.friendsReceivedRequests.addAll(friendsReceivedRequests);
        }
    }

    public void addFriendSentRequest(Account account) {
        if (account != null) {
            friendsSentRequests.add(account);
            account.getFriendsReceivedRequests().add(this);
        }
    }

    public void addFriend(Account account) {
        if (account != null) {
            friends.add(account);
            account.getFriends().add(this);
        }
    }

    public List<GroupMember> getGroupMembers() {
        return groupMembers;
    }

    public void setGroupMembers(List<GroupMember> groupMembers) {
        if (groupMembers != null){
            this.groupMembers.clear();
            this.groupMembers.addAll(groupMembers);
        }
    }

    public List<Group> getRequestsToJoinGroups() {
        return requestsToJoinGroups;
    }

    public void setRequestsToJoinGroups(List<Group> requestsToJoinGroups) {
        if (requestsToJoinGroups != null){
            this.requestsToJoinGroups.clear();
            this.requestsToJoinGroups.addAll(requestsToJoinGroups);
        }
    }

    public void addRequestsToJoinGroups(Group group) {
        if (group != null) {
            requestsToJoinGroups.add(group);
        }
    }

    public void setInvitesToJoinGroups(List<Group> invitesToJoinGroups) {
        if (invitesToJoinGroups != null){
            this.invitesToJoinGroups.clear();
            this.invitesToJoinGroups.addAll(invitesToJoinGroups);
        }    }

    public List<Group> getInvitesToJoinGroups() {
        return invitesToJoinGroups;
    }

    public void addInvitesToJoinGroups(Group group) {
        if (group != null) {
            invitesToJoinGroups.add(group);
        }
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", email='" + uniqueField + '\'' +
                ", pass='" + pass + '\'' +
                ", role=" + role +
                ", registrationDate=" + registrationDate +
                ", firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthDate=" + birthDate +
                ", icq='" + icq + '\'' +
                ", skype='" + skype + '\'' +
                ", phones=" + phones +
                ", photo=" + photo +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        if (!uniqueField.equals(account.uniqueField)) return false;
        return pass.equals(account.pass);

    }

    @Override
    public int hashCode() {
        int result = uniqueField.hashCode();
        result = 31 * result + pass.hashCode();
        return result;
    }

    public enum AccountRoles {
        USER(0), ADMIN(1);
        private int id;

        AccountRoles(int id) {
            this.id = id;
        }

        public static AccountRoles getAccountRoleById(int id) {
            switch (id) {
                case 0:
                    return USER;
                case 1:
                    return ADMIN;
                default:
                    return null;
            }
        }

        public int getId() {
            return id;
        }
    }
}
