# Yet Another Social Network

** Functionality: **

+ registration  
+ ajax loading of all pages (single page site)  
+ authentication  
+ ajax search with pagination  
+ display account and group profile  
+ edit account and group profile  
+ edit group profile privacy settings  
+ upload and download avatar for account and group  
+ dynamic html content change (Javascript/jQuery)
+ data validation on client side and server side
+ online friends tracking service

** Tools: **  
JDK 7, Spring 4, JPA 2 / Hibernate 4, jQuery 2, Twitter Bootstrap 3, JUnit 4, Mockito, Maven 3, Git / Bitbucket, Tomcat 7, MySQL/H2, IntelliJIDEA 15  


** Notes: **  
SQL ddl is located in the `db/ddl.sql`

** Screenshots **
Screenshots are located in 'screenshots' folder:
screenshots/account page.jpg
screenshots/friend page.jpg
screenshots/friends page.jpg
screenshots/group page.jpg
screenshots/groups page.jpg


--  
**Бурняков Евгений**  
Тренинг getJavaJob,   
[http://www.getjavajob.com](http://www.getjavajob.com)
