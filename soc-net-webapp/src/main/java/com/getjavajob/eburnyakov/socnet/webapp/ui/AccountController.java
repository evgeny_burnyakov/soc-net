package com.getjavajob.eburnyakov.socnet.webapp.ui;

import com.getjavajob.eburnyakov.socnet.common.*;
import com.getjavajob.eburnyakov.socnet.service.AccountService;
import com.getjavajob.eburnyakov.socnet.service.GroupService;
import com.getjavajob.eburnyakov.socnet.service.MessageService;
import com.getjavajob.eburnyakov.socnet.service.OnlineAccountService;
import com.getjavajob.eburnyakov.socnet.service.validators.Validator;
import com.getjavajob.eburnyakov.socnet.webapp.ui.dto.SearchDto;
import com.getjavajob.eburnyakov.socnet.webapp.ui.dto.SearchPageDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
public class AccountController {
    private static final int COOKIE_EXPIRY = 3_000_000;
    private static final String DEFAULT_ACCOUNT_PIC = "/resources/img/default.png";
    private static final String DEFAULT_GROUP_PIC = "/resources/img/default.png";
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);
    private static final int SEARCH_PAGE_SIZE = 3;

    @Autowired
    private AccountService accountService;
    @Autowired
    private GroupService groupService;
    @Autowired
    private OnlineAccountService onlineAccountService;
    @Autowired
    private MessageService messageService;
    @Autowired
    private Validator<Account> validator;

    public static DataFile getDataFileFromMultipart(MultipartFile file) {
        String fileName = file.getOriginalFilename();
        DataFile dataFile = new DataFile();
        dataFile.setFilePath(new File(fileName));
        try {
            dataFile.setData(file.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
            dataFile = null;
        }
        return dataFile;
    }

    @GetMapping(value = {"/startpage", "/"})
    public String showLoginPage() {
        return "index";
    }

    @PostMapping("/login")
    public ModelAndView validateLogin(@RequestParam("email") String email, @RequestParam("pass") String pass,
                                      @RequestParam(value = "rememberme", required = false) String rememberMe,
                                      HttpServletResponse response, HttpSession session) {
        Account account = accountService.readByKey(email);
        ModelAndView modelAndView = new ModelAndView();
        logger.info("validateLogin: read account:" + account);
        try {
            if (account == null || !account.getPass().equals(Hashing.hash256(pass))) {
                modelAndView.setViewName("index");
                modelAndView.addObject("emailErr", "Wrong e-mail/password combination.");
                modelAndView.addObject("passErr", "Wrong e-mail/password combination.");
                modelAndView.addObject("email", email);
                logger.error("validateLogin: invalid login/pass for account:" + account);
                return modelAndView;
            }
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        if (rememberMe != null) {
            Cookie cookie = new Cookie("id", String.valueOf(account.getId()));
            cookie.setPath("/");
            cookie.setMaxAge(COOKIE_EXPIRY);
            response.addCookie(cookie);
            logger.info("found 'remember me', set cookie: " + cookie.getName() + " : " + cookie.getValue());
        }

        session.setAttribute("id", account.getId());
        modelAndView.setViewName("redirect:/main?id=" + account.getId());

        return modelAndView;
    }

    @GetMapping("/main")
    public ModelAndView showMainPage(@RequestParam(value = "id", required = false) Long idParam,
                                     @SessionAttribute(value = "id", required = false) Long idSession) {
        logger.debug("showMainPage is called (not AJAX version)");
        ModelAndView modelAndView = showMainPageAjax(idParam, idSession);
        if (modelAndView.getViewName().equals("ourpageajax")) {
            modelAndView.addObject("pageToDisplay", "ourpageajax.jsp");
            logger.debug("Showing our page");
        } else {
            modelAndView.addObject("pageToDisplay", "foreignpageajax.jsp");
            logger.debug("Showing page of user with id in params");
        }
        modelAndView.setViewName("mainpage");

        return modelAndView;
    }

    @GetMapping("/mainpage")
    @ResponseBody
    public ModelAndView showMainPageAjax(@RequestParam(value = "id", required = false) Long idParam,
                                         @SessionAttribute(value = "id", required = false) Long idSession) {
        // if there is id in parameter - display page which belongs to the id(if any), otherwise display home page (id from session)
        Account account = null;
        ModelAndView modelAndView = new ModelAndView("ourpageajax");

        if (idParam != null) {
            account = accountService.readById(idParam);
            logger.debug("id in params is not null, read account: " + account);
            if (account != null && !idParam.equals(idSession)) {
                // page of another user
                modelAndView.setViewName("foreignpageajax");
                logger.debug("Showing page of another account");
                // check if the friend already in friend list or friend requests
                if (!accountService.isRequestedFriendship(idSession, idParam)) {
                    modelAndView.addObject("foreignId", idParam);
                }

                List<Account> friends = accountService.getFriends(idParam);
                if (friends != null) {
                    modelAndView.addObject("friends", friends);
                }
            }
        }
        if (idParam == null || account == null) {
            account = accountService.readById(idSession);
            logger.debug("id from params is null or couldn't read account using this id, " +
                    "reading account using id from session: " + account);
        }

        if (account.getPhoto() != null) {
            modelAndView.addObject("photo", account.getPhoto().getFilePath().toString().replace('\\', '/'));
        } else {
            modelAndView.addObject("photo", DEFAULT_ACCOUNT_PIC);
            logger.debug("account photo was not loaded, using default pic.");
        }

        if (account.getPhones() != null) {
            StringBuilder phones = new StringBuilder();
            for (String str : account.getPhoneNumbers()) {
                phones.append(str);
                phones.append(", ");
            }
            if (phones.length() > 1) {
                phones.delete(phones.length() - 2, phones.length() - 1);
            }
            modelAndView.addObject("phones", phones.toString());
        }
        modelAndView.addObject("account", account);
        modelAndView.addObject("defaultPic", DEFAULT_ACCOUNT_PIC);

        modelAndView.addObject("messages", messageService.readByTargetId(account.getId()));

        return modelAndView;
    }

    @GetMapping("/registration")
    public String showRegistration() {
        logger.info("showRegistration was called");
        return "registration";
    }

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        sdf.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, true));
    }

    @PostMapping("/createaccount")
    public ModelAndView createAccount(@ModelAttribute Account account,
                                      @RequestParam("pass") String pass1, @RequestParam("pass2") String pass2,
                                      @RequestParam("pic") MultipartFile file, HttpSession session) {
        if (account.getPhones() != null) {
            filterIncomingPhones(account);
        }

        if (!file.isEmpty()) {
            account.setPhoto(getDataFileFromMultipart(file));
        }

        // validation:
        Map<String, String> errorMessages = validator.validate(account);

        if (!pass1.equals(pass2)) {
            errorMessages.put("pass", "Fields 'password' do not match!");
        }

        ModelAndView modelAndView = new ModelAndView();

        // input failures processing back to user:
        if (errorMessages.size() > 0) {
            logger.error("Server validation not passed: " + errorMessages);
            modelAndView.setViewName("registration");
            modelAndView.addObject("errorMsgs", errorMessages);
        } else {
            //validation passed, storing friend into database and forwarding to main page:
            logger.info("Server validation passed");
            try {
                account.setPass(Hashing.hash256(pass1));
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            account.setRole(Account.AccountRoles.USER);
            account.setRegistrationDate(new Date());
            accountService.create(account);
            logger.info("updated account: " + account);
            session.setAttribute("id", account.getId());
            logger.info("Saved id in session");
            modelAndView.setViewName("redirect:/main?id=" + account.getId());
        }
        return modelAndView;
    }

    private void filterIncomingPhones(Account account) {
        Iterator<Phone> phoneItr = account.getPhones().iterator();
        while (phoneItr.hasNext()) {
            Phone phone = phoneItr.next();
            if (phone.getPhoneNumber() == null || "".equals(phone.getPhoneNumber()) ||
                    account.getPhones().indexOf(phone) != account.getPhones().lastIndexOf(phone)) {
                phoneItr.remove();
            } else {
                phone.setOwner(account);
            }
        }
    }

    @GetMapping("/profile")
    @ResponseBody
    public ModelAndView showProfileAjax(@SessionAttribute("id") Long id) {
        Account account = accountService.readById(id);
        ModelAndView modelAndView = new ModelAndView("editprofileajax");

        if (account.getPhoto() != null) {
            modelAndView.addObject("photo", account.getPhoto().getFilePath().toString().replace('\\', '/'));
        }
        modelAndView.addObject("account", account);
        logger.debug("showProfileAjax was called");
        return modelAndView;
    }

    @PostMapping("/updateaccount")
    @ResponseBody
    public ModelAndView updateAccount(@SessionAttribute("id") Long id, @ModelAttribute Account account,
                                      @RequestParam("pic") MultipartFile file,
                                      @RequestParam("profileImgHide") boolean isImgDeleted) {
        logger.info("UpdateAccount method called, parsed from @ModelAttribute:" + account);
        Account oldAccount = accountService.readById(id);
        account.setId(oldAccount.getId());

        if (account.getPhones() != null) {
            filterIncomingPhones(account);
        }

        if (!file.isEmpty()) {
            account.setPhoto(getDataFileFromMultipart(file));
        } else if (isImgDeleted) {
            account.setPhoto(null);
        } else {
            account.setPhoto(oldAccount.getPhoto());
        }
        account.setUniqueField(oldAccount.getUniqueField());
        account.setPass(oldAccount.getPass());
        account.setRegistrationDate(oldAccount.getRegistrationDate());
        account.setRole(oldAccount.getRole());

        account.setFriends(accountService.getFriends(account.getId()));
        account.setFriendsReceivedRequests(accountService.getInFriendRequests(account.getId()));
        account.setFriendsSentRequests(accountService.getOutFriendRequests(account.getId()));

        account.setGroupMembers(accountService.getGroupsMembers(account.getId()));
        account.setInvitesToJoinGroups(accountService.getInvitesToGroup(account.getId()));
        account.setRequestsToJoinGroups(accountService.getRequestsToJoinGroup(account.getId()));

        accountService.update(account);
        logger.info("saved account: " + account);

        return showMainPageAjax(null, id);
    }

    @GetMapping("/logout")
    public String logOut(HttpSession session, HttpServletResponse resp, @SessionAttribute long id) {
        OnlineAccount onlineAccount = onlineAccountService.readById(id);
        if (onlineAccount != null) {
            onlineAccount.setTimeOut(null);
            onlineAccountService.update(onlineAccount);
        }
        session.invalidate();
        Cookie cookie = new Cookie("id", "");
        cookie.setPath("/");
        cookie.setMaxAge(0);
        resp.addCookie(cookie);
        logger.info("logging out for account id:" + id);

        return "redirect:/startpage";
    }

    @RequestMapping("/search")
    @ResponseBody
    public SearchPageDto search(@RequestParam("filter") final String filter,
                                @RequestParam("type") final String searchType,
                                @RequestParam("page") final Integer requestedPageNumber) {
        logger.debug("filter = " + filter);
        logger.debug("searchType = " + searchType);
        logger.debug("requestedPageNumber = " + requestedPageNumber);

        int startRow = (requestedPageNumber - 1) * SEARCH_PAGE_SIZE + 1;
        int pagesQty;

        logger.debug("SEARCH start row:" + startRow);

        SearchPageDto searchPageDto = new SearchPageDto();
        List<SearchDto> dtos = new ArrayList<>();
        long accountEntriesQty = 0;

        if (searchType.equals("all") || searchType.equals("accounts")) {
            accountEntriesQty = accountService.getAllEntriesQty(filter);
            pagesQty = (int) accountEntriesQty / SEARCH_PAGE_SIZE + 1;

            logger.debug("FRIEND SECTION: total records found = " + accountEntriesQty);
            logger.debug("FRIEND SECTION: pagesQty = " + pagesQty);

            List<Account> accounts = accountService.search(startRow, SEARCH_PAGE_SIZE, filter);

            logger.debug("search result accounts = " + accounts);

            for (Account account : accounts) {
                SearchDto dto = new SearchDto();
                dto.setType(SearchDto.DtoType.ACCOUNT);
                dto.setId(account.getId());
                dto.setText(account.getFirstName() +
                        (account.getSecondName() == null ? "" : " " + account.getSecondName()) +
                        (account.getLastName() == null ? "" : " " + account.getLastName()));
                if (account.getPhoto() != null) {
                    dto.setPic(account.getPhoto().getFilePath().getPath().replace('\\', '/'));
                } else {
                    dto.setPic(DEFAULT_ACCOUNT_PIC);
                }
                dtos.add(dto);
            }
            logger.debug("dtos after account section = " + dtos);
        }

        long groupEntitiesQty = groupService.getAllEntriesQty(filter);
        pagesQty = (int) (accountEntriesQty + groupEntitiesQty - 1) / SEARCH_PAGE_SIZE + 1;
        if ((searchType.equals("all") && dtos.size() < SEARCH_PAGE_SIZE) || searchType.equals("groups")) {
            logger.debug("GROUP SECTION pagesQty = " + pagesQty);
            logger.debug("GROUP SECTION groupEntitiesQty = " + groupEntitiesQty);
            logger.debug("GROUP SECTION pagesQty = " + pagesQty);

            int groupEntiresQty = SEARCH_PAGE_SIZE;
            if (searchType.equals("all")) {
                if (dtos.size() > 0) {
                    startRow = 1;
                    groupEntiresQty = groupEntiresQty - dtos.size();
                } else {
                    startRow = startRow - (int) accountEntriesQty;
                }
            }

            List<Group> groups = groupService.search(startRow, groupEntiresQty, filter);
            logger.debug("GROUP SECTION SEARCH groups = " + groups);
            for (Group group : groups) {
                SearchDto dto = new SearchDto();
                dto.setType(SearchDto.DtoType.GROUP);
                dto.setId(group.getId());
                dto.setText(group.getUniqueField());
                if (group.getPhoto() != null) {
                    dto.setPic(group.getPhoto().getFilePath().getPath().replace('\\', '/'));
                } else {
                    dto.setPic(DEFAULT_GROUP_PIC);
                }
                dtos.add(dto);
            }
        }
        logger.debug("dtos after all sections = " + dtos);

        searchPageDto.setPagesQty(pagesQty);
        searchPageDto.setCurrentPage(requestedPageNumber);
        searchPageDto.setDtos(dtos);

        logger.debug("FINAL searchPageDto = " + searchPageDto);
        return searchPageDto;
    }

    @GetMapping("/friends")
    @ResponseBody
    public ModelAndView showFriends(@SessionAttribute("id") Long id) {
        List<Account> friends = accountService.getFriends(id);
        ModelAndView modelAndView = new ModelAndView("friendsajax");
        modelAndView.addObject("friends", friends);
        modelAndView.addObject("defaultPic", DEFAULT_ACCOUNT_PIC);
        return modelAndView;
    }

    @GetMapping("/addfriendrequest")
    @ResponseBody
    public void addFriendRequest(@RequestParam(value = "id") Long idParam,
                                 @SessionAttribute(value = "id") Long idSession) {
        if (idParam != null && idParam != idSession) {
            accountService.addFriendRequest(idSession, idParam);
        }
    }

    @GetMapping("/outfriendrequests")
    @ResponseBody
    public ModelAndView showOutfriendRequests(@SessionAttribute(value = "id") Long idSession) {
        List<Account> myFriendRequests = accountService.getOutFriendRequests(idSession);
        ModelAndView modelAndView = new ModelAndView("outfriendrequestsajax");
        modelAndView.addObject("myFriendRequests", myFriendRequests);
        modelAndView.addObject("defaultPic", DEFAULT_ACCOUNT_PIC);
        return modelAndView;
    }

    @GetMapping("/infriendrequests")
    @ResponseBody
    public ModelAndView showInfriendRequests(@SessionAttribute(value = "id") Long idSession) {
        List<Account> inFriendRequests = accountService.getInFriendRequests(idSession);
        ModelAndView modelAndView = new ModelAndView("infriendrequestsajax");
        modelAndView.addObject("inFriendRequests", inFriendRequests);
        modelAndView.addObject("defaultPic", DEFAULT_ACCOUNT_PIC);
        return modelAndView;
    }

    @GetMapping("/approveFriendRequest")
    @ResponseBody
    public ModelAndView approveFriendRequest(@RequestParam(value = "id") Long friendId,
                                             @SessionAttribute(value = "id") Long myId) {
        accountService.addFriend(myId, friendId);
        return showInfriendRequests(myId);
    }

    @GetMapping("/declineFriendRequest")
    @ResponseBody
    public ModelAndView declineFriendRequest(@RequestParam(value = "id") Long friendId,
                                             @SessionAttribute(value = "id") Long myId) {
        accountService.declineFriend(myId, friendId);
        return showInfriendRequests(myId);
    }

    @RequestMapping("/friendsonline")
    @ResponseBody
    public List<SearchDto> showFriendsOnline(@SessionAttribute(value = "id") long accountId) {
        Account account = accountService.readById(accountId);
//        OnlineAccount onlineAccount = onlineAccountService.readByAccount(account);
        OnlineAccount onlineAccount = onlineAccountService.readById(accountId);
        if (onlineAccount == null) {
            onlineAccount = new OnlineAccount();
            onlineAccount.setId(accountId);
            onlineAccount.setFirstName(account.getFirstName());
            onlineAccount.setLastName(account.getLastName());
            onlineAccountService.create(onlineAccount);
        }
        onlineAccount.setTimeOut(new Date((new Date()).getTime() + 1000 * 60 * 5));
        onlineAccountService.update(onlineAccount);

        List<SearchDto> dtosOnline = new ArrayList<>();
        if (account.getFriends() != null) {
            List<OnlineAccount> friendsOnline = onlineAccountService.getFriendsOnline(accountId);

            for (OnlineAccount friendOnline : friendsOnline) {
                SearchDto dto = new SearchDto();
                dto.setId(friendOnline.getId());
                dto.setText(friendOnline.getFirstName() +
                        (friendOnline.getLastName() == null ? "" : " " + friendOnline.getLastName()));
                dtosOnline.add(dto);
            }
        }
        return dtosOnline;
    }

    @GetMapping("/removeFromFriends")
    @ResponseBody
    public ModelAndView removeFromFriends(@RequestParam(value = "id") Long friendId,
                                          @SessionAttribute(value = "id") Long myId) {
        accountService.removeFromFriends(myId, friendId);
        return showFriends(myId);
    }
}
