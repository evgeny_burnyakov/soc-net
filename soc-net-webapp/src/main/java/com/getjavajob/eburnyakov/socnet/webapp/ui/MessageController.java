package com.getjavajob.eburnyakov.socnet.webapp.ui;

import com.getjavajob.eburnyakov.socnet.common.Account;
import com.getjavajob.eburnyakov.socnet.common.Message;
import com.getjavajob.eburnyakov.socnet.service.AccountService;
import com.getjavajob.eburnyakov.socnet.service.GroupService;
import com.getjavajob.eburnyakov.socnet.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class MessageController {
    private static final Logger logger = LoggerFactory.getLogger(MessageController.class);

    @Autowired
    private AccountService accountService;
    @Autowired
    private MessageService messageService;
    @Autowired
    private AccountController accountController;

    @GetMapping("/createWallMessage")
    @ResponseBody
    public ModelAndView createWallMessage(@RequestParam long id) {
        ModelAndView modelAndView = new ModelAndView("createwallmessageajax");
        modelAndView.addObject("messageTargetId", id);
        return modelAndView;
    }

    @GetMapping("/postWallMessage")
    @ResponseBody
    public ModelAndView postWallMessage(@ModelAttribute Message message,
                                        @RequestParam("photo") MultipartFile file,
                                        @SessionAttribute("id") Long accountId) {
        Account account = accountService.readById(accountId);
        message.setAuthor(account);
        message.setRegistrationDate(new Date());
        message.setTargetType(Message.TargetType.ACCOUNT_WALL);
        if (!file.isEmpty()) {
            message.setPhoto(AccountController.getDataFileFromMultipart(file));
        }
        messageService.create(message);

        ModelAndView modelAndView = new ModelAndView("wallmessagesajax");
        List<Message> messages = messageService.readByTargetId(message.getTargetID());
        modelAndView.addObject("messages", messages);
        return modelAndView;
    }
}
