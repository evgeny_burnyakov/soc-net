package com.getjavajob.eburnyakov.socnet.webapp.ui.dto;

public class SearchDto {
    private DtoType type;
    private long id;
    private String text;
    private String pic;

    public DtoType getType() {
        return type;
    }

    public void setType(DtoType type) {
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    @Override
    public String toString() {
        return "SearchDto{" +
                "type='" + type + '\'' +
                ", id=" + id +
                ", text='" + text + '\'' +
                '}';
    }

    public enum DtoType {ACCOUNT, GROUP}
}
