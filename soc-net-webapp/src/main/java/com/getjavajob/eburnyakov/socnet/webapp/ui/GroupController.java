package com.getjavajob.eburnyakov.socnet.webapp.ui;

import com.getjavajob.eburnyakov.socnet.common.Account;
import com.getjavajob.eburnyakov.socnet.common.DataFile;
import com.getjavajob.eburnyakov.socnet.common.Group;
import com.getjavajob.eburnyakov.socnet.common.GroupMember;
import com.getjavajob.eburnyakov.socnet.service.AccountService;
import com.getjavajob.eburnyakov.socnet.service.GroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.File;
import java.io.IOException;
import java.util.*;

@Controller
public class GroupController {
    private static final String DEFAULT_GROUP_PIC = "/resources/img/default.png";
    private static final Logger logger = LoggerFactory.getLogger(GroupController.class);

    @Autowired
    private AccountService accountService;
    @Autowired
    private GroupService groupService;

    @GetMapping("/groups")
    @ResponseBody
    public ModelAndView showGroups(@SessionAttribute("id") Long accountId) {
        List<GroupMember> groupMembers = accountService.getGroupsMembers(accountId);
        ModelAndView modelAndView = new ModelAndView("groupsajax");
        modelAndView.addObject("groupMembers", groupMembers);
        modelAndView.addObject("defaultPic", DEFAULT_GROUP_PIC);
        return modelAndView;
    }

    @GetMapping("/creategroup")
    @ResponseBody
    public ModelAndView showCreateGroupPage() {
        ModelAndView modelAndView = new ModelAndView("creategroupajax");
        return modelAndView;
    }

    @PostMapping("/creategroup")
    @ResponseBody
    public ModelAndView createGroup(@ModelAttribute Group group,
                                    @RequestParam("photo") MultipartFile file,
                                    @SessionAttribute("id") Long accountId) {
        logger.debug("parsed from parameters group:" + group);

        if (!file.isEmpty()) {
            group.setPhoto(AccountController.getDataFileFromMultipart(file));
        }

        // validation:
        Map<String, String> errorMessages = new HashMap<>();
        if (group.getUniqueField() == null || "".equals(group.getUniqueField())) {
            errorMessages.put("groupName", "Group name not filled.");
        } else if (groupService.readByKey(group.getUniqueField()) != null) {
            errorMessages.put("groupName", "Group with this name already exists.");
        }

        if (errorMessages.size() > 0) { // input failures processing back to user:
            logger.error("Server validation not passed:" + errorMessages);
            ModelAndView modelAndView = new ModelAndView("creategroupajax");
            modelAndView.addObject("group", group);
            modelAndView.addObject("errorMsgs", errorMessages);
            return modelAndView;
        } else {
            //validation passed, storing account into database and forwarding to main page:
            group.setCreator(accountService.readById(accountId));
            group.setRegistrationDate(new Date());
            groupService.create(group);
            return showGroupPageAjax(group.getId(), accountId);
        }
    }

    @GetMapping("/grouppage")
    @ResponseBody
    public ModelAndView showGroupPageAjax(@RequestParam(value = "id") Long groupId,
                                          @SessionAttribute("id") Long accountId) {
        ModelAndView modelAndView = new ModelAndView("grouppageajax");
        Group group = groupService.readById(groupId);
        if (group.getPhoto() != null) {
            modelAndView.addObject("photo", group.getPhoto().getFilePath().toString().replace('\\', '/'));
        } else {
            modelAndView.addObject("photo", DEFAULT_GROUP_PIC);
        }
        modelAndView.addObject("group", group);
        modelAndView.addObject("groupMembers", groupService.getGroupMembers(groupId));
        modelAndView.addObject("defaultPic", DEFAULT_GROUP_PIC);

        Account account = accountService.readById(accountId);
        if (group.getCreator().equals(account)) {
            modelAndView.addObject("groupMode", "ADMIN");
            logger.debug("showGroupPageAjax method: admin mode");
        } else {
            GroupMember groupMember = null;
            for (GroupMember member : groupService.getGroupMembers(groupId)) {
                if (member.getAccount().getId() == account.getId()) {
                    groupMember = member;
                    break;
                }
            }
            if (groupMember == null) {
                if (accountService.getRequestsToJoinGroup(accountId).contains(group)) {  // already requested to join group
                    modelAndView.addObject("groupMode", "REQUESTED");
                    logger.debug("showGroupPageAjax method: requested mode");
                } else {
                    modelAndView.addObject("groupMode", "NEW");
                    logger.debug("showGroupPageAjax method: new mode");
                }
            } else if (groupMember.getRole() == GroupMember.GroupRoles.USER) {
                modelAndView.addObject("groupMode", "USER");
                logger.debug("showGroupPageAjax method: user mode");
            } else if (groupMember.getRole() == GroupMember.GroupRoles.MODERATOR) {
                modelAndView.addObject("groupMode", "MODERATOR");
                logger.debug("showGroupPageAjax method: moderator mode");
            }
        }

        return modelAndView;
    }

    @GetMapping("/group")
    public ModelAndView showGroupPage(@RequestParam(value = "id", required = false) Long groupId,
                                      @SessionAttribute("id") Long accountId) {
        if (groupId == null) {
            return showGroups(accountId);
        }
        ModelAndView modelAndView = showGroupPageAjax(groupId, accountId);
        modelAndView.setViewName("mainpage");
        modelAndView.addObject("pageToDisplay", "grouppageajax.jsp");
        return modelAndView;
    }

    @GetMapping("/joingroup")
    @ResponseBody
    public ModelAndView joinGroup(@RequestParam(value = "id") Long groupId,
                                  @SessionAttribute("id") Long accountId) {
        Group group = groupService.readById(groupId);
        if (group.isOpen()) {
            groupService.addGroupMember(accountId, groupId);
        } else {
            groupService.addRequestToJoinGroup(groupId, accountId);
        }
        return showGroupPageAjax(groupId, accountId);
    }

    @GetMapping("/invitefriends")
    @ResponseBody
    public ModelAndView inviteFriends(@RequestParam(value = "id") Long groupId,
                                      @SessionAttribute("id") Long accountId) {
        Group group = groupService.readById(groupId);

        ModelAndView modelAndView = new ModelAndView("invitefriendsajax");
        modelAndView.addObject("group", group);

        // filter friends (may be already members or invited/requested):
        List<Account> groupMembers = groupService.getGroupAccounts(groupId);
        List<Account> groupRequestedAccounts = groupService.getRequestsToJoinGroup(groupId);
        List<Account> invitedAccounts = groupService.getInvitesToGroup(groupId);

        List<Account> friends = accountService.getFriends(accountId);
        Iterator<Account> iterator = friends.iterator();
        while (iterator.hasNext()) {
            Account friend = iterator.next();
            if (groupMembers.contains(friend) || groupRequestedAccounts.contains(friend) ||
                    invitedAccounts.contains(friend)) {
                iterator.remove();
            }
        }
        modelAndView.addObject("friends", friends);

        return modelAndView;
    }

    @PostMapping("/invitefriends")
    @ResponseBody
    public ModelAndView processFriendInvites(@RequestParam(value = "groupId") Long groupId,
                                             @RequestParam(value = "inviteIds", required = false) Long[] inviteIds,
                                             @SessionAttribute("id") Long accountId) {
        if (inviteIds != null && inviteIds.length != 0) {
            for (Long friendId : inviteIds) {
                groupService.addInviteToGroup(groupId, friendId);
            }
        }
        return showGroupPageAjax(groupId, accountId);
    }

    @GetMapping("/grouprequests")
    @ResponseBody
    public ModelAndView showGroupRequests(@SessionAttribute("id") Long accountId) {
        ModelAndView modelAndView = new ModelAndView("grouprequestspageajax");
        modelAndView.addObject("groups", accountService.getRequestsToJoinGroup(accountId));
        return modelAndView;
    }

    @GetMapping("/groupinvites")
    @ResponseBody
    public ModelAndView showGroupInvites(@SessionAttribute("id") Long accountId) {
        ModelAndView modelAndView = new ModelAndView("groupinvitespageajax");
        modelAndView.addObject("groups", accountService.getInvitesToGroup(accountId));
        return modelAndView;
    }

    @GetMapping("/leavegroup")
    @ResponseBody
    public ModelAndView leaveGroup(@RequestParam(value = "id") Long groupId,
                                   @SessionAttribute("id") Long accountId) {
        System.out.println("leaving group " + groupId);
        groupService.leaveGroup(groupId, accountId);
        return showGroups(accountId);
    }

    @GetMapping("/acceptinvite")
    @ResponseBody
    public ModelAndView acceptInvite(@RequestParam(value = "id") Long groupId,
                                     @SessionAttribute("id") Long accountId) {
        groupService.acceptInvite(groupId, accountId);
        return showGroupInvites(accountId);
    }

    @GetMapping("/declineinvite")
    @ResponseBody
    public ModelAndView declineInvite(@RequestParam(value = "id") Long groupId,
                                      @SessionAttribute("id") Long accountId) {
        groupService.declineInvite(groupId, accountId);
        return showGroupInvites(accountId);
    }

    @GetMapping("/editgroup")
    @ResponseBody
    public ModelAndView showEditGroupPage(@RequestParam(value = "id") Long groupId,
                                          @SessionAttribute("id") Long accountId) {
        // check if the account is owner of the group:
        Account account = accountService.readById(accountId);
        Group group = groupService.readById(groupId);
        if (!group.getCreator().equals(account)) {
            return showGroupPageAjax(groupId, accountId);
        }

        ModelAndView modelAndView = new ModelAndView("editgroupajax");
        modelAndView.addObject("group", group);
        return modelAndView;
    }

    @PostMapping("/updategroup")
    @ResponseBody
    public ModelAndView updateGroup(@ModelAttribute Group group,
                                    @RequestParam("photo") MultipartFile file,
                                    @SessionAttribute("id") Long accountId,
                                    @RequestParam("groupImgHide") boolean isImgDeleted) {
        System.out.println("PARSED GROUP UPDATE: " + group);

        Account account = accountService.readById(accountId);
        Group oldGroup = groupService.readById(group.getId());
        // make sure it is owner of the group:
        if (!oldGroup.getCreator().equals(account)) {
            logger.error("NOT A GROUP OWNER, REDIRECTING TO GROUP PAGE");
            return showGroupPageAjax(group.getId(), accountId);
        }

        if (!file.isEmpty()) {
            group.setPhoto(AccountController.getDataFileFromMultipart(file));
        } else if (isImgDeleted) {
            group.setPhoto(null);
        } else {
            group.setPhoto(oldGroup.getPhoto());
        }

        // validation:
        Map<String, String> errorMessages = new HashMap<>();
        if (group.getUniqueField() == null || "".equals(group.getUniqueField())) {
            errorMessages.put("groupName", "Group name not filled.");
        } else if (!oldGroup.getUniqueField().equals(group.getUniqueField()) &&
                groupService.readByKey(group.getUniqueField()) != null) {
            errorMessages.put("groupName", "This name is occupied.");
        }

        if (errorMessages.size() > 0) { // input failures processing back to user:
            logger.error("Server validation not passed:" + errorMessages);
            ModelAndView modelAndView = new ModelAndView("editgroupajax");
            modelAndView.addObject("group", group);
            modelAndView.addObject("errorMsgs", errorMessages);
            return modelAndView;
        } else {
            //validation passed, storing account into database and forwarding to main page:
            group.setCreator(oldGroup.getCreator());
            group.setRegistrationDate(oldGroup.getRegistrationDate());
            if (!oldGroup.isOpen() && group.isOpen()) { // if closed group has been opened - remove moderators
                groupService.removeModerators(group.getId());
            }

            group.setInvitesToJoinGroups(groupService.getInvitesToGroup(group.getId()));
            group.setRequestsToJoinGroups(groupService.getRequestsToJoinGroup(group.getId()));
            group.setMembers(groupService.getGroupMembers(group.getId()));

            groupService.update(group);
            return showGroupPageAjax(group.getId(), accountId);
        }
    }

    @GetMapping("/editmoderators")
    @ResponseBody
    public ModelAndView showEditModerators(@RequestParam(value = "id") Long groupId,
                                           @SessionAttribute("id") Long accountId) {
        Group group = groupService.readById(groupId);

        ModelAndView modelAndView = new ModelAndView("editmoderatorsajax");
        modelAndView.addObject("group", group);
        List<GroupMember> groupMembers = groupService.getGroupMembers(groupId);
        // remove owner from the list:
        GroupMember me = new GroupMember();
        me.setGroup(groupService.readById(groupId));
        me.setAccount(accountService.readById(accountId));
        groupMembers.remove(me);

        modelAndView.addObject("members", groupMembers);

        return modelAndView;
    }

    @GetMapping("/managegrouprequests")
    @ResponseBody
    public ModelAndView showManageGroupRequests(@RequestParam(value = "id") Long groupId,
                                           @SessionAttribute("id") Long accountId) {
        Group group = groupService.readById(groupId);
        ModelAndView modelAndView = new ModelAndView("grouprequestsajax");
        modelAndView.addObject("group", group);
        modelAndView.addObject("accounts", groupService.getRequestsToJoinGroup(groupId));
        return modelAndView;
    }

    @GetMapping("/acceptgrouprequest")
    @ResponseBody
    public ModelAndView acceptGroupRequest(@RequestParam(value = "groupId") Long groupId,
                                           @RequestParam(value = "userId") Long userId,
                                            @SessionAttribute("id") Long accountId) {
        Group group = groupService.readById(groupId);
        Account moderator = accountService.readById(accountId);
        // make sure it is owner or moderator of the group:
        if (!groupService.getGroupModerators(groupId).contains(moderator)) {
            System.out.println("NOT A MODERATOR OR OWNER, REDIRECTING TO GROUP PAGE");
            return showGroupPageAjax(group.getId(), accountId);
        }
        groupService.acceptGroupRequest(groupId, userId);

        return showManageGroupRequests(groupId, accountId);
    }

    @GetMapping("/declinegrouprequest")
    @ResponseBody
    public ModelAndView declineGroupRequest(@RequestParam(value = "groupId") Long groupId,
                                           @RequestParam(value = "userId") Long userId,
                                           @SessionAttribute("id") Long accountId) {
        Group group = groupService.readById(groupId);
        Account moderator = accountService.readById(accountId);
        // make sure it is owner or moderator of the group:
        if (!groupService.getGroupModerators(groupId).contains(moderator)) {
            System.out.println("NOT A MODERATOR OR OWNER, REDIRECTING TO GROUP PAGE");
            return showGroupPageAjax(group.getId(), accountId);
        }
        groupService.declineGroupRequest(groupId, userId);

        return showManageGroupRequests(groupId, accountId);
    }

    @GetMapping("/switchgrouprole")
    @ResponseBody
    public ModelAndView switchGroupRole(@RequestParam(value = "groupId") Long groupId,
                                            @RequestParam(value = "userId") Long userId,
                                            @SessionAttribute("id") Long accountId) {
        Account moderator = accountService.readById(accountId);
        // make sure it is owner or moderator of the group:
        if (!groupService.getGroupModerators(groupId).contains(moderator)) {
            logger.error("NOT A MODERATOR OR OWNER, REDIRECTING TO GROUP PAGE");
            return showGroupPageAjax(groupId, accountId);
        }
        groupService.switchGroupRole(groupId, userId);

        return showEditModerators(groupId, accountId);
    }

    @GetMapping("/deletegroup")
    @ResponseBody
    public ModelAndView deleteGroup(@RequestParam(value = "id") Long groupId,
                                        @SessionAttribute("id") Long accountId) {
        Group group = groupService.readById(groupId);
        Account moderator = accountService.readById(accountId);
        // make sure it is owner or moderator of the group:
        if (!groupService.getGroupModerators(groupId).contains(moderator)) {
            logger.error("NOT A MODERATOR OR OWNER, REDIRECTING TO GROUP PAGE");
            return showGroupPageAjax(groupId, accountId);
        }
        groupService.delete(group);
        return showGroups(accountId);
    }
}
