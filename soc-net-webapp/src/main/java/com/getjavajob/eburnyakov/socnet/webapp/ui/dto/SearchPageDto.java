package com.getjavajob.eburnyakov.socnet.webapp.ui.dto;

import java.util.List;

public class SearchPageDto {
    private List<SearchDto> dtos;
    private int pagesQty;
    private int currentPage;

    public List<SearchDto> getDtos() {
        return dtos;
    }

    public void setDtos(List<SearchDto> dtos) {
        this.dtos = dtos;
    }

    public int getPagesQty() {
        return pagesQty;
    }

    public void setPagesQty(int pagesQty) {
        this.pagesQty = pagesQty;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    @Override
    public String toString() {
        return "SearchPageDto{" +
                "dtos=" + dtos +
                ", pagesQty=" + pagesQty +
                ", currentPage=" + currentPage +
                '}';
    }
}
