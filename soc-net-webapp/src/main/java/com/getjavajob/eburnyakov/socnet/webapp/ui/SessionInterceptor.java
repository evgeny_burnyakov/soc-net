package com.getjavajob.eburnyakov.socnet.webapp.ui;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class SessionInterceptor extends HandlerInterceptorAdapter {
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    private List<String> exceptUrls;

    public void setExceptUrls(List<String> exceptUrls) {
        this.exceptUrls = exceptUrls;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String url = request.getServletPath();
        logger.debug("Interceptor: incoming url:" + url);

        // checking if session is valid:
        if (request.isRequestedSessionIdValid() && request.getSession().getAttribute("id") != null) {
            logger.debug("Interceptor: valid session");
            if (url.equals("/")) { // redirecting user to his home page
                url = "/main?id=" + request.getSession().getAttribute("id");
                response.sendRedirect(request.getContextPath() + url);
                return false;
            }
            return true;
        }

        // session is invalid checking if there is cookie with session id:
        if (request.getCookies() != null) {
            Cookie[] cookies = request.getCookies();
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("id") && cookie.getValue() != null && !cookie.getValue().equals("")) {
                    Long idFromCookie = Long.valueOf(cookie.getValue());
                    request.getSession().setAttribute("id", idFromCookie);
                    logger.debug("Interceptor: invalid session, but there is cookie, so its ok");
                    if (url.equals("/")) { // redirecting user to his home page
                        url = "/main?id=" + idFromCookie;
                        response.sendRedirect(request.getContextPath() + url);
                        return false;
                    } else {
                        return true;
                    }
                }
            }
        }

        // then checking if request is allowed:
        for (String exceptUrl : exceptUrls) {
            if (url.equals(exceptUrl) ||
                    url.matches("^/resources/css/.*") ||
                    url.matches("^/resources/js/.*") ||
                    url.matches("^/resources/img/.*") ||
                    url.matches("^/resources/extlibs/.*")) {
                logger.debug("Interceptor: allowed request");
                return true;
            }
        }

        //wrong request, redirecting to start page:
        logger.debug("Interceptor: invalid request, redirecting to start page...");
        response.sendRedirect(request.getContextPath() + "/startpage");
        return false;
    }
}
