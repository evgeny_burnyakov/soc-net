﻿<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<section class="page" id="searchSection"><h2 class="page">Friends:</h2>
    <div class="form-group">
        <ul class="nav nav-tabs">
            <li role="presentation"><a href="#" class="friendlist" onclick="showFriends()">My friends</a></li>
            <li role="presentation" class="active"><a href="#" class="friendlist">Outcoming friend requests</a></li>
            <li role="presentation"><a href="#" class="friendlist" onclick="inFriendRequests()">Incoming friend
                requests</a></li>
        </ul>

        <div class="table-responsive">
            <table class="table">
                <c:forEach var="account" items="${myFriendRequests}">
                    <tr>
                        <td class="col-md-2" align="center">
                            <img class="img-rounded"
                                 src='<c:if test="${account.photo.filePathString != null}">${pageContext.request.contextPath}${account.photo.filePathString}</c:if><c:if test="${account.photo.filePathString == null}"><c:url value="${defaultPic}"/></c:if>'
                                 height="50px" style="padding: 5px 10px">
                        </td>
                        <td class="col-md-10" style="vertical-align: middle">
                            <a class="login" href="#"
                               onclick="showAccount(${account.id})">${account.firstName} ${account.secondName} ${account.lastName}</a>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>


    </div>
</section>

