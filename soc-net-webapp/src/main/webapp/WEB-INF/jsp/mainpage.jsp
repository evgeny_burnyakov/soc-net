﻿<!DOCTYPE html>
<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html lang="en">
<head>
    <title>Main page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href='<c:url value="/resources/extlibs/bootstrap-3.3.6-dist/css/bootstrap.min.css"/>'
          media="screen">
    <link rel="stylesheet" type="text/css" href='<c:url value="/resources/css/styles2.css" />'>

    <link rel="stylesheet" type="text/css" href='<c:url value="/resources/extlibs/slick/slick.css" />'>
    <link rel="stylesheet" type="text/css" href='<c:url value="/resources/extlibs/slick/slick-theme.css" />'>

    <script src="<c:url value='/resources/extlibs/jquery-2.2.4.js'/>" type="text/javascript"></script>
    <script src="<c:url value='/resources/extlibs/bootstrap-3.3.6-dist/js/bootstrap.min.js'/>"></script>
    <script src="<c:url value='/resources/extlibs/jquery.timer.js'/>"></script>
    <script>
        var baseUrl = "<c:url value='/'/>";
        var currentPage = "main";
        var myId = ${sessionScope.get("id")};
        var foreignId = null;
        <c:if test="${foreignId != null}">
        foreignId = ${foreignId};
        </c:if>
    </script>

    <%--this is for update profile page:--%>
    <link rel="stylesheet"
          href="<c:url value='/resources/extlibs/jquery-ui-themes-1.11.4/themes/smoothness/jquery-ui.css'/>">
    <script src="<c:url value='/resources/extlibs/jquery-ui-1.11.4/jquery-ui.js'/>"></script>
    <script src="<c:url value='/resources/extlibs/jquery.maskedinput.js'/>"></script>
    <script src="<c:url value='/resources/js/update.js'/>"></script>


    <script src="<c:url value='/resources/js/main.js'/>"></script>


</head>
<body>
<div class="container">
    <%@include file="includes/header.jsp" %>

    <div class="row">
        <div class="col-sm-2">
            <%@include file="includes/menu.jsp" %>
        </div>

        <div class="col-sm-8" id="mainsection">
            <jsp:include page="${pageToDisplay}"/>
        </div>

        <div class="col-sm-2">
            <%@include file="includes/friendsonline.jsp" %>
        </div>
    </div>

    <%@include file="includes/footer.jsp" %>
</div>

<script type="text/javascript" src="<c:url value='/resources/extlibs/slick/slick.min.js'/>"></script>
</body>
</html>