<!DOCTYPE html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html lang="en">
<head>
    <title>Log in page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href='<c:url value="/resources/extlibs/bootstrap-3.3.6-dist/css/bootstrap.min.css"/>'
          media="screen">
    <link rel="stylesheet" type="text/css" href='<c:url value="/resources/css/styles2.css" />'>

    <script src="<c:url value='/resources/extlibs/jquery-2.2.4.js'/>" type="text/javascript"></script>
    <script src="<c:url value='/resources/extlibs/bootstrap-3.3.6-dist/js/bootstrap.min.js'/>"></script>
    <script src="<c:url value='/resources/js/login.js'/>"></script>
</head>
<body>

<div class="container">
    <%@include file="includes/header2.jsp" %>

    <div class="row">
        <div class="col-sm-12">
            <h3 class="login text-center">Please enter Email and password to log in:</h3>

            <form class="form-horizontal" role="form" name="login" action="<c:url value='/login'/>" method="post"
                  onsubmit="return validateLogin()">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="email">Email:</label>
                    <div class="col-sm-4">
                        <input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
                    </div>
                    <div class="col-sm-4" style="padding-top: 5px">
                        <span id="emailAlert" class="alert">${emailErr}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="pwd">Password:</label>
                    <div class="col-sm-4">
                        <input type="password" class="form-control" id="pwd" name="pass" placeholder="Enter password">
                    </div>
                    <div class="col-sm-4" style="padding-top: 5px">
                        <span id="passAlert" class="alert">${passErr}</span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-4 text-center">
                        <div class="checkbox">
                            <label><input type="checkbox" name="rememberme"> Remember me</label>
                        </div>
                    </div>
                    <div class="col-sm-4">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-4 text-center">
                        <button type="submit" class="btn btn-default" style="width: 100%">Log in</button>
                    </div>
                    <div class="col-sm-4">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-4 text-center">
                        <br><a class="login" href="<c:url value='/registration'/>">Register account</a>
                    </div>
                    <div class="col-sm-4">
                    </div>
                </div>

                <%--<table>--%>
                <%--<tr>--%>
                <%--<td class="left">--%>
                <%--<input class="login" type="text" id="email" name="email" placeholder="Please enter E-mail"--%>
                <%--value="${email}">--%>
                <%--</td>--%>
                <%--<td class="right">--%>
                <%--<span id="emailAlert" class="alert">${emailErr}</span>--%>
                <%--</td>--%>
                <%--</tr>--%>
                <%--<tr>--%>
                <%--<td class="left">--%>
                <%--<input class="login" type="password" id="pass" name="pass"--%>
                <%--placeholder="Please enter password">--%>
                <%--</td>--%>
                <%--<td class="right">--%>
                <%--<span id="passAlert" class="alert">${passErr}</span>--%>
                <%--</td>--%>
                <%--</tr>--%>
                <%--<tr>--%>
                <%--<td class="left">--%>
                <%--<input class="login" type="checkbox" id="rememberme" name="rememberme"><span class="login">Remember me</span>--%>
                <%--</td>--%>
                <%--<td class="right">--%>
                <%--</td>--%>
                <%--</tr>--%>
                <%--<tr>--%>
                <%--<td class="left">--%>
                <%--<input class="login" type="submit" value=" Log In ">--%>
                <%--</td>--%>
                <%--<td class="right"></td>--%>
                <%--</tr>--%>
                <%--<tr>--%>
                <%--<td class="left">--%>
                <%--<span class="login"><a class="login"--%>
                <%--href="<c:url value='/registration'/>">Register account</a></span>--%>
                <%--</td>--%>
                <%--<td class="right"></td>--%>
                <%--</tr>--%>
                <%--</table>--%>
            </form>
        </div>
    </div>
    <%@include file="includes/footer.jsp" %>
</div>

</body>
</html>