<header class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="<c:url value='/'/>" style="padding-left: 10px; padding-top: 10px;">
                <img src='<c:url value="/resources/img/logo.jpg"/>' alt="logo" width="270">
            </a>
        </div>
    </div>
</header>