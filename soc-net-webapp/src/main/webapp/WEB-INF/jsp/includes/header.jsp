<header class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="<c:url value='/'/>" style="padding-left: 10px; padding-top: 10px;">
                <img src='<c:url value="/resources/img/logo.jpg"/>' alt="logo" width="270">
            </a>
        </div>

        <ul class="nav navbar-nav navbar-right">
            <li><a href="<c:url value='/logout'/>" style="color: black;">Log out
                <img src='<c:url value="/resources/img/logout.png" />' alt="logo" width="20"></a>
            </li>
        </ul>

        <form class="navbar-form navbar-right" role="search" onsubmit="return false">
            <div class="form-group">
                <input id="search" type="text" class="form-control" placeholder="Search" autocomplete="off">
            </div>
        </form>

    </div>
</header>