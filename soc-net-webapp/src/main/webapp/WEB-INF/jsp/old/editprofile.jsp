﻿<!DOCTYPE html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html lang="en">
<head>
    <title>Edit profile</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet"
          href="<c:url value='/resources/extlibs/jquery-ui-themes-1.11.4/themes/smoothness/jquery-ui.css'/>">
    <link rel="stylesheet" href='<c:url value="/resources/extlibs/bootstrap-3.3.6-dist/css/bootstrap.min.css"/>'
          media="screen">
    <link rel="stylesheet" type="text/css" href='<c:url value="/resources/css/styles2.css" />'>

    <script src="<c:url value='/resources/extlibs/jquery-2.2.4.js'/>" type="text/javascript"></script>
    <script src="<c:url value='/resources/extlibs/jquery-ui-1.11.4/jquery-ui.js'/>"></script>
    <script src="<c:url value='/resources/extlibs/bootstrap-3.3.6-dist/js/bootstrap.min.js'/>"></script>
    <script src="<c:url value='/resources/extlibs/jquery.maskedinput.js'/>" type="text/javascript"></script>

    <script src="<c:url value='/resources/js/update.js'/>"></script>
</head>
<body>
<div class="container">
    <%@include file="../includes/header.jsp" %>

    <div class="row">
        <div class="col-sm-2">
            <%@include file="../includes/menu.jsp" %>
        </div>
        <div class="col-sm-8" id="mainsection">
            <section class="page">
                <h2 class="page">Edit profile:</h2>

                <c:if test="${photo != null}">
                    <div class="row">
                        <figure class="edit" id="profileImgFigure">
                            <img id="profileImage" class="img-rounded edit" src='<c:url value="${photo}"/>'
                                 alt="My photo"
                                 width="100"
                                 onmouseover="this.src='<c:url value="/resources/img/remove-icon.png"/>'"
                                 onmouseout="this.src='<c:url value="${photo}"/>'"
                                 onclick='
                                         $("#profileImgFigure").html("<img class=\"img-rounded\" src=\"<c:url
                                         value='/resources/img/default.png'/>\" width=\"100\">");
                                         $("#profileImgHide").val("true");
                                         '>
                        </figure>
                    </div>
                </c:if>

                <form class="form-horizontal" role="form" id="updateForm" name="update"
                      action="<c:url value='/updateaccount'/>" method="post"
                      enctype="multipart/form-data" onsubmit="return validateUpdate()">
                    <input type="hidden" id="profileImgHide" name="profileImgHide" value="false">

                    <div class="form-group">
                        <label class="control-label col-sm-3" for="firstName">First name:</label>
                        <div class="col-sm-5">
                            <div class="row">
                                <div class="col-xs-11" style="padding-right: 0px">
                                    <input type="text" class="form-control text-inline" id="firstName" name="firstName"
                                           placeholder="Enter name"
                                           value="${account.firstName}">
                                </div>
                                <div class="col-xs-1" style="padding-left: 0px">
                                    <span class="alert" style="padding-left: 5px">*</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4" style="padding-top: 5px">
                            <span id="firstNameAlert" class="alert">${errorMsgs["firstName"]}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="secondName">Second name:</label>
                        <div class="col-sm-5">
                            <div class="row">
                                <div class="col-xs-11" style="padding-right: 0px">
                                    <input type="text" class="form-control" id="secondName" name="secondName"
                                           placeholder="Enter second name" value="${account.secondName}">
                                </div>
                                <div class="col-xs-1" style="padding-left: 0px">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4" style="padding-top: 5px">
                            <span id="secondNameAlert" class="alert">${errorMsgs["secondName"]}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="lastName">Last name:</label>
                        <div class="col-sm-5">
                            <div class="row">
                                <div class="col-xs-11" style="padding-right: 0px">
                                    <input type="text" class="form-control" id="lastName" name="lastName"
                                           placeholder="Enter last name" value="${account.lastName}">
                                </div>
                                <div class="col-xs-1" style="padding-left: 0px">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4" style="padding-top: 5px">
                            <span id="lastNameAlert" class="alert">${errorMsgs["lastName"]}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="datepicker">Birth date:</label>
                        <div class="col-sm-5">
                            <div class="row">
                                <div class="col-xs-11" style="padding-right: 0px">
                                    <input class="form-control" type="text" name="birthDate"
                                           placeholder="Please choose your birth date"
                                           value="<fmt:formatDate type='date' pattern='dd-MM-yyyy' value='${account.birthDate}'/>"
                                           id="datepicker">
                                </div>
                                <div class="col-xs-1" style="padding-left: 0px">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4" style="padding-top: 5px">
                            <span id="birthDateAlert" class="alert">${errorMsgs["birthDate"]}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="icq">ICQ:</label>
                        <div class="col-sm-5">
                            <div class="row">
                                <div class="col-xs-11" style="padding-right: 0px">
                                    <input type="text" class="form-control" id="icq" name="icq" placeholder="Enter ICQ"
                                           value="${account.icq}">
                                </div>
                                <div class="col-xs-1" style="padding-left: 0px">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4" style="padding-top: 5px">
                            <span id="icqAlert" class="alert">${errorMsgs["icq"]}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="skype">Skype:</label>
                        <div class="col-sm-5">
                            <div class="row">
                                <div class="col-xs-11" style="padding-right: 0px">
                                    <input type="text" class="form-control" id="skype" name="skype"
                                           placeholder="Enter skype"
                                           value="${account.skype}">
                                </div>
                                <div class="col-xs-1" style="padding-left: 0px">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4" style="padding-top: 5px">
                            <span id="skypeAlert" class="alert">${errorMsgs["skype"]}</span>
                        </div>
                    </div>

                    <c:set var="phoneIndex" scope="request" value="${0}"/>
                    <c:forEach var="phone" items="${account.phones}">
                        <div class="form-group" id="div${phoneIndex}">
                            <label class="control-label col-sm-3" for="phone${phoneIndex}">Phone:</label>
                            <div class="col-sm-5">
                                <div class="row">
                                    <div class="col-xs-11" style="padding-right: 0px">
                                        <input type="text" class="form-control" id="phone${phoneIndex}"
                                               name="phones[${phoneIndex}].phoneNumber"
                                               placeholder="Enter phone" value="${phone.phoneNumber}">
                                    </div>
                                    <div class="col-xs-1" style="padding-left: 0px; padding-top: 5px;">
                                        <img class="update" src='<c:url value="/resources/img/del.png"/>' alt="del"
                                             width="20"
                                             height="15"
                                             onclick="deletePhone('div${phoneIndex}', 'phone${phoneIndex}')">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4" style="padding-top: 5px">
                                     <span id="phonesAlert${phoneIndex}"
                                           class="alert"></span>
                            </div>
                        </div>

                        <c:set var="phoneIndex" scope="request" value="${phoneIndex + 1}"/>
                    </c:forEach>

                    <script type="text/javascript">
                        var lastPhoneIndex = ${phoneIndex - 1};
                        var delPicUrl = "<c:url value="/resources/img/del.png"/>";
                    </script>

                    <div class="form-group" id="lastPhone">
                        <label class="control-label col-sm-3" for="newPhone">Add phone:</label>
                        <div class="col-sm-5">
                            <div class="row">
                                <div class="col-xs-11" style="padding-right: 0px">
                                    <input type="text" class="form-control" id="newPhone" name="newPhone"
                                           placeholder="Enter phone and press '+'">
                                    <script>
                                        $(function ($) {
                                            $("#newPhone").mask("+9(999)999-99-99", {placeholder: "+_(___)___-__-__"});
                                        });
                                    </script>
                                </div>
                                <div class="col-xs-1" style="padding-left: 0px; padding-top: 5px;">
                                    <img class="create" src='<c:url value="/resources/img/add.png"/>' alt="del"
                                         width="20"
                                         height="15" onclick="addPhone()">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4" style="padding-top: 5px">
                            <span id="newPhoneAlert" class="alert"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-3" for="pic">Add new picture:</label>
                        <div class="col-sm-5">
                            <div class="row">
                                <div class="col-xs-11" style="padding-right: 0px">
                                    <input type="file" class="form-control" id="pic" name="pic" value="attach a picture"
                                           accept="image/*,image/jpeg">
                                </div>
                                <div class="col-xs-1" style="padding-left: 0px">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4" style="padding-top: 5px">
                            <span id="picAlert" class="alert">${errorMsgs["pass"]}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5 text-center">
                            <div class="row">
                                <div class="col-xs-11" style="padding-right: 0px">
                                    <button type="submit" class="btn btn-default" style="width: 100%">Update account
                                    </button>
                                </div>
                                <div class="col-xs-1" style="padding-left: 0px">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5 text-center">
                            <div class="row">
                                <div class="col-xs-11" style="padding-right: 0px">
                                    <span class="alert">* mandatory fields</span>
                                </div>
                                <div class="col-xs-1" style="padding-left: 0px">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                        </div>
                    </div>
                </form>

            </section>
        </div>

        <div class="col-sm-2">
            <section class="friendlist">
                <h4 class="friendlist">Friends online:</h4>
                <a class="friendlist" href="#"><p class="friendlist">Vasya Pupkin</p></a>
                <a class="friendlist" href="#"><p class="friendlist">Fedor Sumkin</p></a>
                <a class="friendlist" href="#"><p class="friendlist">Gandalf Seryi</p></a>
            </section>
        </div>
    </div>

    <%@include file="../includes/footer.jsp" %>

</div>
</body>
</html>