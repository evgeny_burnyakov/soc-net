<!DOCTYPE html>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html lang="en">
<head>
    <title>Create account</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet"
          href="<c:url value='/resources/extlibs/jquery-ui-themes-1.11.4/themes/smoothness/jquery-ui.css'/>">
    <link rel="stylesheet" href='<c:url value="/resources/extlibs/bootstrap-3.3.6-dist/css/bootstrap.min.css"/>'
          media="screen">
    <link rel="stylesheet" type="text/css" href='<c:url value="/resources/css/styles2.css" />'>

    <script src="<c:url value='/resources/extlibs/jquery-2.2.4.js'/>" type="text/javascript"></script>
    <script src="<c:url value='/resources/extlibs/jquery-ui-1.11.4/jquery-ui.js'/>"></script>
    <script src="<c:url value='/resources/extlibs/bootstrap-3.3.6-dist/js/bootstrap.min.js'/>"></script>
    <script src="<c:url value='/resources/extlibs/jquery.maskedinput.js'/>" type="text/javascript"></script>

    <script src="<c:url value='/resources/js/registration.js'/>"></script>
</head>
<body>
<div class="container">
    <%@include file="includes/header2.jsp" %>

    <div class="row">
        <div class="col-sm-12">
            <h3 class="login text-center">Please enter information below to register account:</h3>

            <form class="form-horizontal" role="form" id="createForm" name="create"
                  action="<c:url value='/createaccount'/>" method="post"
                  enctype="multipart/form-data" onsubmit="return validateFields()">

                <div class="form-group">
                    <label class="control-label col-sm-4" for="firstName">First name:</label>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-xs-11" style="padding-right: 0px">
                                <input type="text" class="form-control text-inline" id="firstName" name="firstName"
                                       placeholder="Enter name"
                                       value="${account.firstName}">
                            </div>
                            <div class="col-xs-1" style="padding-left: 0px">
                                <span class="alert" style="padding-left: 5px">*</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4" style="padding-top: 5px">
                        <span id="firstNameAlert" class="alert">${errorMsgs["firstName"]}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="secondName">Second name:</label>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-xs-11" style="padding-right: 0px">
                                <input type="text" class="form-control" id="secondName" name="secondName"
                                       placeholder="Enter second name" value="${account.secondName}">
                            </div>
                            <div class="col-xs-1" style="padding-left: 0px">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4" style="padding-top: 5px">
                        <span id="secondNameAlert" class="alert">${errorMsgs["secondName"]}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="lastName">Last name:</label>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-xs-11" style="padding-right: 0px">
                                <input type="text" class="form-control" id="lastName" name="lastName"
                                       placeholder="Enter last name" value="${account.lastName}">
                            </div>
                            <div class="col-xs-1" style="padding-left: 0px">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4" style="padding-top: 5px">
                        <span id="lastNameAlert" class="alert">${errorMsgs["lastName"]}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="datepicker">Birth date:</label>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-xs-11" style="padding-right: 0px">
                                <input class="form-control" type="text" name="birthDate"
                                       placeholder="Please choose your birth date"
                                       value="<fmt:formatDate type='date' pattern='dd-MM-yyyy' value='${account.birthDate}'/>"
                                       id="datepicker">
                            </div>
                            <div class="col-xs-1" style="padding-left: 0px">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4" style="padding-top: 5px">
                        <span id="birthDateAlert" class="alert">${errorMsgs["birthDate"]}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="icq">ICQ:</label>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-xs-11" style="padding-right: 0px">
                                <input type="text" class="form-control" id="icq" name="icq" placeholder="Enter ICQ"
                                       value="${account.icq}">
                            </div>
                            <div class="col-xs-1" style="padding-left: 0px">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4" style="padding-top: 5px">
                        <span id="icqAlert" class="alert">${errorMsgs["icq"]}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="icq">Skype:</label>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-xs-11" style="padding-right: 0px">
                                <input type="text" class="form-control" id="skype" name="skype"
                                       placeholder="Enter skype"
                                       value="${account.skype}">
                            </div>
                            <div class="col-xs-1" style="padding-left: 0px">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4" style="padding-top: 5px">
                        <span id="skypeAlert" class="alert">${errorMsgs["skype"]}</span>
                    </div>
                </div>

                <script>
                    var delPicUrl = "<c:url value="/resources/img/del.png"/>";
                </script>

                <c:set var="phoneIndex" scope="request" value="${0}"/>
                <c:forEach var="phone" items="${account.phones}">
                    <div class="form-group" id="div${phoneIndex}">
                        <label class="control-label col-sm-4" for="phone${phoneIndex}">Phone:</label>
                        <div class="col-sm-4">
                            <div class="row">
                                <div class="col-xs-11" style="padding-right: 0px">
                                    <input type="text" class="form-control" id="phone${phoneIndex}"
                                           name="phones[${phoneIndex}].phoneNumber"
                                           placeholder="+_(___)___-__-__" value="${phone.phoneNumber}">
                                    <script>
                                        $(function ($) {
                                            $("#phone${phoneIndex}").mask("+9(999)999-99-99", {placeholder: "+_(___)___-__-__"});
                                        });
                                    </script>
                                </div>
                                <div class="col-xs-1" style="padding-left: 0px; padding-top: 5px;">
                                    <img class="update" src='<c:url value="/resources/img/del.png"/>' alt="del"
                                         width="20"
                                         height="15" onclick="deletePhone('div${phoneIndex}', 'phone${phoneIndex}')">
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4" style="padding-top: 5px">
                            <span id="phonesAlert${phoneIndex}" class="alert"></span>
                        </div>
                    </div>

                    <c:set var="phoneIndex" scope="request" value="${phoneIndex + 1}"/>
                </c:forEach>

                <script>
                    var lastPhoneIndex = ${phoneIndex - 1};
                    var delPicUrl = "<c:url value="/resources/img/del.png"/>";
                </script>

                <div class="form-group" id="lastPhone">
                    <label class="control-label col-sm-4" for="newPhone">Add phone:</label>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-xs-11" style="padding-right: 0px">
                                <input type="text" class="form-control" id="newPhone" name="newPhone"
                                       placeholder="Enter phone and press '+'">
                                <script>
                                    $(function ($) {
                                        $("#newPhone").mask("+9(999)999-99-99", {placeholder: "+_(___)___-__-__"});
                                    });
                                </script>
                            </div>
                            <div class="col-xs-1" style="padding-left: 0px; padding-top: 5px;">
                                <img class="create" src='<c:url value="/resources/img/add.png"/>' alt="del"
                                     width="20"
                                     height="15" onclick="addPhone()">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4" style="padding-top: 5px">
                        <span id="newPhoneAlert" class="alert"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-4" for="email">Email:</label>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-xs-11" style="padding-right: 0px">
                                <input type="email" class="form-control" id="email" name="uniqueField"
                                       placeholder="Enter email" value="${account.uniqueField}">
                            </div>
                            <div class="col-xs-1" style="padding-left: 0px">
                                <span class="alert" style="padding-left: 5px">*</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4" style="padding-top: 5px">
                        <span id="emailAlert" class="alert">${errorMsgs["email"]}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="pass1">Password:</label>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-xs-11" style="padding-right: 0px">
                                <input type="password" class="form-control" id="pass1" name="pass"
                                       placeholder="Enter password">
                            </div>
                            <div class="col-xs-1" style="padding-left: 0px">
                                <span class="alert" style="padding-left: 5px">*</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4" style="padding-top: 5px">
                        <span id="pass1Alert" class="alert">${errorMsgs["pass"]}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="pass2">Re-enter password:</label>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-xs-11" style="padding-right: 0px">
                                <input type="password" class="form-control" id="pass2" name="pass2"
                                       placeholder="Re-enter password">
                            </div>
                            <div class="col-xs-1" style="padding-left: 0px">
                                <span class="alert" style="padding-left: 5px">*</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4" style="padding-top: 5px">
                        <span id="pass2Alert" class="alert">${errorMsgs["pass"]}</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="pic">Attach a picture:</label>
                    <div class="col-sm-4">
                        <div class="row">
                            <div class="col-xs-11" style="padding-right: 0px">
                                <input type="file" class="form-control" id="pic" name="pic" value="attach a picture"
                                       accept="image/*,image/jpeg">
                            </div>
                            <div class="col-xs-1" style="padding-left: 0px">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4" style="padding-top: 5px">
                        <span id="picAlert" class="alert">${errorMsgs["pic"]}</span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-4 text-center">
                        <div class="row">
                            <div class="col-xs-11" style="padding-right: 0px">
                                <button type="submit" class="btn btn-default" style="width: 100%">Create account
                                </button>
                            </div>
                            <div class="col-xs-1" style="padding-left: 0px">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-4 text-center">
                        <div class="row">
                            <div class="col-xs-11" style="padding-right: 0px">
                                <span class="alert">* mandatory fields</span>
                            </div>
                            <div class="col-xs-1" style="padding-left: 0px">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                    </div>
                </div>

            </form>
        </div>
    </div>
    </form>

    </section>

    <%@include file="includes/footer.jsp" %>
</div>

</body>

</html>