﻿<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

    <table class="table-responsive" style="width: 100%">
        <tr>
            <td align="left">
                <h2 class="message">Wall messages:</h2>
            </td>
            <td align="right">
                <button class="btn btn-default btn-sm" onclick="createWallMessage(${account.id})">New wall message</button>
            </td>
        </tr>
    </table>

<c:forEach var="message" items="${messages}">

    <article class="message">
        <c:if test="${message.photo != null}">
        <figure class="message">
            <img class="img-rounded" src="${pageContext.request.contextPath}${message.photo.filePathString}" alt="Message picture" width="250">
        </figure>
        </c:if>

        <div class="message">${message.text}
        </div>
    </article>

</c:forEach>


