﻿<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script>
    initializeGroupsPage();
</script>

<section class="page" id="searchSection">
    <h2 class="page">Group:</h2>

    <div class="table-responsive">
        <h3 class="page text-center">Manage requests:</h3><br>

        <table class="table">
            <c:forEach var="account" items="${accounts}">
                <tr>
                    <td class="col-md-2" align="center">
                        <c:if test="${account.photo.filePathString != null}">
                            <img class="img-rounded"
                                 src="${pageContext.request.contextPath}${account.photo.filePathString}"
                                 height="50px" style="padding: 5px 10px">
                        </c:if>
                        <c:if test="${account.photo.filePathString == null}">
                            <img class="img-rounded" src="<c:url value='${defaultPic}'/>"
                                 height="50px" style="padding: 5px 10px">
                        </c:if>

                    </td>
                    <td class="col-md-8" style="vertical-align: middle">
                        <a class="login" href="#"
                           onclick="showAccount(${account.id})">${account.firstName} ${account.lastName}</a>
                    </td>
                    <td class="col-sm-1" style="vertical-align: middle" align="center">
                        <button type="button" class="btn btn-success btn-xs"
                                onclick="acceptGroupRequest(${group.id}, ${account.id})">Accept
                        </button>
                    </td>
                    <td class="col-sm-1" style="vertical-align: middle" align="center">
                        <button type="button" class="btn btn-danger btn-xs"
                                onclick="declineGroupRequest(${group.id}, ${account.id})">Decline
                        </button>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>

    </div>
</section>

