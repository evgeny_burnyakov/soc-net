﻿<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script>
    currentPage = "foreign";
</script>

<section class="page">
    <h2 class="page">Account info:</h2>
    <div class="row">
        <figure class="page text-center">
            <img class="img-rounded" src='<c:url value="${photo}"/>' alt="My photo" width="180px"><br>
            <c:if test="${foreignId != null}">
                <button id="addFriendBtn" type="button" class="btn btn-default"
                        style="margin-top: 10px; padding: 10px 50px"
                        onclick="addFriendRequest()">Add friend
                </button>
            </c:if>
        </figure>

        <div class="table-responsive">
            <table class="table">
                <tr>
                    <td class="left">First name:</td>
                    <td>${account.firstName}</td>
                </tr>
                <tr>
                    <td class="left">Second name:</td>
                    <td>${account.secondName}</td>
                </tr>
                <tr>
                    <td class="left">Last name:</td>
                    <td>${account.lastName}</td>
                </tr>
                <tr>
                    <td class="left">Birth date:</td>
                    <td><fmt:formatDate type='date' pattern='dd-MM-yyyy' value='${account.birthDate}'/></td>
                </tr>
                <tr>
                    <td class="left">ICQ:</td>
                    <td>${account.icq}</td>
                </tr>
                <tr>
                    <td class="left">Skype:</td>
                    <td>${account.skype}</td>
                </tr>
                <tr>
                    <td class="left">Phone(s):</td>
                    <td>${phones}</td>
                </tr>
                <tr>
                    <td class="left">E-mail:</td>
                    <td>${account.uniqueField}</td>
                </tr>
            </table>
        </div>
    </div>

    <div class="row">
        <c:if test="${friends != null}">
            <h4 class="friends">Friends:</h4>
            <div class="slider variable-width text-center">
                <c:forEach var="account" items="${friends}">
                    <div style="padding-left: 5px; padding-right: 5px"><a class="login" href="#"
                                                                          onclick="showAccount(${account.id})">
                        <img class="mycarousel"
                             src='<c:if test="${account.photo.filePathString != null}">${pageContext.request.contextPath}${account.photo.filePathString}</c:if><c:if test="${account.photo.filePathString == null}"><c:url value="${defaultPic}"/></c:if>'
                             height="100px"
                             alt="My photo">
                            ${account.firstName} ${account.lastName}</a></div>

                </c:forEach>
            </div>

            <script type="text/javascript">
                $(document).ready(function () {
                    $('.variable-width').slick({
                        dots: true,
                        infinite: false,
                        speed: 300,
                        slidesToShow: 5,
                        slidesToScroll: 3,
                        centerMode: true,
                        variableWidth: true,
                        initialSlide: 2
                    });
                });
            </script>
        </c:if>
    </div>
</section>

<section class="message" id="wallMessagesContainer">

    <%@include file="wallmessagesajax.jsp" %>

</section>
