﻿<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script>
    initializeGroupPage(${group.id});
</script>

<section class="page">
    <h2 class="page">Group:</h2>
    <div class="row">
        <div class="col-sm-5 text-center">
            <img class="img-responsive center-block img-rounded" src='<c:url value="${photo}"/>' alt="Group photo"
                 width="220px">
            <c:choose>
                <c:when test="${groupMode == 'NEW'}">
                    <button id="joinGroupBtn" type="button" class="btn btn-default btn-sm"
                            style="margin-top: 10px; margin-bottom: 10px; padding-left: 40px; padding-right: 40px;"
                            onclick="joinGroup(${group.id})">Join group
                    </button>
                </c:when>
                <c:when test="${groupMode == 'REQUESTED'}">
                    <button id="joinGroupBtn" type="button" class="btn btn-default btn-sm"
                            style="margin-top: 10px; margin-bottom: 10px; padding-left: 40px; padding-right: 40px;"
                            disabled>Requested to join
                    </button>
                </c:when>
                <c:otherwise>
                    <button id="joinGroupBtn" type="button" class="btn btn-default btn-sm"
                            style="margin-top: 10px; margin-bottom: 10px; padding-left: 40px; padding-right: 40px;"
                            onclick="inviteFriends(${group.id})">Invite friends
                    </button>
                </c:otherwise>
            </c:choose>
        </div>
        <div class="col-sm-7">
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <td class="left">Name:</td>
                        <td>${group.uniqueField}</td>
                    </tr>
                    <tr>
                        <td class="left">Description:</td>
                        <td>${group.description}</td>
                    </tr>
                    <tr>
                        <td class="left">Type:</td>
                        <c:if test="${group.isOpen}">
                            <td>Open group</td>
                        </c:if>
                        <c:if test="${!group.isOpen}">
                            <td>Closed group</td>
                        </c:if>
                    </tr>
                    <tr>
                        <td class="left">Creation date:</td>
                        <td><fmt:formatDate type='date' pattern='dd-MM-yyyy' value='${group.registrationDate}'/></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <c:if test="${groupMembers != null}">
            <h4 class="friends">Group members:</h4>
            <div class="slider variable-width text-center">
                <c:forEach var="groupMember" items="${groupMembers}">
                    <div style="padding-left: 5px; padding-right: 5px"><a class="login" href="#"
                                                                          onclick="showAccount(${groupMember.account.id})">
                        <img class="mycarousel"
                             src='<c:if test="${groupMember.account.photo.filePathString != null}">${pageContext.request.contextPath}${groupMember.account.photo.filePathString}</c:if><c:if test="${groupMember.account.photo.filePathString == null}"><c:url value="${defaultPic}"/></c:if>'
                             height="100px"
                             alt="My photo">
                            ${groupMember.account.firstName} ${groupMember.account.lastName}</a></div>

                </c:forEach>
            </div>

            <script type="text/javascript">
                $(document).ready(function () {
                    $('.variable-width').slick({
                        dots: true,
                        infinite: false,
                        speed: 300,
                        slidesToShow: 5,
                        slidesToScroll: 3,
                        centerMode: true,
                        variableWidth: true,
                        initialSlide: 2
                    });
                });
            </script>
        </c:if>

    </div>
    <c:if test="${groupMode == 'ADMIN' || groupMode == 'MODERATOR'}">
        <div class="row text-center">
            <br><h4 class="friends">Group management:</h4>
            <c:if test="${groupMode == 'ADMIN'}">
                <button id="editGroupBtn" type="button" class="btn btn-default btn-sm"
                        style="margin-top: 10px; padding: 10px 50px"
                        onclick="editGroup(${group.id})">Edit group
                </button>
                <c:if test="${!group.isOpen}">
                    <button id="editModeratorsBtn" type="button" class="btn btn-default btn-sm"
                            style="margin-top: 10px; padding: 10px 50px"
                            onclick="editModerators(${group.id})">Edit moderators
                    </button>
                </c:if>
            </c:if>
            <c:if test="${!group.isOpen}">
                <button id="manageRequestsBtn" type="button" class="btn btn-default btn-sm"
                        style="margin-top: 10px; padding: 10px 50px"
                        onclick="manageGroupRequests(${group.id})">Manage requests
                </button>
            </c:if>
        </div>
    </c:if>

</section>

