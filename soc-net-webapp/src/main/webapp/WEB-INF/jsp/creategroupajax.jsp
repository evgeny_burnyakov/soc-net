﻿<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<section class="page">
    <h2 class="page">Groups:</h2>
    <div class="form-group">
        <ul class="nav nav-tabs">
            <li role="presentation"><a href="#" class="friendlist" onclick="showGroups()">My groups</a></li>
            <li role="presentation"><a href="#" class="friendlist" onclick="showRequests()">Requests to join group</a></li>
            <li role="presentation"><a href="#" class="friendlist" onclick="showInvites()">Invites to join group</a></li>
            <li role="presentation" class="active"><a href="#" class="friendlist">Create group</a></li>
        </ul>

        <br><h3 class="page text-center">Create group:</h3><br>

        <form class="form-horizontal" role="form" id="createGroupForm" name="createGroup"
              action="<c:url value='/creategroup'/>" method="post"
              enctype="multipart/form-data" onsubmit="return validateGroupCreate()">

            <div class="form-group">
                <label class="control-label col-sm-3" for="groupName">Group name:</label>
                <div class="col-sm-5">
                    <div class="row">
                        <div class="col-xs-11" style="padding-right: 0px">
                            <input type="text" class="form-control text-inline" id="groupName" name="uniqueField"
                                   placeholder="Enter group name"
                                   value="${group.uniqueField}">
                        </div>
                        <div class="col-xs-1" style="padding-left: 0px">
                            <span class="alert" style="padding-left: 5px">*</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4" style="padding-top: 5px">
                    <span id="groupNameAlert" class="alert">${errorMsgs["groupName"]}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="description">Group description:</label>
                <div class="col-sm-5">
                    <div class="row">
                        <div class="col-xs-11" style="padding-right: 0px">
                            <input type="text" class="form-control" id="description" name="description"
                                   placeholder="Group description" value="${group.description}">
                        </div>
                        <div class="col-xs-1" style="padding-left: 0px">
                        </div>
                    </div>
                </div>
                <div class="col-sm-4" style="padding-top: 5px">
                    <span id="descriptionAlert" class="alert">${errorMsgs["description"]}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="pic">Add picture:</label>
                <div class="col-sm-5">
                    <div class="row">
                        <div class="col-xs-11" style="padding-right: 0px">
                            <input type="file" class="form-control" id="pic" name="photo" value="attach a picture"
                                   accept="image/*,image/jpeg">
                        </div>
                        <div class="col-xs-1" style="padding-left: 0px">
                        </div>
                    </div>
                </div>
                <div class="col-sm-4" style="padding-top: 5px">
                    <span id="photoAlert" class="alert">${errorMsgs["photo"]}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="isopen"></label>
                <div class="col-sm-5">
                    <div class="row">
                        <div class="col-xs-11 text-center" style="padding-right: 0px">
                            <input type="checkbox" id="isopen" name="isOpen"
                                   value="true">
                            <input type="hidden" value="on" name="_isOpen"/>
                            Open group
                        </div>
                        <div class="col-xs-1" style="padding-left: 0px">
                        </div>
                    </div>
                </div>
                <div class="col-sm-4" style="padding-top: 5px">
                    <span id="isOpenAlert" class="alert">${errorMsgs["isOpen"]}</span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-5 text-center">
                    <div class="row">
                        <div class="col-xs-11" style="padding-right: 0px">
                            <button type="submit" class="btn btn-default" style="width: 100%">Create group
                            </button>
                        </div>
                        <div class="col-xs-1" style="padding-left: 0px">
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-5 text-center">
                    <div class="row">
                        <div class="col-xs-11" style="padding-right: 0px">
                            <span class="alert">* mandatory fields</span>
                        </div>
                        <div class="col-xs-1" style="padding-left: 0px">
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                </div>
            </div>
        </form>
    </div>
</section>
