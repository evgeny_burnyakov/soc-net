﻿<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<section class="page">
    <h2 class="page">Account info:</h2>
    <figure class="page">
        <img class="img-rounded" src='<c:url value="${photo}"/>' alt="My photo" width="220px">
    </figure>

    <div class="table-responsive">
        <table class="table">
            <tr>
                <td class="left">First name:</td>
                <td>${account.firstName}</td>
            </tr>
            <tr>
                <td class="left">Second name:</td>
                <td>${account.secondName}</td>
            </tr>
            <tr>
                <td class="left">Last name:</td>
                <td>${account.lastName}</td>
            </tr>
            <tr>
                <td class="left">Birth date:</td>
                <td><fmt:formatDate type='date' pattern='dd-MM-yyyy' value='${account.birthDate}'/></td>
            </tr>
            <tr>
                <td class="left">ICQ:</td>
                <td>${account.icq}</td>
            </tr>
            <tr>
                <td class="left">Skype:</td>
                <td>${account.skype}</td>
            </tr>
            <tr>
                <td class="left">Phone(s):</td>
                <td>${phones}</td>
            </tr>
            <tr>
                <td class="left">E-mail:</td>
                <td>${account.uniqueField}</td>
            </tr>
        </table>
    </div>
</section>

<section class="message" id="wallMessagesContainer">

    <%@include file="wallmessagesajax.jsp" %>

</section>
