﻿<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<section class="page">
    <h2 class="page">Edit profile:</h2>

    <c:if test="${photo != null}">
        <div class="row text-center" id="profileImgFigure" style="margin-bottom: 20px;">
                <img id="profileImage" class="img-rounded img-responsive center-block update" src='<c:url value="${photo}"/>'
                     alt="My photo"
                     width="150"
                     onmouseover='$("#delProfileImg").css("visibility", "visible")'
                     onmouseout='$("#delProfileImg").css("visibility", "hidden")'
                     onclick='
                             $("#profileImgFigure").html("<img class=\"img-rounded\" src=\"<c:url
                             value='/resources/img/default.png'/>\" width=\"150\"><br>");
                             $("#profileImgHide").val("true");
                             '>
            <span id="delProfileImg" style="visibility: hidden; color: red">Delete image</span>
        </div>
    </c:if>

    <form class="form-horizontal" role="form" id="updateForm" name="update"
          action="<c:url value='/updateaccount'/>" method="post"
          enctype="multipart/form-data" onsubmit="return validateUpdate()">
        <input type="hidden" id="profileImgHide" name="profileImgHide" value="false">

        <div class="form-group">
            <label class="control-label col-sm-3" for="firstName">First name:</label>
            <div class="col-sm-5">
                <div class="row">
                    <div class="col-xs-11" style="padding-right: 0px">
                        <input type="text" class="form-control text-inline" id="firstName" name="firstName"
                               placeholder="Enter name"
                               value="${account.firstName}">
                    </div>
                    <div class="col-xs-1" style="padding-left: 0px">
                        <span class="alert" style="padding-left: 5px">*</span>
                    </div>
                </div>
            </div>
            <div class="col-sm-4" style="padding-top: 5px">
                <span id="firstNameAlert" class="alert">${errorMsgs["firstName"]}</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="secondName">Second name:</label>
            <div class="col-sm-5">
                <div class="row">
                    <div class="col-xs-11" style="padding-right: 0px">
                        <input type="text" class="form-control" id="secondName" name="secondName"
                               placeholder="Enter second name" value="${account.secondName}">
                    </div>
                    <div class="col-xs-1" style="padding-left: 0px">
                    </div>
                </div>
            </div>
            <div class="col-sm-4" style="padding-top: 5px">
                <span id="secondNameAlert" class="alert">${errorMsgs["secondName"]}</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="lastName">Last name:</label>
            <div class="col-sm-5">
                <div class="row">
                    <div class="col-xs-11" style="padding-right: 0px">
                        <input type="text" class="form-control" id="lastName" name="lastName"
                               placeholder="Enter last name" value="${account.lastName}">
                    </div>
                    <div class="col-xs-1" style="padding-left: 0px">
                    </div>
                </div>
            </div>
            <div class="col-sm-4" style="padding-top: 5px">
                <span id="lastNameAlert" class="alert">${errorMsgs["lastName"]}</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="datepicker">Birth date:</label>
            <div class="col-sm-5">
                <div class="row">
                    <div class="col-xs-11" style="padding-right: 0px">
                        <input class="form-control" type="text" name="birthDate"
                               placeholder="Please choose your birth date"
                               value="<fmt:formatDate type='date' pattern='dd-MM-yyyy' value='${account.birthDate}'/>"
                               id="datepicker">
                    </div>
                    <div class="col-xs-1" style="padding-left: 0px">
                    </div>
                </div>
            </div>
            <div class="col-sm-4" style="padding-top: 5px">
                <span id="birthDateAlert" class="alert">${errorMsgs["birthDate"]}</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="icq">ICQ:</label>
            <div class="col-sm-5">
                <div class="row">
                    <div class="col-xs-11" style="padding-right: 0px">
                        <input type="text" class="form-control" id="icq" name="icq" placeholder="Enter ICQ"
                               value="${account.icq}">
                    </div>
                    <div class="col-xs-1" style="padding-left: 0px">
                    </div>
                </div>
            </div>
            <div class="col-sm-4" style="padding-top: 5px">
                <span id="icqAlert" class="alert">${errorMsgs["icq"]}</span>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-3" for="skype">Skype:</label>
            <div class="col-sm-5">
                <div class="row">
                    <div class="col-xs-11" style="padding-right: 0px">
                        <input type="text" class="form-control" id="skype" name="skype"
                               placeholder="Enter skype"
                               value="${account.skype}">
                    </div>
                    <div class="col-xs-1" style="padding-left: 0px">
                    </div>
                </div>
            </div>
            <div class="col-sm-4" style="padding-top: 5px">
                <span id="skypeAlert" class="alert">${errorMsgs["skype"]}</span>
            </div>
        </div>

        <c:set var="phoneIndex" scope="request" value="${0}"/>
        <c:forEach var="phone" items="${account.phones}">
            <div class="form-group" id="div${phoneIndex}">
                <label class="control-label col-sm-3" for="phone${phoneIndex}">Phone:</label>
                <div class="col-sm-5">
                    <div class="row">
                        <div class="col-xs-11" style="padding-right: 0px">
                            <input type="text" class="form-control" id="phone${phoneIndex}"
                                   name="phones[${phoneIndex}].phoneNumber"
                                   placeholder="Enter phone" value="${phone.phoneNumber}">
                        </div>
                        <div class="col-xs-1" style="padding-left: 0px; padding-top: 5px;">
                            <img class="update" src='<c:url value="/resources/img/del.png"/>' alt="del"
                                 width="20"
                                 height="15"
                                 onclick="deletePhone('div${phoneIndex}', 'phone${phoneIndex}')">
                        </div>
                    </div>
                </div>

                <div class="col-sm-4" style="padding-top: 5px">
                                     <span id="phonesAlert${phoneIndex}"
                                           class="alert"></span>
                </div>
            </div>

            <c:set var="phoneIndex" scope="request" value="${phoneIndex + 1}"/>
        </c:forEach>

        <script type="text/javascript">
            var lastPhoneIndex = ${phoneIndex - 1};
            var delPicUrl = "<c:url value="/resources/img/del.png"/>";
        </script>

        <div class="form-group" id="lastPhone">
            <label class="control-label col-sm-3" for="newPhone">Add phone:</label>
            <div class="col-sm-5">
                <div class="row">
                    <div class="col-xs-11" style="padding-right: 0px">
                        <input type="text" class="form-control" id="newPhone" name="newPhone"
                               placeholder="Enter phone and press '+'">
                        <script>
                            $(function ($) {
                                $("#newPhone").mask("+9(999)999-99-99", {placeholder: "+_(___)___-__-__"});
                            });
                        </script>
                    </div>
                    <div class="col-xs-1" style="padding-left: 0px; padding-top: 5px;">
                        <img class="create" src='<c:url value="/resources/img/add.png"/>' alt="del"
                             width="20"
                             height="15" onclick="addPhone()">
                    </div>
                </div>
            </div>

            <div class="col-sm-4" style="padding-top: 5px">
                <span id="newPhoneAlert" class="alert"></span>
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-3" for="pic">Add new picture:</label>
            <div class="col-sm-5">
                <div class="row">
                    <div class="col-xs-11" style="padding-right: 0px">
                        <input type="file" class="form-control" id="pic" name="pic" value="attach a picture"
                               accept="image/*,image/jpeg">
                    </div>
                    <div class="col-xs-1" style="padding-left: 0px">
                    </div>
                </div>
            </div>
            <div class="col-sm-4" style="padding-top: 5px">
                <span id="picAlert" class="alert">${errorMsgs["pass"]}</span>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-5 text-center">
                <div class="row">
                    <div class="col-xs-11" style="padding-right: 0px">
                        <button type="submit" class="btn btn-default" style="width: 100%">Update account
                        </button>
                    </div>
                    <div class="col-xs-1" style="padding-left: 0px">
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-5 text-center">
                <div class="row">
                    <div class="col-xs-11" style="padding-right: 0px">
                        <span class="alert">* mandatory fields</span>
                    </div>
                    <div class="col-xs-1" style="padding-left: 0px">
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
            </div>
        </div>
    </form>
</section>
