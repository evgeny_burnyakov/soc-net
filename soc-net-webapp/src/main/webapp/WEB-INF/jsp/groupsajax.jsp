﻿<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script>
    initializeGroupsPage();
</script>

<section class="page" id="searchSection">
    <h2 class="page">Groups:</h2>
    <div class="form-group">
        <ul class="nav nav-tabs">
            <li role="presentation" class="active"><a href="#" class="friendlist">My groups</a></li>
            <li role="presentation"><a href="#" class="friendlist" onclick="showRequests()">Requests to join group</a>
            </li>
            <li role="presentation"><a href="#" class="friendlist" onclick="showInvites()">Invites to join group</a>
            </li>
            <li role="presentation"><a href="#" class="friendlist" onclick="createGroup()">Create group</a></li>
        </ul>

        <div class="table-responsive">
            <table class="table">
                <c:forEach var="groupMember" items="${groupMembers}">
                    <tr>
                        <td class="col-md-2" align="center">
                            <c:if test="${groupMember.group.photo.filePathString != null}">
                                <img class="img-rounded"
                                     src="${pageContext.request.contextPath}${groupMember.group.photo.filePathString}"
                                     height="50px" style="padding: 5px 10px">
                            </c:if>
                            <c:if test="${groupMember.group.photo.filePathString == null}">
                                <img class="img-rounded" src="<c:url value='${defaultPic}'/>"
                                     height="50px" style="padding: 5px 10px">
                            </c:if>

                        </td>
                        <td class="col-md-7" style="vertical-align: middle">
                            <a class="login" href="#"
                               onclick="showGroup(${groupMember.group.id})">${groupMember.group.uniqueField}</a>
                        </td>
                        <td class="col-md-1" style="vertical-align: middle" align="center">
                            <c:if test="${groupMember.group.isOpen}">
                                <span class="label label-success">Open group</span>
                            </c:if>
                            <c:if test="${!groupMember.group.isOpen}">
                                <span class="label label-danger">Closed group</span>
                            </c:if>
                        </td>
                        <td class="col-md-1" style="vertical-align: middle" align="center">
                            <c:choose>
                                <c:when test="${groupMember.group.creator.id == groupMember.account.id}">
                                    <span class="label label-danger">Admin</span>
                                </c:when>
                                <c:otherwise>
                                    <c:if test="${groupMember.role == 'MODERATOR'}">
                                        <span class="label label-warning">Moderator</span>
                                    </c:if>
                                    <c:if test="${groupMember.role == 'USER'}">
                                        <span class="label label-success">User</span>
                                    </c:if>
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td class="col-sm-1" style="vertical-align: middle">
                            <c:if test="${groupMember.group.creator.id != groupMember.account.id}">
                                <button type="button" class="btn btn-default btn-xs"
                                        onclick="leaveGroup(${groupMember.group.id})">Leave group
                                </button>
                            </c:if>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>

    </div>
</section>

