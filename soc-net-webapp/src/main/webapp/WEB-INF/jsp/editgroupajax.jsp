﻿<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<section class="page">
    <h2 class="page">Group:</h2>
    <div class="form-group">
        <h3 class="page text-center">Edit group:</h3><br>
        <c:if test="${group.photo.filePathString != null}">
            <div class="row text-center" id="groupImgFigure" style="margin-bottom: 20px;">
                <img id="groupImage" class="img-rounded img-responsive center-block update"
                     src='${pageContext.request.contextPath}${group.photo.filePathString}'
                     alt="My photo" width="150" style="text-align:center; display:block"
                     onmouseover='$("#delGroupImg").css("visibility", "visible")'
                     onmouseout='$("#delGroupImg").css("visibility", "hidden")'
                     onclick='
                             $("#groupImgFigure").html("<img class=\"img-rounded center-block\" src=\"<c:url
                             value='/resources/img/default.png'/>\" width=\"150\"><br>");
                             $("#groupImgHide").val("true");
                             '>
                <span id="delGroupImg" style="visibility: hidden; color: red">Delete image</span>
            </div>

        </c:if>
        <form class="form-horizontal" role="form" id="updateGroupForm" name="updateGroup"
              method="post"
              enctype="multipart/form-data" onsubmit="return validateGroupUpdate()">
            <input type="hidden" id="groupImgHide" name="groupImgHide" value="false">
            <input type="hidden" name="id" value="${group.id}">

            <div class="form-group">
                <label class="control-label col-sm-3" for="groupName">Group name:</label>
                <div class="col-sm-5">
                    <div class="row">
                        <div class="col-xs-11" style="padding-right: 0px">
                            <input type="text" class="form-control text-inline" id="groupName" name="uniqueField"
                                   placeholder="Enter group name"
                                   value="${group.uniqueField}">
                        </div>
                        <div class="col-xs-1" style="padding-left: 0px">
                            <span class="alert" style="padding-left: 5px">*</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4" style="padding-top: 5px">
                    <span id="groupNameAlert" class="alert">${errorMsgs["groupName"]}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="description">Group description:</label>
                <div class="col-sm-5">
                    <div class="row">
                        <div class="col-xs-11" style="padding-right: 0px">
                            <input type="text" class="form-control" id="description" name="description"
                                   placeholder="Group description" value="${group.description}">
                        </div>
                        <div class="col-xs-1" style="padding-left: 0px">
                        </div>
                    </div>
                </div>
                <div class="col-sm-4" style="padding-top: 5px">
                    <span id="descriptionAlert" class="alert">${errorMsgs["description"]}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="pic">Add new picture:</label>
                <div class="col-sm-5">
                    <div class="row">
                        <div class="col-xs-11" style="padding-right: 0px">
                            <input type="file" class="form-control" id="pic" name="photo" value="attach a picture"
                                   accept="image/*,image/jpeg">
                        </div>
                        <div class="col-xs-1" style="padding-left: 0px">
                        </div>
                    </div>
                </div>
                <div class="col-sm-4" style="padding-top: 5px">
                    <span id="photoAlert" class="alert">${errorMsgs["photo"]}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="isopen"></label>
                <div class="col-sm-5">
                    <div class="row">
                        <div class="col-xs-11 text-center" style="padding-right: 0px">
                            <input type="checkbox" id="isopen" name="isOpen"
                                   value="true"
                                   <c:if test="${group.isOpen}">checked</c:if> >
                            <input type="hidden" value="on" name="_isOpen"/>
                            Open group
                        </div>
                        <div class="col-xs-1" style="padding-left: 0px">
                        </div>
                    </div>
                </div>
                <div class="col-sm-4" style="padding-top: 5px">
                    <span id="isOpenAlert" class="alert">${errorMsgs["isOpen"]}</span>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-5 text-center">
                    <div class="row">
                        <div class="col-xs-11" style="padding-right: 0px">
                            <button type="submit" class="btn btn-default" style="width: 100%">Update group
                            </button>
                        </div>
                        <div class="col-xs-1" style="padding-left: 0px">
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-5 text-center">
                    <div class="row">
                        <div class="col-xs-11" style="padding-right: 0px">
                            <span class="alert">* mandatory fields</span>
                        </div>
                        <div class="col-xs-1" style="padding-left: 0px">
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                </div>
            </div>
        </form>
    </div>
    <br><br>
    <div class="row text-center">
        <button type="button" class="btn btn-danger btn-xs" style="width: 100px" onclick="deleteGroup(${group.id})">Delete group</button>
    </div>

</section>
