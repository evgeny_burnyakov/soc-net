﻿<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<section class="page" id="searchSection"><h2 class="page">Friends:</h2>
    <div class="form-group">
        <ul class="nav nav-tabs">
            <li role="presentation" class="active"><a href="#" class="friendlist">My friends</a></li>
            <li role="presentation"><a href="#" class="friendlist" onclick="outFriendRequests()">Outcoming friend
                requests</a></li>
            <li role="presentation"><a href="#" class="friendlist" onclick="inFriendRequests()">Incoming friend
                requests</a></li>
        </ul>

        <div class="table-responsive">
            <table class="table">
                <c:forEach var="account" items="${friends}">
                    <tr>
                        <td class="col-md-2" align="center">
                            <c:if test="${account.photo.filePathString != null}">
                                <img class="img-rounded" src="${pageContext.request.contextPath}${account.photo.filePathString}"
                                     height="50px" style="padding: 5px 10px">
                            </c:if>
                            <c:if test="${account.photo.filePathString == null}">
                                <img class="img-rounded" src="<c:url value='${defaultPic}'/>"
                                     height="50px" style="padding: 5px 10px">
                            </c:if>

                        </td>
                        <td class="col-md-9" style="vertical-align: middle">
                            <a class="login" href="#"
                               onclick="showAccount(${account.id})">${account.firstName} ${account.secondName} ${account.lastName}</a>
                        </td>
                        <td class="col-sm-1" style="vertical-align: middle">
                                <button type="button" class="btn btn-default btn-xs"
                                        onclick="removeFromFriends(${account.id})">Remove from friends
                                </button>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>

    </div>
</section>

