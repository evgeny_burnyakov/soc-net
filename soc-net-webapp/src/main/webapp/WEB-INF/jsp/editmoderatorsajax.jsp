﻿<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script>
    initializeGroupsPage();
</script>

<section class="page" id="searchSection">
    <h2 class="page">Group:</h2>

        <div class="table-responsive">
            <h3 class="page text-center">Edit moderators:</h3><br>

            <table class="table">
                <c:forEach var="member" items="${members}">
                    <tr>
                        <td class="col-md-2" align="center">
                            <c:if test="${member.account.photo.filePathString != null}">
                                <img class="img-rounded"
                                     src="${pageContext.request.contextPath}${member.account.photo.filePathString}"
                                     height="50px" style="padding: 5px 10px">
                            </c:if>
                            <c:if test="${member.account.photo.filePathString == null}">
                                <img class="img-rounded" src="<c:url value='${defaultPic}'/>"
                                     height="50px" style="padding: 5px 10px">
                            </c:if>

                        </td>
                        <td class="col-md-7" style="vertical-align: middle">
                            <a class="login" href="#"
                               onclick="showAccount(${member.account.id})">${member.account.firstName} ${member.account.lastName}</a>
                        </td>
                        <td class="col-md-3" style="vertical-align: middle" align="center">
                            <c:if test="${member.role == 'USER'}">
                                <span class="label label-success">User</span>&nbsp;
                                <button type="button" class="btn btn-default btn-xs"
                                        onclick="switchGroupRole(${group.id}, ${member.account.id})">To moderators
                                </button>
                            </c:if>
                            <c:if test="${member.role == 'MODERATOR'}">
                                <span class="label label-warning">Moderator</span>&nbsp;
                                <button type="button" class="btn btn-default btn-xs"
                                        onclick="switchGroupRole(${group.id}, ${member.account.id})">To users
                                </button>
                            </c:if>
                    </tr>
                </c:forEach>
            </table>
        </div>

    </div>
</section>

