﻿<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="input" uri="http://www.springframework.org/tags/form" %>


<section class="page">
    <h2 class="page">Group:</h2>
    <div class="form-group">

        <div class="table-responsive">
            <br><h3 class="page text-center">Invite friends to group "${group.uniqueField}":</h3><br>
            <form class="form-horizontal" role="form" id="inviteFriendForm" name="inviteFriendForm"
                  method="post">
                <input type="hidden" name="groupId" value="${group.id}">
                <table class="table">
                    <c:forEach var="account" items="${friends}">
                        <tr>
                            <td class="col-md-2" align="center">
                                <c:if test="${account.photo.filePathString != null}">
                                    <img class="img-rounded"
                                         src="${pageContext.request.contextPath}${account.photo.filePathString}"
                                         height="50px" style="padding: 5px 10px">
                                </c:if>
                                <c:if test="${account.photo.filePathString == null}">
                                    <img class="img-rounded" src="<c:url value='${defaultPic}'/>"
                                         height="50px" style="padding: 5px 10px">
                                </c:if>

                            </td>
                            <td class="col-md-8" style="vertical-align: middle">
                                    ${account.firstName} ${account.lastName}
                            </td>
                            <td class="col-md-2" style="vertical-align: middle">
                                <input type="checkbox" name="inviteIds" value="${account.id}">
                            </td>
                        </tr>
                    </c:forEach>
                </table>
                <div class="text-center">
                    <button id="inviteFriendsBtn" type="button" class="btn btn-default btn-sm"
                            style="margin-top: 10px; padding: 10px 50px"
                            onclick="sendInviteFriends()">Invite friends
                    </button>
                </div>
            </form>
        </div>

    </div>
</section>

