﻿<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<h2 class="message">Wall messages:</h2>
<h3 class="page text-center">Create new message:</h3><br>

<form class="form-horizontal" role="form" id="createWallMessageForm" name="createWallMessage"
      action="<c:url value='/postWallMessage'/>" method="post"
      enctype="multipart/form-data" onsubmit="return postWallMessage()">

    <input type="hidden" name="targetId" value="${messageTargetId}">

    <div class="form-group">
        <label class="control-label col-sm-3" for="messageText">Message text:</label>
        <div class="col-sm-5">
            <div class="row">
                <div class="col-xs-11" style="padding-right: 0px">
                    <textarea class="form-control" id="messageText" name="text" rows="5" maxlength="255"></textarea>
                </div>
                <div class="col-xs-1" style="padding-left: 0px">
                </div>
            </div>
        </div>
        <div class="col-sm-4" style="padding-top: 5px">
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-3" for="pic">Add picture:</label>
        <div class="col-sm-5">
            <div class="row">
                <div class="col-xs-11" style="padding-right: 0px">
                    <input type="file" class="form-control" id="pic" name="photo" value="attach a picture"
                           accept="image/*,image/jpeg">
                </div>
                <div class="col-xs-1" style="padding-left: 0px">
                </div>
            </div>
        </div>
        <div class="col-sm-4" style="padding-top: 5px">
            <span id="photoAlert" class="alert">${errorMsgs["photo"]}</span>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-5 text-center">
            <div class="row">
                <div class="col-xs-11" style="padding-right: 0px">
                    <button type="submit" class="btn btn-default" style="width: 100%">Post message
                    </button>
                </div>
                <div class="col-xs-1" style="padding-left: 0px">
                </div>
            </div>
        </div>
        <div class="col-sm-4">
        </div>
    </div>
</form>
