﻿<%@ page language="java" pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<section class="page" id="searchSection"><h2 class="page">Friends:</h2>
    <div class="form-group">
        <ul class="nav nav-tabs">
            <li role="presentation"><a href="#" class="friendlist" onclick="showFriends()">My friends</a></li>
            <li role="presentation"><a href="#" class="friendlist" onclick="outFriendRequests()">Outcoming friend
                requests</a></li>
            <li role="presentation" class="active"><a href="#" class="friendlist" onclick="inFriendRequests()">Incoming
                friend requests</a></li>
        </ul>

        <div class="table-responsive">
            <table class="table">
                <c:forEach var="account" items="${inFriendRequests}">
                    <tr>
                        <td class="col-md-2"  align="center">
                            <img class="img-rounded"
                                 src='<c:if test="${account.photo.filePathString != null}">${pageContext.request.contextPath}${account.photo.filePathString}</c:if><c:if test="${account.photo.filePathString == null}"><c:url value="${defaultPic}"/></c:if>'
                                 height="50px" style="padding: 5px 10px">
                        </td>
                        <td class="col-md-6" style="vertical-align: middle">
                            <a class="login" href="#"
                               onclick="showAccount(${account.id})">${account.firstName} ${account.secondName} ${account.lastName}</a>
                        </td>
                        <td class="col-md-4" style="vertical-align: middle">
                            <button type="button" class="btn btn-success btn-sm"
                                    onclick="approveFriendRequest(${account.id})">Accept
                            </button>
                            <button type="button" class="btn btn-danger btn-sm"
                                    onclick="declineFriendRequest(${account.id})">Decline
                            </button>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>


    </div>
</section>

