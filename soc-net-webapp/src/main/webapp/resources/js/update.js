$(function () {
    $("#datepicker").datepicker({
        dateFormat: "dd-mm-yy",
        changeMonth: true,
        changeYear: true,
        yearRange: "1900:+00"
    });
});

function validateUpdate() {
    var isPassed = true;

    // clear previous alerts:
    $("#firstNameAlert").text("");
    $("#icqAlert").text("");

    //validation:
    var firstName = $("input[name='firstName']").val();
    if (firstName == null || firstName == "") {
        $("#firstNameAlert").text("First name should be filled.");
        isPassed = false;
    }

    var icq = $("input[name='icq']").val();
    var icqPattern = /^[0-9]+$/;
    if (icq != null && icq != "" && !icqPattern.test(icq)) {
        $("#icqAlert").text("ICQ should contain only digits.");
        isPassed = false;
    }

    var phonesPattern = /^[0-9\+\-\(\),\s]+$/;
    for (var i = 0; i <= lastPhoneIndex; i++) {
        var phone = $("#phone" + i).val();
        if (phone != null && phone != "" && !phonesPattern.test(phone)) {
            $("#phonesAlert" + i).text("phone field may contain only digits and '+-(),'.");
            isPassed = false;
        } else {
            $("#phonesAlert" + i).text("");
        }
    }

    var newPhone = $("#newPhone").val();
    if (newPhone != null && newPhone != "") {
        $("#newPhoneAlert").text("Please click '+' or clear this field.");
        isPassed = false;
    } else {
        $("#newPhoneAlert").text("");
    }

    if (isPassed && confirm("Do you really want to apply the changes?") == false) {
        isPassed = false;
    }

    if (isPassed) {
        var formData = new FormData($('#updateForm')[0]);
        $.ajax({
            url: baseUrl + "updateaccount",
            method: "POST",
            data: formData,
            success: function (result) {
                $("#mainsection").html(result);
            },
            contentType: false,
            processData: false,
            cache: false,
            dataType: "html"
        });
        currentPage = "main";
    }
    return false;
}

function addPhone() {
    lastPhoneIndex++;

    $("#lastPhone").before(
        "<div class=\"form-group\" id=\"div" + lastPhoneIndex + "\">" +
        "<label class=\"control-label col-sm-3\" for=\"phone" + lastPhoneIndex + "\">Phone:</label>" +
        "<div class=\"col-sm-5\">" +
        "<div class=\"row\">" +
        "<div class=\"col-xs-11\" style=\"padding-right: 0px\">" +
        "<input type=\"text\" class=\"form-control\" id=\"phone" + lastPhoneIndex + "\" placeholder=\"+_(___)___-__-__\" " +
        "name=\"phones[" + lastPhoneIndex + "].phoneNumber\" value=\"" + $("#newPhone").val() + "\">" +
        "<script>" +
        "$(function($){" +
        "$(\"#phone" + lastPhoneIndex + "\").mask(\"+9(999)999-99-99\", {placeholder: \"+_(___)___-__-__\"});" +
        "});" +
        "</script>" +
        "</div>" +
        "<div class=\"col-xs-1\" style=\"padding-left: 0px; padding-top: 5px;\">" +
        "<img src=\"" + delPicUrl + "\" alt=\"del\" class=\"update\" width=\"20\" height=\"15\" onclick='deletePhone(\"div" + lastPhoneIndex + "\", \"phone" + lastPhoneIndex + "\")'></div></div></div>" +
        "<div class=\"col-sm-4\" style=\"padding-top: 5px\">" +
        "<span id=\"phonesAlert" + lastPhoneIndex + "\" class=\"alert\"></span></div></div>"
    );
    $("#newPhone").val("");
}

function deletePhone(idToHide, idToDisable) {
    if (confirm("Do you really want to delete this phone?") == true) {
        $("#" + idToHide).hide();
        $("#" + idToDisable).prop("disabled", true);
    }
}


