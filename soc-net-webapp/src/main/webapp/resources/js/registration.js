$(function () {
    $("#datepicker").datepicker({
        dateFormat: "dd-mm-yy",
        changeMonth: true,
        changeYear: true,
        yearRange: "1900:+00"
    });
});

function validateFields() {
    var isPassed = true;

    // clear previous alerts:
    $("#firstNameAlert").text("");
    $("#emailAlert").text("");
    $("#pass1Alert").text("");
    $("#pass2Alert").text("");
    $("#icqAlert").text("");
    $("#phonesAlert").text("");

    //validation:

    var firstName = $("input[name='firstName']").val();
    if (firstName == null || firstName == "") {
        $("#firstNameAlert").text("First name should be filled.");
        isPassed = false;
    }

    var icq = $("input[name='icq']").val();
    var icqPattern = /^[0-9]+$/;
    if (icq != null && icq != "" && !icqPattern.test(icq)) {
        $("#icqAlert").text("ICQ can contain only digits.");
        isPassed = false;
    }

    var email = $("input[name='uniqueField']").val();
    if (email == null || email == "") {
        $("#emailAlert").text("E-mail should be filled.");
        isPassed = false;
    }

    var emailPattern = /^[_A-Za-z0-9-\\+]+@[A-Za-z0-9-]+.[A-Za-z]{2,}$/;
    if (email != null && email != "" && !emailPattern.test(email)) {
        $("#emailAlert").text("Invalid E-mail format.");
        isPassed = false;
    }

    var pass1 = $("input[name='pass']").val();
    if (pass1 == null || pass1 == "") {
        $("#pass1Alert").text("Password should be filled.");
        isPassed = false;
    }

    var pass2 = $("input[name='pass2']").val();
    if (pass2 == null || pass2 == "") {
        $("#pass2Alert").text("Password should be filled.");
        isPassed = false;
    }

    if (pass1 != null && pass2 != null && pass1 != "" && pass2 != "" && pass1 != pass2) {
        $("#pass1Alert").text("Passwords do not match.");
        $("#pass2Alert").text("Passwords do not match.");
        isPassed = false;
    }

    if (pass1 != null && pass2 != null && pass1 != "" && pass2 != "" && pass1 == pass2 && pass1.length < 8) {
        $("#pass1Alert").text("Password should contain at least 8 characters.");
        $("#pass2Alert").text("Password should contain at least 8 characters.");
        isPassed = false;
    }

    var newPhone = $("#newPhone").val();
    if (newPhone != null && newPhone != "") {
        $("#newPhoneAlert").text("Please click '+' or clear this field.");
        isPassed = false;
    } else {
        $("#newPhoneAlert").text("");
    }

    return isPassed;
}

function addPhone() {
    lastPhoneIndex++;
    $("#lastPhone").before(
        "<div class=\"form-group\" id=\"div" + lastPhoneIndex + "\">" +
        "<label class=\"control-label col-sm-4\" for=\"phone" + lastPhoneIndex + "\">Phone:</label>" +
        "<div class=\"col-sm-4\">" +
        "<div class=\"row\">" +
        "<div class=\"col-xs-11\" style=\"padding-right: 0px\">" +
        "<input type=\"text\" class=\"form-control\" id=\"phone" + lastPhoneIndex + "\" placeholder=\"+_(___)___-__-__\" " +
        "name=\"phones[" + lastPhoneIndex + "].phoneNumber\" value=\"" + $("#newPhone").val() + "\">" +
        "<script>" +
        "$(function($){" +
        "$(\"#phone" + lastPhoneIndex + "\").mask(\"+9(999)999-99-99\", {placeholder: \"+_(___)___-__-__\"});" +
        "});" +
        "</script>" +
        "</div>" +
        "<div class=\"col-xs-1\" style=\"padding-left: 0px; padding-top: 5px;\">" +
        "<img src=\"" + delPicUrl + "\" alt=\"del\" class=\"create\" width=\"20\" height=\"15\" onclick='deletePhone(\"div" + lastPhoneIndex + "\", \"phone" + lastPhoneIndex + "\")'></div></div></div>" +
        "<div class=\"col-sm-4\" style=\"padding-top: 5px\">" + 
        "<span id=\"phonesAlert" + lastPhoneIndex + "\" class=\"alert\"></span></div></div>"
    );
    $("#newPhone").val("");
}

function deletePhone(idToHide, idToDisable) {
    if (confirm("Do you really want to delete this phone?") == true) {
        $("#" + idToHide).hide();
        $("#" + idToDisable).prop("disabled", true);
    }
}



