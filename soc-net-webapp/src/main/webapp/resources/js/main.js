$(document).ready(
    function () {
        // search:
        $("#search").change(function () {
            searchType = "all";
            if (currentPage == "friends") {
                searchType = "accounts";
            }
            if (currentPage == "groups") {
                searchType = "groups";
            }
            findItems($("#search").val(), searchType, 1);
            $("#search").val("");
            currentPage = "search";
        });

        //menu:
        // main:
        $("#menuMain").click(function () {
            if (currentPage == "main") {
                return false;
            }
            history.pushState({}, null, baseUrl + "main?id=" + myId);
            $.get(baseUrl + "mainpage", function (result) {
                $("#mainsection").html(result);
            }, "html")
            currentPage = "main";
            foreignId = null;
        })

        // Friends:
        $("#menuFriends").click(function () {
            if (currentPage == "friends") {
                return false;
            }
            $.get(baseUrl + "friends", function (result) {
                $("#mainsection").html(result);
            }, "html")
            currentPage = "friends";
        })

        // Groups:
        $("#menuGroups").click(function () {
            if (currentPage == "groups") {
                return false;
            }
            $.get(baseUrl + "groups", function (result) {
                $("#mainsection").html(result);
            }, "html")
            currentPage = "groups";
        })

        // Account profile:
        $("#menuProfile").click(function () {
            if (currentPage == "profile") {
                return false;
            }
            $.get(baseUrl + "profile", function (result) {
                $("#mainsection").html(result);
            }, "html")
            currentPage = "profile";
        })

        $.timer(10000, function () {
            $.getJSON(baseUrl + "friendsonline", function (result) {
                $("#friendsOnline").html("");
                $.each(result, function (i, field) {
                    $("#friendsOnline").append(
                        "<img src='" + baseUrl + "resources/img/greendot.jpg' width='7px' heigh='7px'>&nbsp;&nbsp;<a class=\"friendsonline\" href=\"#\" onclick=\"showAccount(" + field.id + ")\">" + field.text + "</a><br>"
                    );
                });
            });
        })
    }
)

function findItems(filter, type, page) {
    $.getJSON(baseUrl + "search?filter=" + filter + "&type=" + type + "&page=" + page, function (result) {
        var currentPageNumber = result.currentPage;
        var pagesQty = result.pagesQty;
        var dtos = result.dtos;

        $("#mainsection").html("<section class='page' id='searchSection'><h2 class='page'>Search Result:</h2>");
        $("#searchSection").append("<div class=\"table-responsive\"><table class=\"table\">");
        $.each(dtos, function (i, field) {
            $("#searchSection").append("<tr><td align=\"center\">" +
                "<img class=\"img-rounded\" src=\"" + baseUrl + field.pic + "\" height=\"50px\" style=\"padding: 5px 10px\"></td>" +
                "<td><span class=\"label label-default\">" +
                field.type + "</span> &nbsp;" +
                "<a class=\"login\" href=\"#\" onclick=\"\show" + field.type.substr(0, 1) + field.type.substr(1).toLowerCase() + "(" + field.id + ")\">" + field.text +
                "</a></td></tr>"
            );
        });
        $("#searchSection").append("</table></div>");
        var htmlText = "<div class=\"row text-center\"><nav aria-label=\"Page navigation\"><ul class=\"pagination\">";

        for (var i = 1; i <= pagesQty; i++) {
            htmlText += "<li class=\"page-item";
            if (i == currentPageNumber) {
                htmlText += " active";
            }
            htmlText += "\"><a class=\"page-link\" href=\"#\" ";
            if (i != currentPageNumber) {
                htmlText += " onclick=\"findItems('" + filter.trim() + "', '" + type + "', " + i + ")\" ";
            }
            htmlText += ">" + i + "</a></li>";
        }
        $("#searchSection").append(htmlText);
        $("#searchSection").append("</ul></nav></div>");
        $("#mainsection").append("</section>");
    });
}


function showAccount(accountId) {
    $.get(baseUrl + "mainpage?id=" + accountId, function (result) {
        $("#mainsection").html(result);
    }, "html")
    history.pushState({}, null, baseUrl + "main?id=" + accountId + "#");
    currentPage = "foreign";
    foreignId = accountId;
}


// FRIENDS FUNCTIONS:
function addFriendRequest() {
    if (foreignId != null) {
        $("#addFriendBtn").prop('disabled', true);
        $.get(baseUrl + "addfriendrequest?id=" + foreignId)
    }
}

function showFriends() {
    if (currentPage == "friends") {
        return false;
    }
    $.get(baseUrl + "friends", function (result) {
        $("#mainsection").html(result);
    }, "html")
    currentPage = "friends";
}

function outFriendRequests() {
    if (currentPage == "outFriendRequests") {
        return false;
    }
    $.get(baseUrl + "outfriendrequests", function (result) {
        $("#mainsection").html(result);
    }, "html")
    currentPage = "outFriendRequests";
}

function inFriendRequests() {
    if (currentPage == "inFriendRequests") {
        return false;
    }
    $.get(baseUrl + "infriendrequests", function (result) {
        $("#mainsection").html(result);
    }, "html")
    currentPage = "inFriendRequests";
}

function approveFriendRequest(friendId) {
    $.get(baseUrl + "approveFriendRequest?id=" + friendId, function (result) {
        $("#mainsection").html(result);
    }, "html")
}

function declineFriendRequest(friendId) {
    $.get(baseUrl + "declineFriendRequest?id=" + friendId, function (result) {
        $("#mainsection").html(result);
    }, "html")
}

function removeFromFriends(friendId) {
    $.get(baseUrl + "removeFromFriends?id=" + friendId, function (result) {
        $("#mainsection").html(result);
    }, "html")
}


// GROUPS FUNCTIONS:
function createGroup() {
    if (currentPage == "createGroup") {
        return false;
    }
    $.get(baseUrl + "creategroup", function (result) {
        $("#mainsection").html(result);
    }, "html")
    currentPage = "createGroup";
}

function validateGroupCreate() {
    var isPassed = true;

    // clear previous alerts:
    $("#groupNameAlert").text("");

    //validation:

    var groupName = $("input[name='uniqueField']").val();
    if (groupName == null || groupName == "") {
        $("#groupNameAlert").text("Group name should be filled.");
        isPassed = false;
    }

    if (isPassed) {
        var formData = new FormData($('#createGroupForm')[0]);
        $.ajax({
            url: baseUrl + "creategroup",
            method: "POST",
            data: formData,
            success: function (result) {
                $("#mainsection").html(result);
            },
            contentType: false,
            processData: false,
            cache: false,
            dataType: "html"
        });
        currentPage = "group";
    }
    return false;
}

function initializeGroupsPage() {
    history.pushState({}, null, baseUrl + "groups#");
}

function initializeGroupPage(groupId) {
    history.pushState({}, null, baseUrl + "group?id=" + groupId + "#");
    currentPage = "group";
}

function showGroup(groupId) {
    $.get(baseUrl + "grouppage?id=" + groupId, function (result) {
        $("#mainsection").html(result);
    }, "html")
    currentPage = "group";
}

function joinGroup(groupId) {
    $.get(baseUrl + "joingroup?id=" + groupId, function (result) {
        $("#mainsection").html(result);
    }, "html")
    currentPage = "group";
}

function inviteFriends(groupId) {
    $.get(baseUrl + "invitefriends?id=" + groupId, function (result) {
        $("#mainsection").html(result);
    }, "html")
    currentPage = "inviteFriends";
}

function sendInviteFriends() {
    var formData = new FormData($('#inviteFriendForm')[0]);
    $.ajax({
        url: baseUrl + "invitefriends",
        method: "POST",
        data: formData,
        success: function (result) {
            $("#mainsection").html(result);
        },
        contentType: false,
        processData: false,
        cache: false,
        dataType: "html"
    });
    currentPage = "group";
}

function showRequests() {
    if (currentPage == "groupRequests") {
        return false;
    }
    $.get(baseUrl + "grouprequests", function (result) {
        $("#mainsection").html(result);
    }, "html")
    currentPage = "groupRequests";
}

function showInvites() {
    if (currentPage == "groupInvites") {
        return false;
    }
    $.get(baseUrl + "groupinvites", function (result) {
        $("#mainsection").html(result);
    }, "html")
    currentPage = "groupInvites";
}

function showGroups() {
    if (currentPage == "groups") {
        return false;
    }
    $.get(baseUrl + "groups", function (result) {
        $("#mainsection").html(result);
    }, "html")
    currentPage = "groups";
}

function leaveGroup(groupId) {
    $.get(baseUrl + "leavegroup?id=" + groupId, function (result) {
        $("#mainsection").html(result);
    }, "html")
    currentPage = "groups";
}

function acceptInvite(groupId) {
    $.get(baseUrl + "acceptinvite?id=" + groupId, function (result) {
        $("#mainsection").html(result);
    }, "html")
    currentPage = "groupInvites";
}

function declineInvite(groupId) {
    $.get(baseUrl + "declineinvite?id=" + groupId, function (result) {
        $("#mainsection").html(result);
    }, "html")
    currentPage = "groupInvites";
}

function editGroup(groupId) {
    $.get(baseUrl + "editgroup?id=" + groupId, function (result) {
        $("#mainsection").html(result);
    }, "html")
    currentPage = "editGroup";
}

function validateGroupUpdate() {
    var isPassed = true;

    // clear previous alerts:
    $("#groupNameAlert").text("");

    //validation:
    var groupName = $("input[name='uniqueField']").val();
    if (groupName == null || groupName == "") {
        $("#groupNameAlert").text("Group name should be filled.");
        isPassed = false;
    }

    if (isPassed && confirm("Do you really want to update the group?") == true) {
        var formData = new FormData($('#updateGroupForm')[0]);
        $.ajax({
            url: baseUrl + "updategroup",
            method: "POST",
            data: formData,
            success: function (result) {
                $("#mainsection").html(result);
            },
            contentType: false,
            processData: false,
            cache: false,
            dataType: "html"
        });
        currentPage = "group";
    }
    return false;
}

function editModerators(groupId) {
    $.get(baseUrl + "editmoderators?id=" + groupId, function (result) {
        $("#mainsection").html(result);
    }, "html")
    currentPage = "editModerators";
}

function manageGroupRequests(groupId) {
    $.get(baseUrl + "managegrouprequests?id=" + groupId, function (result) {
        $("#mainsection").html(result);
    }, "html")
    currentPage = "manageGroupRequests";
}

function acceptGroupRequest(groupId, userId) {
    $.get(baseUrl + "acceptgrouprequest?groupId=" + groupId + "&userId=" + userId, function (result) {
        $("#mainsection").html(result);
    }, "html")
    currentPage = "manageGroupRequests";
}

function declineGroupRequest(groupId, userId) {
    $.get(baseUrl + "declinegrouprequest?groupId=" + groupId + "&userId=" + userId, function (result) {
        $("#mainsection").html(result);
    }, "html")
    currentPage = "manageGroupRequests";
}

function switchGroupRole(groupId, userId) {
    $.get(baseUrl + "switchgrouprole?groupId=" + groupId + "&userId=" + userId, function (result) {
        $("#mainsection").html(result);
    }, "html")
    currentPage = "editModerators";
}

function deleteGroup(groupId) {
    if (confirm("Do you really want to delete the group?") == true) {
        $.get(baseUrl + "deletegroup?id=" + groupId, function (result) {
            $("#mainsection").html(result);
        }, "html")
        currentPage = "groups";
    }
}

function createWallMessage(id) {
    $.get(baseUrl + "createWallMessage?id=" + id, function (result) {
        $("#wallMessagesContainer").html(result);
    }, "html")
}

function postWallMessage() {
    var formData = new FormData($('#createWallMessageForm')[0]);
    $.ajax({
        url: baseUrl + "postWallMessage",
        method: "POST",
        data: formData,
        success: function (result) {
            $("#wallMessagesContainer").html(result);
        },
        contentType: false,
        processData: false,
        cache: false,
        dataType: "html"
    });
    return false;
}