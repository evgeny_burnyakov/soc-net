function validateLogin() {
    var isPassed = true;

    // clear previous alerts:
    $("#emailAlert").text = "";
    $("#passAlert").text = "";

    //validation:

    var email = $("input[name='email']").val();
    if (email == null || email == "") {
        $("#emailAlert").text("Email must be entered.");
        isPassed = false;
    }

    var pass = $("input[name='pass']").val();
    if (pass == null || pass == "") {
        $("#passAlert").text("Password must be entered.");
        isPassed = false;
    }

    return isPassed;
}

