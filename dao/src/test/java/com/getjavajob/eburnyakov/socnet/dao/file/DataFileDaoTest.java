package com.getjavajob.eburnyakov.socnet.dao.file;

import com.getjavajob.eburnyakov.socnet.common.DataFile;
import org.junit.*;

import java.io.*;

import static java.io.File.separator;
import static org.junit.Assert.*;

public class DataFileDaoTest {
    private static final String ROOT_DIR = "src" + separator + "test";
    private static final String TEST_DIR = separator + "resources" + separator + "files" + separator;
    private static final String TEST_IMG = "img0672.jpg";

    private DataFileDao dataFileDao;
    private DataFile dataFile;
    private byte[] expectedFileBytes;

    @BeforeClass
    public static void onBeforeClass() {
    }

    @AfterClass
    public static void onAfterClass() {
    }

    @Before
    public void init() {
        expectedFileBytes = getBytesFromFile(new File(TEST_DIR + TEST_IMG));
        dataFileDao = new DataFileDao();
        dataFile = new DataFile();
    }

    @After
    public void destroy() {
    }

    @Test
    public void readFile() throws FileNotFoundException {
        dataFile = dataFileDao.read(new File(TEST_DIR + TEST_IMG));

        assertEquals(TEST_DIR + TEST_IMG, dataFile.getFilePath().toString());
        assertArrayEquals(expectedFileBytes, dataFile.getData());
    }

    @Test
    public void saveFile() {
        dataFile = dataFileDao.read(new File(TEST_DIR + TEST_IMG));
        dataFile.setFilePath(new File("/resources/img0672.jpg")); //changing dir to pass filter for the existing file
        dataFileDao.save(dataFile);

        System.out.println("file saved to: " + dataFile.getFilePath());
        assertArrayEquals(dataFile.getData(), getBytesFromFile(dataFile.getFilePath()));

//        try {
//            Thread.sleep(10000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        //restoring files/dirs after test:
        File fileToDel = new File(ROOT_DIR + dataFile.getFilePath().toString());
        fileToDel.delete();
        File innerDir = fileToDel.getParentFile();
        innerDir.delete();
        innerDir.getParentFile().delete();
    }

    private byte[] getBytesFromFile(File file) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try (InputStream input = new FileInputStream(ROOT_DIR + separator + file.getPath())) {
            int read;
            byte[] bytes = new byte[1024];
            while ((read = input.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return out.toByteArray();
    }
}
