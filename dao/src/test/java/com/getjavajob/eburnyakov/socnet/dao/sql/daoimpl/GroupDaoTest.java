package com.getjavajob.eburnyakov.socnet.dao.sql.daoimpl;

import com.getjavajob.eburnyakov.socnet.common.Account;
import com.getjavajob.eburnyakov.socnet.common.Group;
import com.getjavajob.eburnyakov.socnet.dao.FileDao;
import com.getjavajob.eburnyakov.socnet.dao.GeneralReadByKeyDao;
import com.getjavajob.eburnyakov.socnet.dao.file.DataFileDao;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.List;

import static java.io.File.separator;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-context.xml", "classpath:dao-context-override.xml"})
@Transactional
public class GroupDaoTest {
    private static final String TEST_DIR = separator + "resources" + separator + "files" + separator;
    private static final String TEST_IMG = TEST_DIR + "img0672.jpg";

    @Autowired
    private GeneralReadByKeyDao<Group> groupDao;
    private Account testAccount;
    @Autowired
    private FileDao fileDao;

    @BeforeClass
    public static void onBeforeClass() {
    }

    @AfterClass
    public static void onAfterClass() {
    }

    @Before
    public void init() {
        testAccount = new Account("test2@ya.ru");
        testAccount.setId(2);
        testAccount.setPass("password234");
        testAccount.setRole(Account.AccountRoles.USER);
        testAccount.setRegistrationDate(new GregorianCalendar(2016, 04, 01, 0, 0, 0).getTime());
        testAccount.setFirstName("Petya");
        testAccount.setSecondName("Petrovich");
        testAccount.setLastName("Petrov");
        testAccount.setBirthDate(new GregorianCalendar(1995, 1, 7).getTime());
        testAccount.setIcq("ICQ:1118985678978");
        testAccount.setSkype("skype:petya_programmer");
        testAccount.setPhoto(new DataFileDao().read(new File(TEST_IMG)));
    }

    @After
    public void destroy() {
    }

    @Test
    public void read() {
        Group expectedGroup = new Group("Snowboarders");
        expectedGroup.setId(1);
        expectedGroup.setRegistrationDate(new GregorianCalendar(2016, 00, 01, 0, 0, 0).getTime());
        expectedGroup.setOpen(true);
        expectedGroup.setDescription("Moscow snowboard Club");
        expectedGroup.setCreator(testAccount);
        expectedGroup.setPhoto(new DataFileDao().read(new File(TEST_IMG)));

        Group resultGroup = groupDao.read(1);
        resultGroup.getPhoto().setData(fileDao.read(resultGroup.getPhoto().getFilePath()).getData());

        System.out.println("Create and read test:");

        assertEquals(expectedGroup, resultGroup);
    }


    @Test
    public void create() {
        Group expectedGroup = new Group("Bikers");
        expectedGroup.setRegistrationDate(new GregorianCalendar(2016, 00, 01, 0, 0, 0).getTime());
        expectedGroup.setOpen(true);
        expectedGroup.setDescription("Moscow bikers Club");
        expectedGroup.setCreator(testAccount);
        expectedGroup.setPhoto(new DataFileDao().read(new File(TEST_IMG)));

        groupDao.create(expectedGroup);

        Group resultGroup = groupDao.read(expectedGroup.getId());

        System.out.println("Create and read test:");

        assertEquals(expectedGroup, resultGroup);
    }

    @Test
    public void delete() {
        Group groupBefore = groupDao.read(1);

        groupDao.delete(groupBefore.getId());

        Group groupAfter = groupDao.read(groupBefore.getId());

        System.out.println("Delete test:");

        assertNull(groupAfter);
    }

    @Test
    public void readByIds() {
        System.out.println("Read by IDs group test (reading id 1 and 3):");

        Group group1 = new Group("Snowboarders");
        group1.setId(1);
        group1.setDescription("Moscow beer Club");
        group1.setRegistrationDate(new GregorianCalendar(2016, 00, 01, 0, 0, 0).getTime());
        group1.setOpen(true);
        group1.setDescription("Moscow snowboard Club");
        group1.setCreator(testAccount);
        group1.setPhoto(new DataFileDao().read(new File(TEST_IMG)));

        Group group2 = new Group("Skaters");
        group2.setId(3);
        group2.setRegistrationDate(new GregorianCalendar(2016, 06, 15, 0, 0, 0).getTime());
        group2.setOpen(true);
        group2.setDescription("Moscow skaters Club");
        group2.setCreator(testAccount);
        group2.setPhoto(new DataFileDao().read(new File(TEST_IMG)));

        List<Group> expectedResult = new ArrayList<>();
        expectedResult.add(group1);
        expectedResult.add(group2);

        List<Group> groups = groupDao.readByIds(Arrays.asList(1L, 3L));
        for (Group grp : groups) {
            grp.getPhoto().setData(fileDao.read(grp.getPhoto().getFilePath()).getData());
        }

        assertEquals(expectedResult.size(), groups.size());

        for (int i = 0; i < groups.size(); i++) {
            assertEquals(expectedResult.get(i), groups.get(i));
        }
    }

    @Test
    public void readAll() {
        System.out.println("Read all groups:");

        Group group1 = new Group("Snowboarders");
        group1.setId(1);
        group1.setDescription("Moscow beer Club");
        group1.setRegistrationDate(new GregorianCalendar(2016, 00, 01, 0, 0, 0).getTime());
        group1.setOpen(true);
        group1.setDescription("Moscow snowboard Club");
        group1.setCreator(testAccount);
        group1.setPhoto(new DataFileDao().read(new File(TEST_IMG)));

        Group group2 = new Group("Beer club");
        group2.setId(2);
        group2.setRegistrationDate(new GregorianCalendar(2016, 04, 01, 0, 0, 0).getTime());
        group2.setOpen(false);
        group2.setDescription("Moscow beer Club");
        group2.setCreator(testAccount);
        group2.setPhoto(new DataFileDao().read(new File(TEST_IMG)));

        Group group3 = new Group("Skaters");
        group3.setId(3);
        group3.setRegistrationDate(new GregorianCalendar(2016, 06, 15, 0, 0, 0).getTime());
        group3.setOpen(true);
        group3.setDescription("Moscow skaters Club");
        group3.setCreator(testAccount);
        group3.setPhoto(new DataFileDao().read(new File(TEST_IMG)));

        List<Group> expectedResult = new ArrayList<>();
        expectedResult.add(group1);
        expectedResult.add(group2);
        expectedResult.add(group3);

        List<Group> groups = groupDao.readAll();
        for (Group grp : groups) {
            grp.getPhoto().setData(fileDao.read(grp.getPhoto().getFilePath()).getData());
        }
        assertEquals(groups.size(), expectedResult.size());

        for (int i = 0; i < groups.size(); i++) {
            assertEquals(expectedResult.get(i), groups.get(i));
        }
    }

    @Test
    public void update() {
        Group group = new Group("Snowboarders");
        group.setId(1);
        group.setRegistrationDate(new GregorianCalendar(2016, 00, 01, 0, 0, 0).getTime());
        group.setOpen(true);
        group.setDescription("Moscow snowboard and ski Club");
        group.setCreator(testAccount);
        group.setPhoto(new DataFileDao().read(new File(TEST_IMG)));

        groupDao.update(group);

        Group groupResult = groupDao.read(group.getId());
        groupResult.getPhoto().setData(fileDao.read(groupResult.getPhoto().getFilePath()).getData());

        System.out.println("Update group test:");

        assertEquals(group, groupResult);
    }
}
