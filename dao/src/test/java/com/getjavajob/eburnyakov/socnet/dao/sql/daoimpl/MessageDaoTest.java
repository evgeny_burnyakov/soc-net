package com.getjavajob.eburnyakov.socnet.dao.sql.daoimpl;

import com.getjavajob.eburnyakov.socnet.common.Account;
import com.getjavajob.eburnyakov.socnet.common.Message;
import com.getjavajob.eburnyakov.socnet.dao.FileDao;
import com.getjavajob.eburnyakov.socnet.dao.GeneralDao;
import com.getjavajob.eburnyakov.socnet.dao.file.DataFileDao;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.List;

import static java.io.File.separator;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-context.xml", "classpath:dao-context-override.xml"})
@Transactional
public class MessageDaoTest {
    private static final String TEST_DIR = separator + "resources" + separator + "files" + separator;
    private static final String TEST_IMG = TEST_DIR + "img0672.jpg";

    @Autowired
    private GeneralDao<Message> messageDao;
    private Account testAccount;
    @Autowired
    private FileDao fileDao;

    @BeforeClass
    public static void onBeforeClass() {
    }

    @AfterClass
    public static void onAfterClass() {
    }

    @Before
    public void init() {
        testAccount = new Account("test22@ya.ru");
        testAccount.setId(2);
        testAccount.setPass("password23455");
        testAccount.setRole(Account.AccountRoles.USER);
        testAccount.setRegistrationDate(new GregorianCalendar(2016, 04, 01, 0, 0, 0).getTime());
        testAccount.setFirstName("Petya");
        testAccount.setSecondName("Petrovich");
        testAccount.setLastName("Petrov");
        testAccount.setBirthDate(new GregorianCalendar(1995, 1, 7).getTime());
        testAccount.setIcq("ICQ:1118985678978");
        testAccount.setSkype("skype:petya_programmer");
        testAccount.setPhoto(new DataFileDao().read(new File(TEST_IMG)));
    }

    @After
    public void destroy() {
    }

    @Test
    public void read() {
        Message expectedMessage = new Message();
        expectedMessage.setId(1);
        expectedMessage.setAuthor(testAccount);
        expectedMessage.setText("Hello World!");
        expectedMessage.setRegistrationDate(new GregorianCalendar(2016, 00, 01, 0, 0, 0).getTime());
        expectedMessage.setTargetType(Message.TargetType.ACCOUNT_WALL);
        expectedMessage.setTargetID(testAccount.getId());
        expectedMessage.setPhoto(new DataFileDao().read(new File(TEST_IMG)));

        Message resultMessage = messageDao.read(1);
        resultMessage.getPhoto().setData(fileDao.read(resultMessage.getPhoto().getFilePath()).getData());

        System.out.println("Create and read test:");

        assertEquals(expectedMessage, resultMessage);
    }

    @Test
    public void create() {
        Message expectedMessage = new Message();
        expectedMessage.setAuthor(testAccount);
        expectedMessage.setText("Test message!");
        expectedMessage.setRegistrationDate(new GregorianCalendar(2016, 03, 03, 0, 0, 0).getTime());
        expectedMessage.setTargetType(Message.TargetType.ACCOUNT_WALL);
        expectedMessage.setTargetID(testAccount.getId());
        expectedMessage.setPhoto(new DataFileDao().read(new File(TEST_IMG)));

        messageDao.create(expectedMessage);

        Message resultMessage = messageDao.read(expectedMessage.getId());
        resultMessage.getPhoto().setData(fileDao.read(resultMessage.getPhoto().getFilePath()).getData());

        System.out.println("Create test:");
        System.out.println("expected:" + expectedMessage);
        System.out.println("result:" + resultMessage);

        assertEquals(expectedMessage, resultMessage);
    }

    @Test
    public void delete() {
        Message messageBefore = messageDao.read(3);

        messageDao.delete(messageBefore.getId());

        Message messageAfter = messageDao.read(messageBefore.getId());

        System.out.println("Delete test:");

        assertNull(messageAfter);
    }

    @Test
    public void readByIds() {
        System.out.println("Read by IDs message test (reading id 1 and 3):");

        Message message1 = new Message();
        message1.setId(1);
        message1.setAuthor(testAccount);
        message1.setText("Hello World!");
        message1.setRegistrationDate(new GregorianCalendar(2016, 00, 01, 0, 0, 0).getTime());
        message1.setTargetType(Message.TargetType.ACCOUNT_WALL);
        message1.setTargetID(testAccount.getId());

        message1.setPhoto(new DataFileDao().read(new File(TEST_IMG)));

        Message message2 = new Message();
        message2.setId(2);
        message2.setAuthor(testAccount);
        message2.setText("Hello Vasya!");
        message2.setRegistrationDate(new GregorianCalendar(2016, 01, 01, 0, 0, 0).getTime());
        message2.setTargetType(Message.TargetType.ACCOUNT_PRIVATE);
        message2.setTargetID(testAccount.getId());

        message2.setPhoto(new DataFileDao().read(new File(TEST_IMG)));

        Message message3 = new Message();
        message3.setId(3);
        message3.setAuthor(testAccount);
        message3.setText("Hello everybody!");
        message3.setRegistrationDate(new GregorianCalendar(2016, 02, 01, 0, 0, 0).getTime());
        message3.setTargetType(Message.TargetType.ACCOUNT_WALL);
        message3.setTargetID(testAccount.getId());

        message2.setPhoto(new DataFileDao().read(new File(TEST_IMG)));

        List<Message> expectedResult = new ArrayList<>();
        expectedResult.add(message1);
        expectedResult.add(message3);

        List<Message> resultMessages = messageDao.readByIds(Arrays.asList(1L, 3L));
        for (Message msg:resultMessages) {
            msg.getPhoto().setData(fileDao.read(msg.getPhoto().getFilePath()).getData());
        }

        assertEquals(expectedResult.size(), resultMessages.size());

        for (int i = 0; i < resultMessages.size(); i++) {
            assertEquals(expectedResult.get(i), resultMessages.get(i));
        }
    }

    @Test
    public void readAll() {
        System.out.println("Read all messages:");

        Message message1 = new Message();
        message1.setId(1);
        message1.setAuthor(testAccount);
        message1.setText("Hello World!");
        message1.setRegistrationDate(new GregorianCalendar(2016, 00, 01, 0, 0, 0).getTime());
        message1.setTargetType(Message.TargetType.ACCOUNT_WALL);
        message1.setTargetID(testAccount.getId());

        message1.setPhoto(new DataFileDao().read(new File(TEST_IMG)));

        Message message2 = new Message();
        message2.setId(2);
        message2.setAuthor(testAccount);
        message2.setText("Hello Vasya!");
        message2.setRegistrationDate(new GregorianCalendar(2016, 01, 01, 0, 0, 0).getTime());
        message2.setTargetType(Message.TargetType.ACCOUNT_PRIVATE);
        message2.setTargetID(testAccount.getId());

        message2.setPhoto(new DataFileDao().read(new File(TEST_IMG)));

        Message message3 = new Message();
        message3.setId(3);
        message3.setAuthor(testAccount);
        message3.setText("Hello everybody!");
        message3.setRegistrationDate(new GregorianCalendar(2016, 02, 01, 0, 0, 0).getTime());
        message3.setTargetType(Message.TargetType.ACCOUNT_WALL);
        message3.setTargetID(testAccount.getId());

        message3.setPhoto(new DataFileDao().read(new File(TEST_IMG)));

        List<Message> expectedResult = new ArrayList<>();
        expectedResult.add(message1);
        expectedResult.add(message2);
        expectedResult.add(message3);

        List<Message> resultMessages = messageDao.readAll();
        for (Message msg:resultMessages) {
            msg.getPhoto().setData(fileDao.read(msg.getPhoto().getFilePath()).getData());
        }
        assertEquals(expectedResult.size(), resultMessages.size());

        for (int i = 0; i < resultMessages.size(); i++) {
            assertEquals(expectedResult.get(i), resultMessages.get(i));
        }
    }

    @Test
    public void update() {
        Message message = new Message();
        message.setId(1);
        message.setAuthor(testAccount);
        message.setText("Hello Petrovich!");
        message.setRegistrationDate(new GregorianCalendar(2016, 05, 01, 0, 0, 0).getTime());
        message.setTargetType(Message.TargetType.ACCOUNT_PRIVATE);
        message.setTargetID(testAccount.getId());

        message.setPhoto(new DataFileDao().read(new File(TEST_IMG)));

        messageDao.update(message);

        Message messageResult = messageDao.read(message.getId());
        messageResult.getPhoto().setData(fileDao.read(messageResult.getPhoto().getFilePath()).getData());

        System.out.println("Update message test:");

        assertEquals(message, messageResult);
    }
}
