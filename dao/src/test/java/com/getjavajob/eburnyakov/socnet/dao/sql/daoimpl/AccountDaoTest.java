package com.getjavajob.eburnyakov.socnet.dao.sql.daoimpl;

import com.getjavajob.eburnyakov.socnet.common.Account;
import com.getjavajob.eburnyakov.socnet.common.DataFile;
import com.getjavajob.eburnyakov.socnet.dao.FileDao;
import com.getjavajob.eburnyakov.socnet.dao.GeneralReadByKeyDao;
import com.getjavajob.eburnyakov.socnet.dao.file.DataFileDao;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.List;

import static java.io.File.separator;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-context.xml", "classpath:dao-context-override.xml"})
@Transactional
public class AccountDaoTest {
    private static final String TEST_DIR = separator + "resources" + separator + "files" + separator;
    private static final String TEST_IMG = TEST_DIR + "img0672.jpg";
    @Autowired
    private GeneralReadByKeyDao<Account> accountDao;
    @Autowired
    private FileDao fileDao;

    @BeforeClass
    public static void onBeforeClass() {
    }

    @AfterClass
    public static void onAfterClass() {
    }

    @BeforeTransaction
    public void init() {
    }

    @After
    public void destroy() {
    }

    @Test
    public void read() {
        System.out.println("Read Account test:");
        Account accountExpected = new Account("test@mail.ru");
        accountExpected.setId(1);
        accountExpected.setPass("password123");
        accountExpected.setRole(Account.AccountRoles.USER);
        accountExpected.setRegistrationDate(new GregorianCalendar(2016, 05, 06, 0, 0, 0).getTime());
        accountExpected.setFirstName("Vasya");
        accountExpected.setSecondName("Ivanovich");
        accountExpected.setLastName("Pupkin");
        accountExpected.setBirthDate(new GregorianCalendar(1990, 5, 5).getTime());
        accountExpected.setIcq("ICQ:9898985678978");
        accountExpected.setSkype("skype:vasya_kpocaB4er");

        DataFile photo = new DataFileDao().read(new File(TEST_IMG));
        accountExpected.setPhoto(photo);

        Account accountResult = accountDao.read(1);
        accountResult.getPhoto().setData(fileDao.read(accountResult.getPhoto().getFilePath()).getData());
        System.out.println("expected: " + accountExpected);
        System.out.println("actual:" + accountResult);
        assertEquals(accountExpected, accountResult);
    }

    @Test
    public void readByKey() {
        System.out.println("Read by key Account test:");
        Account accountExpected2 = new Account("test2@ya.ru");
        accountExpected2.setId(2);
        accountExpected2.setPass("password234");
        accountExpected2.setRole(Account.AccountRoles.USER);
        accountExpected2.setRegistrationDate(new GregorianCalendar(2016, 04, 01, 0, 0, 0).getTime());
        accountExpected2.setFirstName("Petya");
        accountExpected2.setSecondName("Petrovich");
        accountExpected2.setLastName("Petrov");
        accountExpected2.setBirthDate(new GregorianCalendar(1995, 1, 7).getTime());
        accountExpected2.setIcq("ICQ:1118985678978");
        accountExpected2.setSkype("skype:petya_programmer");
        accountExpected2.setPhoto(new DataFileDao().read(new File(TEST_IMG)));

        Account accountResult = accountDao.readByKey("test2@ya.ru");
        accountResult.getPhoto().setData(fileDao.read(accountResult.getPhoto().getFilePath()).getData());
        System.out.println("expected: " + accountExpected2);
        System.out.println("actual:" + accountResult);
        assertEquals(accountExpected2, accountResult);
    }

    @Test
    public void readByIds() {
        System.out.println("Read accounts test with IDs 2 and 3:");

        Account accountExpected2 = new Account("test2@ya.ru");
        accountExpected2.setId(2);
        accountExpected2.setPass("password234");
        accountExpected2.setRole(Account.AccountRoles.USER);
        accountExpected2.setRegistrationDate(new GregorianCalendar(2016, 04, 01, 0, 0, 0).getTime());
        accountExpected2.setFirstName("Petya");
        accountExpected2.setSecondName("Petrovich");
        accountExpected2.setLastName("Petrov");
        accountExpected2.setBirthDate(new GregorianCalendar(1995, 1, 7).getTime());
        accountExpected2.setIcq("ICQ:1118985678978");
        accountExpected2.setSkype("skype:petya_programmer");
        accountExpected2.setPhoto(new DataFileDao().read(new File(TEST_IMG)));

        Account accountExpected3 = new Account("test2@gmail.com");
        accountExpected3.setId(3);
        accountExpected3.setPass("password789");
        accountExpected3.setRole(Account.AccountRoles.USER);
        accountExpected3.setRegistrationDate(new GregorianCalendar(2016, 00, 01, 0, 0, 0).getTime());
        accountExpected3.setFirstName("Katya");
        accountExpected3.setSecondName("Petrovna");
        accountExpected3.setLastName("Petrova");
        accountExpected3.setBirthDate(new GregorianCalendar(1997, 2, 7).getTime());
        accountExpected3.setIcq("ICQ:2228985678978");
        accountExpected3.setSkype("skype:katya_prog");
        accountExpected3.setPhoto(new DataFileDao().read(new File(TEST_IMG)));

        List<Account> expectedResult = new ArrayList<>();
        expectedResult.add(accountExpected2);
        expectedResult.add(accountExpected3);

        List<Account> accounts;
        accounts = accountDao.readByIds(Arrays.asList(2L, 3L));

        for (Account acc : accounts) {
            acc.getPhoto().setData(fileDao.read(acc.getPhoto().getFilePath()).getData());
        }

        System.out.println(accounts);

        assertEquals(accounts.size(), expectedResult.size());

        for (int i = 0; i < accounts.size(); i++) {
            assertEquals(expectedResult.get(i), accounts.get(i));
        }
    }

    @Test
    public void readAll() {
        System.out.println("Read all accounts test:");

        Account accountExpected1 = new Account("test@mail.ru");
        accountExpected1.setId(1);
        accountExpected1.setPass("password123");
        accountExpected1.setRole(Account.AccountRoles.USER);
        accountExpected1.setRegistrationDate(new GregorianCalendar(2016, 05, 06, 0, 0, 0).getTime());
        accountExpected1.setFirstName("Vasya");
        accountExpected1.setSecondName("Ivanovich");
        accountExpected1.setLastName("Pupkin");
        accountExpected1.setBirthDate(new GregorianCalendar(1990, 5, 5).getTime());
        accountExpected1.setIcq("ICQ:9898985678978");
        accountExpected1.setSkype("skype:vasya_kpocaB4er");
        accountExpected1.setPhoto(new DataFileDao().read(new File(TEST_IMG)));

        Account accountExpected2 = new Account("test2@ya.ru");
        accountExpected2.setId(2);
        accountExpected2.setPass("password234");
        accountExpected2.setRole(Account.AccountRoles.USER);
        accountExpected2.setRegistrationDate(new GregorianCalendar(2016, 04, 01, 0, 0, 0).getTime());
        accountExpected2.setFirstName("Petya");
        accountExpected2.setSecondName("Petrovich");
        accountExpected2.setLastName("Petrov");
        accountExpected2.setBirthDate(new GregorianCalendar(1995, 1, 7).getTime());
        accountExpected2.setIcq("ICQ:1118985678978");
        accountExpected2.setSkype("skype:petya_programmer");
        accountExpected2.setPhoto(new DataFileDao().read(new File(TEST_IMG)));

        Account accountExpected3 = new Account("test2@gmail.com");
        accountExpected3.setId(3);
        accountExpected3.setPass("password789");
        accountExpected3.setRole(Account.AccountRoles.USER);
        accountExpected3.setRegistrationDate(new GregorianCalendar(2016, 00, 01, 0, 0, 0).getTime());
        accountExpected3.setFirstName("Katya");
        accountExpected3.setSecondName("Petrovna");
        accountExpected3.setLastName("Petrova");
        accountExpected3.setBirthDate(new GregorianCalendar(1997, 2, 7).getTime());
        accountExpected3.setIcq("ICQ:2228985678978");
        accountExpected3.setSkype("skype:katya_prog");
        accountExpected3.setPhoto(new DataFileDao().read(new File(TEST_IMG)));

        List<Account> expectedResult = new ArrayList<>();
        expectedResult.add(accountExpected1);
        expectedResult.add(accountExpected2);
        expectedResult.add(accountExpected3);

        List<Account> accounts = accountDao.readAll();
        for (Account acc : accounts) {
            acc.getPhoto().setData(fileDao.read(acc.getPhoto().getFilePath()).getData());
        }

        assertEquals(accounts.size(), expectedResult.size());

        for (int i = 0; i < accounts.size(); i++) {
            assertEquals(expectedResult.get(i), accounts.get(i));
        }
    }

    @Test
    public void create() {
        System.out.println("Create account test:");
        Account accountExpected = new Account("test3@mail.ru");
        accountExpected.setPass("password1234");
        accountExpected.setRole(Account.AccountRoles.USER);
        accountExpected.setRegistrationDate(new GregorianCalendar(2016, 05, 06, 0, 0, 0).getTime());
        accountExpected.setFirstName("Petr");
        accountExpected.setSecondName("Petrovich");
        accountExpected.setLastName("Popov");
        accountExpected.setBirthDate(new GregorianCalendar(1995, 8, 1).getTime());
        accountExpected.setIcq("ICQ:98984564565");
        accountExpected.setSkype("skype:petr");
        accountExpected.setPhoto(new DataFileDao().read(new File(TEST_IMG)));
        System.out.println("expected: " + accountExpected);

        accountDao.create(accountExpected);

        System.out.println("Created account with ID: " + accountExpected.getId());

        Account accountResult = accountDao.read(accountExpected.getId());
        System.out.println("result: " + accountResult);

        assertEquals(accountExpected, accountResult);
    }

    @Test
    public void delete() {
        System.out.println("Delete test:");

        Account account = accountDao.read(1);
        System.out.println("Before: " + account);

        accountDao.delete(1);

        Account account2 = accountDao.read(1);
        System.out.println("after: " + account2);
        assertNull(account2);
    }

    @Test
    public void update() {
        System.out.println("Update account test:");

        Account account = new Account("test2@ya.ru");
        account.setId(2);
        account.setPass("password12345");
        account.setRole(Account.AccountRoles.USER);
        account.setRegistrationDate(new GregorianCalendar(2016, 05, 06, 0, 0, 0).getTime());
        account.setFirstName("Senya"); // in DB originally was Petya...
        account.setSecondName("Petrovich");
        account.setLastName("Putin");
        account.setBirthDate(new GregorianCalendar(1985, 5, 5).getTime());
        account.setIcq("ICQ:9898985678789");
        account.setSkype("skype:senya_skype");
        account.setPhoto(new DataFileDao().read(new File(TEST_IMG)));

        accountDao.update(account);

        Account accountResult = accountDao.read(account.getId());
        accountResult.getPhoto().setData(fileDao.read(accountResult.getPhoto().getFilePath()).getData());

        assertEquals(account, accountResult);
    }
}
