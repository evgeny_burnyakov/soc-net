package com.getjavajob.eburnyakov.socnet.dao;

public interface GeneralReadByKeyDao<T> extends GeneralDao<T> {
    T readByKey(String key);

    T readByKey(Long key);
}
