package com.getjavajob.eburnyakov.socnet.common;

import com.getjavajob.eburnyakov.socnet.common.Account.AccountRoles;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Account.class)
public abstract class Account_ extends com.getjavajob.eburnyakov.socnet.common.MainEntity_ {

	public static volatile ListAttribute<Account, GroupMember> groupMembers;
	public static volatile SingularAttribute<Account, String> uniqueField;
	public static volatile SingularAttribute<Account, String> lastName;
	public static volatile SingularAttribute<Account, AccountRoles> role;
	public static volatile ListAttribute<Account, Group> invitesToJoinGroups;
	public static volatile SingularAttribute<Account, String> pass;
	public static volatile SingularAttribute<Account, DataFile> photo;
	public static volatile ListAttribute<Account, Phone> phones;
	public static volatile SingularAttribute<Account, Date> birthDate;
	public static volatile ListAttribute<Account, Group> requestsToJoinGroups;
	public static volatile ListAttribute<Account, Account> friends;
	public static volatile SingularAttribute<Account, Date> timeOut;
	public static volatile SingularAttribute<Account, String> firstName;
	public static volatile SingularAttribute<Account, String> skype;
	public static volatile ListAttribute<Account, Account> friendsReceivedRequests;
	public static volatile SingularAttribute<Account, String> icq;
	public static volatile ListAttribute<Account, Account> friendsSentRequests;
	public static volatile SingularAttribute<Account, Date> registrationDate;
	public static volatile SingularAttribute<Account, String> secondName;

}

