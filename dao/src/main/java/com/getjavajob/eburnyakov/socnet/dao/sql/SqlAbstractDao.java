package com.getjavajob.eburnyakov.socnet.dao.sql;

import com.getjavajob.eburnyakov.socnet.dao.GeneralDao;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public abstract class SqlAbstractDao<T> implements GeneralDao<T> {
    @PersistenceContext
    protected EntityManager entityManager;
    protected Class<T> type;

    public SqlAbstractDao(Class<T> type) {
        this.type = type;
    }

    @Override
    public void create(final T obj) {
        entityManager.persist(obj);
    }

    @Override
    public T read(long id) {
        return entityManager.find(type, id);
    }

    @Override
    public void update(final T obj) {
        entityManager.merge(obj);
    }

    @Override
    public void delete(long id) {
        System.out.println("DELETING ENTITY WITH id:" + id);
        T entity = read(id);
        System.out.println("DELETING ENTITY :" + entity);
        entityManager.remove(entity);
    }

    @Override
    public List<T> readByIds(final List<Long> ids) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();

        CriteriaQuery<T> criteriaQuery = builder.createQuery(type);
        Root<T> root = criteriaQuery.from(type);
        criteriaQuery.where(root.get("id").in(ids));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public List<T> readAll() {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();

        CriteriaQuery<T> criteriaQuery = builder.createQuery(type);
        Root<T> root = criteriaQuery.from(type);
        criteriaQuery.select(root);

        return entityManager.createQuery(criteriaQuery).getResultList();
    }
}
