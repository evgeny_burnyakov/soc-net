package com.getjavajob.eburnyakov.socnet.dao.sql.daoimpl;

import com.getjavajob.eburnyakov.socnet.common.Account;
import com.getjavajob.eburnyakov.socnet.common.Account_;
import com.getjavajob.eburnyakov.socnet.dao.GeneralReadByKeySearchDao;
import com.getjavajob.eburnyakov.socnet.dao.sql.SqlAbstractReadByKeyDao;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class AccountDao extends SqlAbstractReadByKeyDao<Account> implements GeneralReadByKeySearchDao<Account> {

    public static final String KEY_FIELD_NAME = "uniqueField";

    public AccountDao() {
        super(Account.class, KEY_FIELD_NAME);
    }

    @Override
    public List<Account> search(int startRow, int recordsQty, String filter) {

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Account> criteriaQuery = builder.createQuery(type);
        Root<Account> root = criteriaQuery.from(type);

        criteriaQuery.where(
                builder.or(
                        builder.like(builder.lower(root.get(Account_.firstName)), "%" + filter.toLowerCase() + "%"),
                        builder.like(builder.lower(root.get(Account_.secondName)), "%" + filter.toLowerCase() + "%"),
                        builder.like(builder.lower(root.get(Account_.lastName)), "%" + filter.toLowerCase() + "%")
                )
        );

        CriteriaQuery<Account> select = criteriaQuery.select(root);
        TypedQuery<Account> typedQuery = entityManager.createQuery(select);
        typedQuery.setFirstResult(startRow - 1);
        typedQuery.setMaxResults(recordsQty);
        return typedQuery.getResultList();
    }

    @Override
    public Long getAllEntriesQty(String filter) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> countQuery = builder.createQuery(Long.class);

        Root<Account> root = countQuery.from(type);

        countQuery.where(
                builder.or(
                        builder.like(builder.lower(root.get(Account_.firstName)), "%" + filter.toLowerCase() + "%"),
                        builder.like(builder.lower(root.get(Account_.secondName)), "%" + filter.toLowerCase() + "%"),
                        builder.like(builder.lower(root.get(Account_.lastName)), "%" + filter.toLowerCase() + "%")
                )
        );

        countQuery.select(builder.count(root));

        Long result = null;
        try {
            result = entityManager.createQuery(countQuery).getSingleResult();
        } catch (NoResultException e) {
            //Ignore this because as per our logic this is ok.
        }
        return result;
    }
}
