package com.getjavajob.eburnyakov.socnet.dao;

import java.util.List;

public interface GeneralReadByKeySearchDao<T> extends GeneralReadByKeyDao<T> {

    List<T> search(int startRow, int recordsQty, String filter);

    Long getAllEntriesQty(String filter);
}
