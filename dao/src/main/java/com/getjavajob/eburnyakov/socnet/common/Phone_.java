package com.getjavajob.eburnyakov.socnet.common;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Phone.class)
public abstract class Phone_ extends com.getjavajob.eburnyakov.socnet.common.MainEntity_ {

	public static volatile SingularAttribute<Phone, Account> owner;
	public static volatile SingularAttribute<Phone, String> phoneNumber;

}

