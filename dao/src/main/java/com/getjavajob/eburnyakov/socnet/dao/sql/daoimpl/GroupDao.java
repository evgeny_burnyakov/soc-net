package com.getjavajob.eburnyakov.socnet.dao.sql.daoimpl;

import com.getjavajob.eburnyakov.socnet.common.Group;
import com.getjavajob.eburnyakov.socnet.common.Group_;
import com.getjavajob.eburnyakov.socnet.dao.GeneralReadByKeySearchDao;
import com.getjavajob.eburnyakov.socnet.dao.sql.SqlAbstractReadByKeyDao;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class GroupDao extends SqlAbstractReadByKeyDao<Group> implements GeneralReadByKeySearchDao<Group> {

    public static final String KEY_FIELD_NAME = "uniqueField";

    public GroupDao() {
        super(Group.class, KEY_FIELD_NAME);
    }

    @Override
    public List<Group> search(int startRow, int recordsQty, String filter) {

        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Group> criteriaQuery = builder.createQuery(type);
        Root<Group> root = criteriaQuery.from(type);

        criteriaQuery.where(builder.like(builder.lower(root.get(Group_.uniqueField)), "%" + filter.toLowerCase() + "%"));

        CriteriaQuery<Group> select = criteriaQuery.select(root);
        TypedQuery<Group> typedQuery = entityManager.createQuery(select);
        typedQuery.setFirstResult(startRow - 1);
        typedQuery.setMaxResults(recordsQty);
        return typedQuery.getResultList();
    }

    @Override
    public Long getAllEntriesQty(String filter) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> countQuery = builder.createQuery(Long.class);

        Root<Group> root = countQuery.from(type);
        countQuery.where(builder.like(builder.lower(root.get(Group_.uniqueField)), "%" + filter.toLowerCase() + "%"));
        countQuery.select(builder.count(root));

        Long result = null;
        try {
            result = entityManager.createQuery(countQuery).getSingleResult();
        } catch (NoResultException e) {
            //Ignore this because as per our logic this is ok.
        }
        return result;
    }
}
