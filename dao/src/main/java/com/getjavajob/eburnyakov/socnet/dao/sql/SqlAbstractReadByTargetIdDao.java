package com.getjavajob.eburnyakov.socnet.dao.sql;

import com.getjavajob.eburnyakov.socnet.dao.GeneralReadByKeyDao;
import com.getjavajob.eburnyakov.socnet.dao.GeneralReadByTargetIdDao;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public abstract class SqlAbstractReadByTargetIdDao<T> extends SqlAbstractDao<T> implements GeneralReadByTargetIdDao<T> {

    public SqlAbstractReadByTargetIdDao(Class<T> type) {
        super(type);
    }

    @Override
    public List<T> readByTargetId(long id) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();

        CriteriaQuery<T> criteriaQuery = builder.createQuery(type);
        Root<T> root = criteriaQuery.from(type);
        criteriaQuery.where(builder.equal(root.get("targetId"), id));
        criteriaQuery.orderBy(builder.desc(root.get("registrationDate")));

        return entityManager.createQuery(criteriaQuery).getResultList();
    }
}
