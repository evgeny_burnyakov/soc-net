package com.getjavajob.eburnyakov.socnet.common;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Group.class)
public abstract class Group_ extends com.getjavajob.eburnyakov.socnet.common.MainEntity_ {

	public static volatile SingularAttribute<Group, String> uniqueField;
	public static volatile SingularAttribute<Group, Account> creator;
	public static volatile SingularAttribute<Group, Boolean> isOpen;
	public static volatile ListAttribute<Group, Account> invitesToJoinGroups;
	public static volatile ListAttribute<Group, GroupMember> members;
	public static volatile SingularAttribute<Group, Date> registrationDate;
	public static volatile SingularAttribute<Group, String> description;
	public static volatile SingularAttribute<Group, DataFile> photo;
	public static volatile ListAttribute<Group, Account> requestsToJoinGroups;

}

