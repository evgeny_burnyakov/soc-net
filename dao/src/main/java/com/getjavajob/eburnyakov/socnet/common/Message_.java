package com.getjavajob.eburnyakov.socnet.common;

import com.getjavajob.eburnyakov.socnet.common.Message.TargetType;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Message.class)
public abstract class Message_ extends com.getjavajob.eburnyakov.socnet.common.MainEntity_ {

	public static volatile SingularAttribute<Message, Message> prevMessage;
	public static volatile SingularAttribute<Message, Long> targetID;
	public static volatile SingularAttribute<Message, Account> author;
	public static volatile SingularAttribute<Message, Date> registrationDate;
	public static volatile SingularAttribute<Message, DataFile> photo;
	public static volatile SingularAttribute<Message, TargetType> targetType;
	public static volatile SingularAttribute<Message, String> text;

}

