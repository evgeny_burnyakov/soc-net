package com.getjavajob.eburnyakov.socnet.common;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(DataFile.class)
public abstract class DataFile_ {

	public static volatile SingularAttribute<DataFile, String> filePathString;

}

