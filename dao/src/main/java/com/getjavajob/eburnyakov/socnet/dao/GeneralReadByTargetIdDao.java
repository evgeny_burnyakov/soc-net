package com.getjavajob.eburnyakov.socnet.dao;

import java.util.List;

public interface GeneralReadByTargetIdDao<T> extends GeneralDao<T> {
    List<T> readByTargetId(long id);
}
