package com.getjavajob.eburnyakov.socnet.dao.sql;

import com.getjavajob.eburnyakov.socnet.dao.GeneralReadByKeyDao;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public abstract class SqlAbstractReadByKeyDao<T> extends SqlAbstractDao<T> implements GeneralReadByKeyDao<T> {
    private String keyFieldName;

    public SqlAbstractReadByKeyDao(Class<T> type, String keyFieldName) {
        super(type);
        this.keyFieldName = keyFieldName;
    }

    @Override
    public T readByKey(String key) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();

        CriteriaQuery<T> criteriaQuery = builder.createQuery(type);
        Root<T> root = criteriaQuery.from(type);
        criteriaQuery.where(builder.equal(root.get(keyFieldName), key));

        T obj = null;
        try {
            obj = entityManager.createQuery(criteriaQuery).getSingleResult();
        } catch (NoResultException e) {
            //Ignore this because as per our logic this is ok!
        }
        return obj;
    }

    @Override
    public T readByKey(Long key) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();

        CriteriaQuery<T> criteriaQuery = builder.createQuery(type);
        Root<T> root = criteriaQuery.from(type);
        criteriaQuery.where(builder.equal(root.get(keyFieldName), key));

        T obj = null;
        try {
            obj = entityManager.createQuery(criteriaQuery).getSingleResult();
        } catch (NoResultException e) {
            //Ignore this because as per our logic this is ok!
        }
        return obj;
    }
}
