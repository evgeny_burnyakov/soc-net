package com.getjavajob.eburnyakov.socnet.dao;

import com.getjavajob.eburnyakov.socnet.common.DataFile;

import java.io.File;

public interface FileDao {
    DataFile read(File file);

    void save(DataFile dataFile);
}
