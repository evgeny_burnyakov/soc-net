package com.getjavajob.eburnyakov.socnet.dao;

import javax.persistence.EntityManager;
import java.util.List;

public interface GeneralDao<T> {

    T read(long id);

    List<T> readByIds(List<Long> ids);

    List<T> readAll();

    void create(T obj);

    void update(T obj);

    void delete(long id);

}
