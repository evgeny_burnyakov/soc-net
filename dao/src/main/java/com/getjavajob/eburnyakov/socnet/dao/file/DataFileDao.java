package com.getjavajob.eburnyakov.socnet.dao.file;

import com.getjavajob.eburnyakov.socnet.common.DataFile;
import com.getjavajob.eburnyakov.socnet.dao.FileDao;
import javaxt.io.Image;
import org.springframework.stereotype.Repository;

import java.io.*;
import java.util.Properties;
import java.util.UUID;

import static java.io.File.separator;

@Repository
public class DataFileDao implements FileDao {
    private static final String PROPERTIES_FILE = "datafile.properties";

    private String rootFileDir;
    private String dataFileDir;

    public DataFileDao() {
        Properties dataFileProps = new Properties();
        try {
            dataFileProps.load(this.getClass().getClassLoader().getResourceAsStream(PROPERTIES_FILE));
            dataFileDir = dataFileProps.getProperty("datafile.directory");
            rootFileDir = dataFileProps.getProperty("projectRoot.directory");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public DataFile read(File filePath) {
        String fileFullPath = rootFileDir + filePath.getPath();

        DataFile dataFile = new DataFile();
        dataFile.setFilePath(filePath);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try (InputStream input = new FileInputStream(fileFullPath)) {
            int read;
            final byte[] bytes = new byte[1024];
            while ((read = input.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        dataFile.setData(out.toByteArray());
        return dataFile;
    }

    @Override
    public void save(DataFile dataFile) {
        if (dataFile == null) {
            return;
        }
        //checking if just updating the same file:
        if (new File(rootFileDir + dataFile.getFilePath().getPath()).exists()) {
            return;
        }
        String oldFileName = dataFile.getFilePath().getName();
        String fileName;
        String fileExtension;
        String fileRelPath;
        String fileFullPath;
        File file;
        do {
            String genFilePrefix = UUID.randomUUID().toString();
            /**
             * Creating 2 level directory structure using 1-2th and 3-4th left letters from the generated file name
             * */
            fileExtension = oldFileName.substring(oldFileName.lastIndexOf(".")).toLowerCase();
            fileName = genFilePrefix + fileExtension;
            fileRelPath = dataFileDir + genFilePrefix.substring(0, 2) + separator + genFilePrefix.substring(2, 4);
            fileFullPath = rootFileDir + fileRelPath;
            file = new File(fileFullPath, fileName);
        } while (file.exists());

        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        dataFile.setFilePath(file);

        //auto-rotate jpeg images:
        if (fileExtension.matches("\\.jpe?g")) {
            Image image = new Image(dataFile.getData());
            image.rotate();
            dataFile.setData(image.getByteArray());
        }

        ByteArrayInputStream input = new ByteArrayInputStream(dataFile.getData());
        try (OutputStream out = new FileOutputStream(dataFile.getFilePath())) {
            int read;
            final byte[] bytes = new byte[1024];
            while ((read = input.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        dataFile.setFilePath(new File(fileRelPath, fileName));
    }
}
