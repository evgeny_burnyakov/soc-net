package com.getjavajob.eburnyakov.socnet.dao.sql.daoimpl;

import com.getjavajob.eburnyakov.socnet.common.Message;
import com.getjavajob.eburnyakov.socnet.dao.sql.SqlAbstractDao;
import com.getjavajob.eburnyakov.socnet.dao.sql.SqlAbstractReadByKeyDao;
import com.getjavajob.eburnyakov.socnet.dao.sql.SqlAbstractReadByTargetIdDao;
import org.springframework.stereotype.Repository;

@Repository
public class MessageDao extends SqlAbstractReadByTargetIdDao<Message> {
    public MessageDao() {
        super(Message.class);
    }
}
