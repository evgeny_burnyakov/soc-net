package com.getjavajob.eburnyakov.socnet.common;

import com.getjavajob.eburnyakov.socnet.common.GroupMember.GroupRoles;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(GroupMember.class)
public abstract class GroupMember_ {

	public static volatile SingularAttribute<GroupMember, GroupRoles> role;
	public static volatile SingularAttribute<GroupMember, Account> account;
	public static volatile SingularAttribute<GroupMember, Group> group;

}

