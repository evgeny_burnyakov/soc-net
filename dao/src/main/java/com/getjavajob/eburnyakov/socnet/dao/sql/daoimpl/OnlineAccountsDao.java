package com.getjavajob.eburnyakov.socnet.dao.sql.daoimpl;

import com.getjavajob.eburnyakov.socnet.common.OnlineAccount;
import com.getjavajob.eburnyakov.socnet.dao.sql.SqlAbstractDao;
import org.springframework.stereotype.Repository;

@Repository
public class OnlineAccountsDao extends SqlAbstractDao<OnlineAccount> {
    public OnlineAccountsDao() {
        super(OnlineAccount.class);
    }
}
