package com.getjavajob.eburnyakov.socnet.service.validators;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Validator<T> {
    private List<Checkable<T>> checklist;

    public Validator() {
    }

    public void setChecklist(List<Checkable<T>> checklist) {
        this.checklist = checklist;
    }

    public Map<String, String> validate(T obj) {
        Map<String, String> checkMessages = new HashMap<>();
        for (Checkable check : checklist) {
            Map.Entry<String, String> resultCheck = check.validate(obj);
            if (resultCheck != null) {
                checkMessages.put(resultCheck.getKey(), resultCheck.getValue());
            }
        }
        return checkMessages;
    }
}
