package com.getjavajob.eburnyakov.socnet.service.validators.account;

import com.getjavajob.eburnyakov.socnet.common.Account;
import com.getjavajob.eburnyakov.socnet.service.validators.CheckName;

import java.util.AbstractMap;
import java.util.Map;

public class CheckAccountSecondName extends CheckName<Account> {
    @Override
    protected String getName(Account account) {
        return account.getSecondName();
    }

    @Override
    protected Map.Entry<String, String> getEmptyMessage() {
        return null; // empty field is allowed
    }

    @Override
    protected Map.Entry<String, String> getNotMatchMessage() {
        return new AbstractMap.SimpleEntry<>("secondName", "Name field should contain only letters and '-'.");
    }
}
