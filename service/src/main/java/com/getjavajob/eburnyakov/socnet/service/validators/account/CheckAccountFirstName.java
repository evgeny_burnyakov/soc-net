package com.getjavajob.eburnyakov.socnet.service.validators.account;

import com.getjavajob.eburnyakov.socnet.common.Account;
import com.getjavajob.eburnyakov.socnet.service.validators.CheckName;

import java.util.AbstractMap;
import java.util.Map;

public class CheckAccountFirstName extends CheckName<Account> {
    @Override
    protected String getName(Account account) {
        return account.getFirstName();
    }

    @Override
    protected Map.Entry<String, String> getEmptyMessage() {
        return new AbstractMap.SimpleEntry<>("firstName", "First name should be entered.");
    }

    @Override
    protected Map.Entry<String, String> getNotMatchMessage() {
        return new AbstractMap.SimpleEntry<>("firstName", "Name field should contain only letters and '-'.");
    }
}
