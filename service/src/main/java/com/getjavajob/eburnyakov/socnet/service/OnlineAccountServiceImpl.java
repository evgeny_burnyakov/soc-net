package com.getjavajob.eburnyakov.socnet.service;

import com.getjavajob.eburnyakov.socnet.common.Account;
import com.getjavajob.eburnyakov.socnet.common.OnlineAccount;
import com.getjavajob.eburnyakov.socnet.dao.GeneralDao;
import com.getjavajob.eburnyakov.socnet.dao.GeneralReadByKeySearchDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class OnlineAccountServiceImpl implements OnlineAccountService {
    private GeneralDao<OnlineAccount> onlineAccountDao;
    private GeneralReadByKeySearchDao<Account> accountDao;

    @Autowired
    public OnlineAccountServiceImpl(GeneralDao<OnlineAccount> onlineAccountDao, GeneralReadByKeySearchDao<Account> accountDao) {
        this.onlineAccountDao = onlineAccountDao;
        this.accountDao = accountDao;
    }

    @Override
    public OnlineAccount readById(long id) {
        return onlineAccountDao.read(id);
    }

    @Override
    public void create(OnlineAccount onlineAccount) {
        onlineAccountDao.create(onlineAccount);
    }

    @Override
    public void update(OnlineAccount onlineAccount) {
        onlineAccountDao.update(onlineAccount);
    }

    @Override
    public void delete(long id) {
        onlineAccountDao.delete(id);
    }

    @Override
    public List<OnlineAccount> getFriendsOnline(long id) {
        List<OnlineAccount> accountsOnline = new ArrayList<>();

        Account account = accountDao.read(id);
        List<Account> friends = account.getFriends();

        List<Long> ids = new ArrayList<>();
        for (Account acc : friends) {
            ids.add(acc.getId());
        }

        if (friends != null && friends.size() > 0) {
            List<OnlineAccount> registeredAccounts = onlineAccountDao.readByIds(ids);

            Date currentDateTime = new Date();
            for (OnlineAccount onlineAccount : registeredAccounts) {
                if (onlineAccount.getTimeOut() != null && onlineAccount.getTimeOut().after(currentDateTime)) {
                    accountsOnline.add(onlineAccount);
                }
            }
        }
        return accountsOnline;
    }
}
