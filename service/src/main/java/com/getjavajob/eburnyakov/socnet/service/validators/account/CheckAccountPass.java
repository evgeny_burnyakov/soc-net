package com.getjavajob.eburnyakov.socnet.service.validators.account;

import com.getjavajob.eburnyakov.socnet.common.Account;
import com.getjavajob.eburnyakov.socnet.service.validators.CheckEmail;
import com.getjavajob.eburnyakov.socnet.service.validators.CheckPass;

public class CheckAccountPass extends CheckPass<Account> {

    @Override
    protected String getPass(Account account) {
        return account.getPass();
    }
}
