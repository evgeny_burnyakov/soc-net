package com.getjavajob.eburnyakov.socnet.service;

import com.getjavajob.eburnyakov.socnet.common.Account;
import com.getjavajob.eburnyakov.socnet.common.OnlineAccount;

import java.util.List;

public interface OnlineAccountService {

    OnlineAccount readById(long id);

    void create(OnlineAccount onlineAccount);

    void update(OnlineAccount onlineAccount);

    void delete(long id);

    List<OnlineAccount> getFriendsOnline(long id);
}
