package com.getjavajob.eburnyakov.socnet.service;

import com.getjavajob.eburnyakov.socnet.common.Account;
import com.getjavajob.eburnyakov.socnet.common.Group;
import com.getjavajob.eburnyakov.socnet.common.GroupMember;
import com.getjavajob.eburnyakov.socnet.dao.FileDao;
import com.getjavajob.eburnyakov.socnet.dao.GeneralReadByKeySearchDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {
    private GeneralReadByKeySearchDao<Account> accountDao;
    private FileDao dataFileDao;

    @Autowired
    public AccountServiceImpl(GeneralReadByKeySearchDao<Account> accountDao, FileDao dataFileDao) {
        this.accountDao = accountDao;
        this.dataFileDao = dataFileDao;
    }

    @Override
    public void create(Account account) {
        dataFileDao.save(account.getPhoto());
        accountDao.create(account);
    }

    @Override
    public void update(Account account) {
        if (account.getPhoto() != null) {
            dataFileDao.save(account.getPhoto());
        }
        accountDao.update(account);
    }

    @Override
    public Account readById(long id) {
        Account account = accountDao.read(id);
        if (account == null) {
            return null;
        }
        if (account.getPhoto() != null && account.getPhoto().getFilePath() != null) {
            account.setPhoto(dataFileDao.read(account.getPhoto().getFilePath()));
        }
        return account;
    }

    @Override
    public Account readByKey(String key) {
        return accountDao.readByKey(key);
    }

    @Override
    public List<Account> getAll() {
        return accountDao.readAll();
    }

    @Override
    public List<Account> search(int startRow, int recordsQty, String filter) {
        List<Account> accounts = accountDao.search(startRow, recordsQty, filter);
        accounts.size(); // force JPA initialization
        return accounts;
    }

    @Override
    public long getAllEntriesQty(String filter) {
        Long entriesQty = accountDao.getAllEntriesQty(filter);
        if (entriesQty == null) {
            return 0;
        }
        return entriesQty;
    }

    @Override
    public void delete(Account account) {
        long id = account.getId();
        if (id == 0) {
            id = accountDao.readByKey(account.getUniqueField()).getId();
        }
        accountDao.delete(id);
    }

    @Override
    public void addFriendRequest(long myAccountId, long friendAccountId) {
        Account myAccount = accountDao.read(myAccountId);
        Account friendAccount = accountDao.read(friendAccountId);
        if (myAccount != null && friendAccount != null) {
            myAccount.addFriendSentRequest(friendAccount);
        }
    }

    @Override
    public List<Account> getOutFriendRequests(long id) {
        List<Account> friendsSentRequests = accountDao.read(id).getFriendsSentRequests();
        friendsSentRequests.size();
        return friendsSentRequests;
    }

    @Override
    public List<Account> getInFriendRequests(long id) {
        List<Account> friendsReceivedRequests = accountDao.read(id).getFriendsReceivedRequests();
        friendsReceivedRequests.size();
        return friendsReceivedRequests;
    }

    @Override
    public void addFriend(long myAccountId, long friendAccountId) {
        Account myAccount = accountDao.read(myAccountId);
        Account friendAccount = accountDao.read(friendAccountId);
        myAccount.getFriendsReceivedRequests().remove(friendAccount);
        friendAccount.getFriendsSentRequests().remove(myAccount);
        myAccount.addFriend(friendAccount);
    }

    @Override
    public void declineFriend(long myAccountId, long friendAccountId) {
        Account myAccount = accountDao.read(myAccountId);
        Account friendAccount = accountDao.read(friendAccountId);
        myAccount.getFriendsReceivedRequests().remove(friendAccount);
        friendAccount.getFriendsSentRequests().remove(myAccount);
    }

    @Override
    public void removeFromFriends(long myAccountId, long friendAccountId) {
        Account myAccount = accountDao.read(myAccountId);
        Account friendAccount = accountDao.read(friendAccountId);
        myAccount.getFriends().remove(friendAccount);
        friendAccount.getFriends().remove(myAccount);
    }

    @Override
    public List<Account> getFriends(long id) {
        List<Account> friends = accountDao.read(id).getFriends();
        friends.size(); // initialization
        return friends;
    }

    @Override
    public boolean isRequestedFriendship(long myId, long foreignId) {
        Account myAccount = accountDao.read(myId);
        Account foreignAccount = accountDao.read(foreignId);
        if (myAccount.getFriends().contains(foreignAccount)) {
            return true;
        }
        if (myAccount.getFriendsSentRequests().contains(foreignAccount)) {
            return true;
        }
        if (myAccount.getFriendsReceivedRequests().contains(foreignAccount)) {
            return true;
        }
        return false;
    }

    @Override
    public List<Group> getRequestsToJoinGroup(long accountId) {
        List<Group> requestsToJoinGroup = accountDao.read(accountId).getRequestsToJoinGroups();
        requestsToJoinGroup.size();
        return requestsToJoinGroup;
    }

    @Override
    public List<Group> getInvitesToGroup(long accountId) {
        List<Group> invitesToGroup = accountDao.read(accountId).getInvitesToJoinGroups();
        invitesToGroup.size();
        return invitesToGroup;
    }

    @Override
    public List<GroupMember> getGroupsMembers(long accountId) {
        List<GroupMember> groupsMembers = accountDao.read(accountId).getGroupMembers();
        groupsMembers.size();
        return groupsMembers;
    }
}

