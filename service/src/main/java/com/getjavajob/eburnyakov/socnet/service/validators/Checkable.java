package com.getjavajob.eburnyakov.socnet.service.validators;

import java.util.Map;

public interface Checkable<T> {

    Map.Entry<String, String> validate(T obj);
}
