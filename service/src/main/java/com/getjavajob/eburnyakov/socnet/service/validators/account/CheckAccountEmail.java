package com.getjavajob.eburnyakov.socnet.service.validators.account;

import com.getjavajob.eburnyakov.socnet.common.Account;
import com.getjavajob.eburnyakov.socnet.dao.GeneralReadByKeyDao;
import com.getjavajob.eburnyakov.socnet.service.validators.CheckEmail;

import java.util.AbstractMap;
import java.util.Map;

public class CheckAccountEmail extends CheckEmail<Account> {
    private GeneralReadByKeyDao accountDao;

    public CheckAccountEmail() {
    }

    public void setAccountDao(GeneralReadByKeyDao accountDao) {
        this.accountDao = accountDao;
    }

    @Override
    protected String getEmail(Account account) {
        return account.getUniqueField();
    }

    @Override
    protected Map.Entry<String, String> getEmptyMessage() {
        return new AbstractMap.SimpleEntry<>("email", "Email should be entered.");
    }

    @Override
    protected Map.Entry<String, String> getNotMatchMessage() {
        return new AbstractMap.SimpleEntry<>("email", "Email field do not match email pattern.");
    }

    @Override
    protected boolean isEmailAlreadyExists(String email) {
        return accountDao.readByKey(email) != null;
    }

    @Override
    protected Map.Entry<String, String> getAlreadyExistsMessage() {
        return new AbstractMap.SimpleEntry<>("email", "This Email has been already registered.");
    }
}
