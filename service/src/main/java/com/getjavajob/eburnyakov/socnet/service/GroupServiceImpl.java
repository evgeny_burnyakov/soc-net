package com.getjavajob.eburnyakov.socnet.service;

import com.getjavajob.eburnyakov.socnet.common.Account;
import com.getjavajob.eburnyakov.socnet.common.Group;
import com.getjavajob.eburnyakov.socnet.common.GroupMember;
import com.getjavajob.eburnyakov.socnet.dao.FileDao;
import com.getjavajob.eburnyakov.socnet.dao.GeneralReadByKeyDao;
import com.getjavajob.eburnyakov.socnet.dao.GeneralReadByKeySearchDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class GroupServiceImpl implements GroupService {
    private GeneralReadByKeySearchDao<Account> accountDao;
    private GeneralReadByKeySearchDao<Group> groupDao;
    private FileDao dataFileDao;

    @Autowired
    public GroupServiceImpl(GeneralReadByKeySearchDao<Account> accountDao, GeneralReadByKeySearchDao<Group> groupDao, FileDao dataFileDao) {
        this.accountDao = accountDao;
        this.groupDao = groupDao;
        this.dataFileDao = dataFileDao;
    }

    @Override
    public void create(Group group) {
        dataFileDao.save(group.getPhoto());
        groupDao.create(group);
        addGroupMember(group.getCreator().getId(), group.getId());
    }

    @Override
    public void update(Group group) {
        if (group.getPhoto() != null) {
            dataFileDao.save(group.getPhoto());
        }
        groupDao.update(group);
    }

    @Override
    public Group readById(long id) {
        Group group = groupDao.read(id);
        if (group == null) {
            return null;
        }
        if (group.getPhoto() != null && group.getPhoto().getFilePath() != null) {
            group.setPhoto(dataFileDao.read(group.getPhoto().getFilePath()));
        }
        return group;
    }

    @Override
    public Group readByKey(String key) {
        return groupDao.readByKey(key);
    }

    @Override
    public List<Group> getAll() {
        return groupDao.readAll();
    }

    @Override
    public List<Group> search(int startRow, int recordsQty, String filter) {
        List<Group> groups = groupDao.search(startRow, recordsQty, filter);
        groups.size(); // force JPA initialization
        return groups;
    }

    @Override
    public long getAllEntriesQty(String filter) {
        Long entriesQty = groupDao.getAllEntriesQty(filter);
        if (entriesQty == null) {
            return 0;
        }
        return entriesQty;
    }

    @Override
    public void delete(Group group) {
        long id = group.getId();
        if (id == 0) {
            id = groupDao.readByKey(group.getUniqueField()).getId();
        }
        groupDao.delete(id);
    }

    @Override
    public void addRequestToJoinGroup(long groupId, long accountId) {
        Group group = groupDao.read(groupId);
        Account account = accountDao.read(accountId);
        if (group != null && account != null) {
            group.addRequestsToJoinGroups(account);
            group.removeInviteToJoinGroup(account);
        }
    }

    @Override
    public void addInviteToGroup(long groupId, long accountId) {
        Group group = groupDao.read(groupId);
        Account account = accountDao.read(accountId);
        if (group != null && account != null) {
            account.addInvitesToJoinGroups(group);
        }
    }

    @Override
    public List<Account> getRequestsToJoinGroup(long groupId) {
        List<Account> requestsToJoinGroup = groupDao.read(groupId).getRequestsToJoinGroups();
        requestsToJoinGroup.size();
        return requestsToJoinGroup;
    }

    @Override
    public void declineRequestToJoinGroup(long accountId, long groupId) {
        List<Account> requestsToJoinGroup = groupDao.read(groupId).getRequestsToJoinGroups();
        requestsToJoinGroup.remove(accountDao.read(accountId));
    }

    @Override
    public List<Account> getInvitesToGroup(long groupId) {
        List<Account> invitesToGroup = groupDao.read(groupId).getInvitesToJoinGroups();
        invitesToGroup.size();
        return invitesToGroup;
    }

    @Override
    public void declineInviteToGroup(long accountId, long groupId) {
        List<Account> invitesToGroup = groupDao.read(groupId).getInvitesToJoinGroups();
        invitesToGroup.remove(accountDao.read(accountId));
    }

    @Override
    public void addGroupMember(long accountId, long groupId) {
        Account account = accountDao.read(accountId);
        Group group = groupDao.read(groupId);
        group.addGroupMember(account);
        group.removeInviteToJoinGroup(account);
    }

    @Override
    public void removeGroupMember(GroupMember groupMember) {
        Account account = accountDao.read(groupMember.getAccount().getId());
        Group group = groupDao.read(groupMember.getGroup().getId());
        group.removeGroupMember(account);
    }

    @Override
    public List<GroupMember> getGroupMembers(long groupId) {
        List<GroupMember> groupMembers = groupDao.read(groupId).getMembers();
        groupMembers.size();
        return groupMembers;
    }


    @Override
    public List<Account> getGroupAccounts(long groupId) {
        List<Account> groupAccounts = new ArrayList<>();
        for (GroupMember groupMember : getGroupMembers(groupId)) {
            groupAccounts.add(groupMember.getAccount());
        }
        return groupAccounts;
    }

    @Override
    public void leaveGroup(long groupId, long accountId) {
        Account account = accountDao.read(accountId);
        Group group = groupDao.read(groupId);
        group.removeGroupMember(account);
    }

    @Override
    public void acceptInvite(long groupId, long accountId) {
        Account account = accountDao.read(accountId);
        Group group = groupDao.read(groupId);
        group.removeInviteToJoinGroup(account);
        if (group.isOpen()) {
            group.addGroupMember(account);
        } else {
            group.addRequestsToJoinGroups(account);
        }
    }

    @Override
    public void declineInvite(long groupId, long accountId) {
        Account account = accountDao.read(accountId);
        Group group = groupDao.read(groupId);
        group.removeInviteToJoinGroup(account);
    }

    @Override
    public void removeModerators(long groupId) {
        Group group = groupDao.read(groupId);
        for (GroupMember gm: group.getMembers()) {
            if (gm.getRole() == GroupMember.GroupRoles.MODERATOR) {
                gm.setRole(GroupMember.GroupRoles.USER);
            }
        }
    }

    @Override
    public void acceptGroupRequest(long groupId, long accountId) {
        Group group = groupDao.read(groupId);
        Account account = accountDao.read(accountId);
        group.removeRequestToJoinGroup(account);
        group.addGroupMember(account);
    }

    @Override
    public void declineGroupRequest(long groupId, long accountId) {
        Group group = groupDao.read(groupId);
        Account account = accountDao.read(accountId);
        group.removeRequestToJoinGroup(account);
    }

    @Override
    public List<Account> getGroupModerators(long groupId) {
        Group group = groupDao.read(groupId);
        List<Account> moderators = new ArrayList<>();
        moderators.add(group.getCreator());
        for (GroupMember member: group.getMembers()) {
            if (member.getRole() == GroupMember.GroupRoles.MODERATOR) {
                moderators.add(member.getAccount());
            }
        }
        return moderators;
    }

    @Override
    public void switchGroupRole(long groupId, long userId) {
        Group group = groupDao.read(groupId);
        Account account = accountDao.read(userId);
        GroupMember groupMember = getGroupMember(account, group);
        if (groupMember.getRole() == GroupMember.GroupRoles.USER) {
            groupMember.setRole(GroupMember.GroupRoles.MODERATOR);
        } else {
            groupMember.setRole(GroupMember.GroupRoles.USER);
        }
    }

    private GroupMember getGroupMember(Account account, Group group) {
        for (GroupMember member : group.getMembers()) {
            if (member.getAccount().equals(account)) {
                return member;
            }
        }
        return null;
    }
}

