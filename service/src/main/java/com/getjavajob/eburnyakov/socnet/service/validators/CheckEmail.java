package com.getjavajob.eburnyakov.socnet.service.validators;

import java.util.Map;

public abstract class CheckEmail<T> implements Checkable<T> {

    @Override
    public Map.Entry<String, String> validate(T obj) {
        String email = getEmail(obj);
        if (email == null || email.equals("")) {
            return getEmptyMessage();
        }
        if (!email.matches("^[_A-Za-z0-9-\\\\+]+@[A-Za-z0-9-]+.[A-Za-z]{2,}$")) {
            return getNotMatchMessage();
        }
        if (isEmailAlreadyExists(email)) {
            return getAlreadyExistsMessage();
        }
        return null;
    }

    protected abstract String getEmail(T obj);

    protected abstract Map.Entry<String, String> getEmptyMessage();

    protected abstract Map.Entry<String, String> getNotMatchMessage();

    protected abstract boolean isEmailAlreadyExists(String email);

    protected abstract Map.Entry<String, String> getAlreadyExistsMessage();
}
