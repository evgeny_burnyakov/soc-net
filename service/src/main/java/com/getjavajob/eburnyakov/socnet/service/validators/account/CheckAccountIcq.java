package com.getjavajob.eburnyakov.socnet.service.validators.account;

import com.getjavajob.eburnyakov.socnet.common.Account;
import com.getjavajob.eburnyakov.socnet.service.validators.Checkable;

import java.util.AbstractMap;
import java.util.Map;

public class CheckAccountIcq implements Checkable<Account> {
    @Override
    public Map.Entry<String, String> validate(Account account) {
        String icq = account.getIcq();
        if (icq == null || icq.equals("")) {
            return null;
        }
        if (!icq.matches("^[0-9]+$")) {
            return new AbstractMap.SimpleEntry<>("icq", "ICQ field should contain only numbers.");
        }
        return null;
    }
}
