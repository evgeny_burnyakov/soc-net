package com.getjavajob.eburnyakov.socnet.service;

import com.getjavajob.eburnyakov.socnet.common.Message;

import java.util.List;

public interface MessageService {

    Message readById(long id);

    List<Message> readByTargetId(long id);

    void create(Message message);

    void update(Message message);

    void delete(long id);
}
