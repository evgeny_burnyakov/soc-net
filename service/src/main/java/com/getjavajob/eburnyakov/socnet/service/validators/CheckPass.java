package com.getjavajob.eburnyakov.socnet.service.validators;

import java.util.AbstractMap;
import java.util.Map;

public abstract class CheckPass<T> implements Checkable<T> {
    @Override
    public Map.Entry<String, String> validate(T obj) {
        String pass = getPass(obj);
        if (pass == null || pass.equals("")) {
            return new AbstractMap.SimpleEntry<>("pass", "Password should be entered.");
        }
        if (pass.length() < 8) {
            return new AbstractMap.SimpleEntry<>("pass", "Password should be at least 8 symbols.");
        }
        return null;
    }

    protected abstract String getPass(T obj);
}
