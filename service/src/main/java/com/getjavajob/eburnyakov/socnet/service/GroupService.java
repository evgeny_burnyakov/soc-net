package com.getjavajob.eburnyakov.socnet.service;

import com.getjavajob.eburnyakov.socnet.common.Account;
import com.getjavajob.eburnyakov.socnet.common.Group;
import com.getjavajob.eburnyakov.socnet.common.GroupMember;

import java.util.List;

public interface GroupService {

    void create(Group group);

    Group readById(long id);

    Group readByKey(String key);

    List<Group> getAll();

    List<Group> search(int startRow, int recordsQty, String filter);

    long getAllEntriesQty(String filter);

    void update(Group group);

    void delete(Group group);

    void addRequestToJoinGroup(long groupId, long accountId);

    void addInviteToGroup(long groupId, long accountId);

    List<Account> getRequestsToJoinGroup(long groupId);

    List<Account> getInvitesToGroup(long groupId);

    void declineInviteToGroup(long accountId, long groupId);

    void declineRequestToJoinGroup(long accountId, long groupId);

    void addGroupMember(long accountId, long groupId);

    void removeGroupMember(GroupMember groupMember);

    List<GroupMember> getGroupMembers(long groupId);

    List<Account> getGroupAccounts(long groupId);

    void leaveGroup(long groupId, long accountId);

    void acceptInvite(long groupId, long accountId);

    void declineInvite(long groupId, long accountId);

    void removeModerators(long groupId);

    void acceptGroupRequest(long groupId, long accountId);

    void declineGroupRequest(long groupId, long accountId);

    List<Account> getGroupModerators(long groupId);

    void switchGroupRole(long groupId, long userId);
}
