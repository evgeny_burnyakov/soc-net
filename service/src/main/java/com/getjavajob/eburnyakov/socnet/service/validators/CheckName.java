package com.getjavajob.eburnyakov.socnet.service.validators;

import java.util.Map;

public abstract class CheckName<T> implements Checkable<T> {
    @Override
    public Map.Entry<String, String> validate(T obj) {
        String name = getName(obj);
        if (name == null || name.equals("")) {
            return getEmptyMessage();
        }
        if (!name.matches("^\\p{L}[\\p{L}\\-]*\\p{L}$")) {
            System.out.println("name:" + name);
            return getNotMatchMessage();
        }
        return null;
    }

    protected abstract String getName(T obj);

    protected abstract Map.Entry<String, String> getEmptyMessage();

    protected abstract Map.Entry<String, String> getNotMatchMessage();
}
