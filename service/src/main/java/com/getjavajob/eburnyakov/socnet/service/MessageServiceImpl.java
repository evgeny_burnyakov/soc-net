package com.getjavajob.eburnyakov.socnet.service;

import com.getjavajob.eburnyakov.socnet.common.Account;
import com.getjavajob.eburnyakov.socnet.common.Message;
import com.getjavajob.eburnyakov.socnet.common.OnlineAccount;
import com.getjavajob.eburnyakov.socnet.dao.GeneralDao;
import com.getjavajob.eburnyakov.socnet.dao.GeneralReadByKeyDao;
import com.getjavajob.eburnyakov.socnet.dao.GeneralReadByKeySearchDao;
import com.getjavajob.eburnyakov.socnet.dao.GeneralReadByTargetIdDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class MessageServiceImpl implements MessageService {
    private GeneralReadByTargetIdDao<Message> messageDao;

    @Autowired
    public MessageServiceImpl(GeneralReadByTargetIdDao<Message> messageDao) {
        this.messageDao = messageDao;
    }

    @Override
    public Message readById(long id) {
        return messageDao.read(id);
    }

    @Override
    public List<Message> readByTargetId(long id) {
        return messageDao.readByTargetId(id);
    }

    @Override
    public void create(Message message) {
        messageDao.create(message);
    }

    @Override
    public void update(Message message) {
        messageDao.update(message);
    }

    @Override
    public void delete(long id) {
        messageDao.delete(id);
    }
}
