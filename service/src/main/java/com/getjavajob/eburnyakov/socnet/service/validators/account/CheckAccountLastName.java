package com.getjavajob.eburnyakov.socnet.service.validators.account;

import com.getjavajob.eburnyakov.socnet.common.Account;
import com.getjavajob.eburnyakov.socnet.service.validators.CheckName;

import java.util.AbstractMap;
import java.util.Map;

public class CheckAccountLastName extends CheckName<Account> {
    @Override
    protected String getName(Account account) {
        return account.getLastName();
    }

    @Override
    protected Map.Entry<String, String> getEmptyMessage() {
        return null; // empty field is allowed
    }

    @Override
    protected Map.Entry<String, String> getNotMatchMessage() {
        return new AbstractMap.SimpleEntry<>("lastName", "Name field should contain only letters and '-'.");
    }
}
