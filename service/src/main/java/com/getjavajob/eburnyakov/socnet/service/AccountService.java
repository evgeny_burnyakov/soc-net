package com.getjavajob.eburnyakov.socnet.service;

import com.getjavajob.eburnyakov.socnet.common.Account;
import com.getjavajob.eburnyakov.socnet.common.Group;
import com.getjavajob.eburnyakov.socnet.common.GroupMember;
import com.getjavajob.eburnyakov.socnet.common.MainEntity;

import java.util.List;

public interface AccountService {

    void create(Account account);

    Account readById(long id);

    Account readByKey(String key);

    List<Account> getAll();

    List<Account> search(int startRow, int recordsQty, String filter);

    long getAllEntriesQty(String filter);

    void update(Account account);

    void delete(Account account);

    void addFriendRequest(long myAccountId, long friendAccountId);

    List<Account> getOutFriendRequests(long id);

    List<Account> getInFriendRequests(long id);

    void addFriend(long myAccountId, long friendAccountId);

    void declineFriend(long myAccountId, long friendAccountId);

    void removeFromFriends(long myAccountId, long friendAccountId);

    List<Account> getFriends(long id);

    boolean isRequestedFriendship(long myId, long friendId);

    List<Group> getRequestsToJoinGroup(long accountId);

    List<Group> getInvitesToGroup(long accountId);

    List<GroupMember> getGroupsMembers(long accountId);

}
