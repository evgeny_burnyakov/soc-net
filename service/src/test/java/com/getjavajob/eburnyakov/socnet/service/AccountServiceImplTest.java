package com.getjavajob.eburnyakov.socnet.service;

import com.getjavajob.eburnyakov.socnet.common.Account;
import com.getjavajob.eburnyakov.socnet.dao.FileDao;
import com.getjavajob.eburnyakov.socnet.dao.GeneralReadByKeySearchDao;
import com.getjavajob.eburnyakov.socnet.dao.file.DataFileDao;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.util.GregorianCalendar;

import static java.io.File.separator;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceImplTest {
    private static final String TEST_DIR = separator + "resources" + separator + "files" + separator;
    private static final String TEST_IMG = TEST_DIR + "img0672.jpg";

    @InjectMocks
    private AccountServiceImpl accountService;

    @Mock
    private GeneralReadByKeySearchDao<Account> accountDao;
    @Mock
    private FileDao dataFileDao;

    @BeforeClass
    public static void onBeforeClass() {
    }

    @AfterClass
    public static void onAfterClass() {
    }

    @Before
    public void init() {

    }

    @After
    public void destroy() {

    }

    @Test
    @Ignore
    public void createAccount() {
        Account account = new Account("test@mail.ru");
        account.setId(1);
        account.setPass("password123");
        account.setRole(Account.AccountRoles.USER);
        account.setRegistrationDate(new GregorianCalendar(2016, 05, 06, 0, 0, 0).getTime());
        account.setFirstName("Vasya");
        account.setSecondName("Ivanovich");
        account.setLastName("Pupkin");
        account.setBirthDate(new GregorianCalendar(1990, 5, 5).getTime());
        account.setIcq("ICQ:9898985678978");
        account.setSkype("skype:vasya_kpocaB4er");
        account.setPhoto(new DataFileDao().read(new File(TEST_IMG)));
        System.out.println(account);

        when(accountDao.read(anyLong())).thenReturn(account);

        accountService.create(account);

        Account result = accountService.readById(account.getId());
        accountService.delete(account);

        verify(accountDao).read(anyLong());
        verify(accountDao).create(any(Account.class));
        assertEquals(account, result);
    }

}
